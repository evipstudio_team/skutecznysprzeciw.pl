<?php
/*
 * Linijka do wywołania np.:
 * /home/evip/subdomeny/dyczkowscy/protected/yiic newsletter startsend --limit=5
 *
 */

Yii::import('ext.pid.pid');

class NewsletterCommand extends CConsoleCommand {

  public function actionStartSend($limit, $backupEmail = true) {
    $pid = new pid(Yii::app()->basePath . '/../assets/pids', 'NewsletterStartSend', '');
    if ($pid->already_running)
      die('Already running!');

    $activeNewsletters = Newsletter::model()->findAll('status=:status', array(':status' => 1));
    if ($activeNewsletters) {
      foreach ($activeNewsletters as $activeNewsletter) {
        //$newslettersRecipients = NewsletterRecipient::model()->findAll(array('condition'=>'`sent_at` IS NULL AND `newsletter_id` IN ('.  implode(',', CHtml::listData($activeNewsletters, 'id', 'id')).')','limit'=>$limit,'order'=>'run_count'));
        $newslettersRecipients = NewsletterRecipient::model()->findAll(array('condition' => '`sent_at` IS NULL AND `newsletter_id`=' . $activeNewsletter->id, 'limit' => $limit, 'order' => 'run_count'));
        if ($newslettersRecipients) {
          foreach ($newslettersRecipients as $newsletterRecipient) {
            $newsletterRecipient->send();
          }
        } else {
          echo 'Brak aktywnych odbiorców';
          $activeNewsletter->status=2;
          $activeNewsletter->save();
        }
      }
    } else {
      echo 'Brak aktywnych newsletterów';
    }
  }

}

?>