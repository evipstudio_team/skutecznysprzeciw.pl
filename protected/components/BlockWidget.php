<?php

class BlockWidget extends CWidget {

  public $id;

  public function run() {
    $model = Block::model()->findByPk($this->id);
    $functionName = $model->template;
    if(method_exists($this, $functionName))
      $this->$functionName($model);
    else $this->defaultAction($model);
  }

  protected function defaultAction($model) {
    $this->render($model->template, array(
        'model' => $model
    ));
  }

//  protected function articleList1($model) {
//    $pages = $model->activePages;
//    $this->render($model->template, array(
//        'model' => $model,
//        'pages'=>$pages
//    ));
//  }

}

?>