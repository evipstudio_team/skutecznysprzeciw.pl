<?php

class SButtonColumn extends CButtonColumn {

  protected function renderButton($id, $button, $row, $data) {
    if($button['imageUrl'] && isset($button['expressionImage']))
      $button['imageUrl'] = $this->evaluateExpression($button['imageUrl'], array('data' => $data, 'row' => $row));
    if($button['label'] && isset($button['expressionLabel']))
      $button['label'] = $this->evaluateExpression($button['label'], array('data' => $data, 'row' => $row));
    parent::renderButton($id, $button, $row, $data);

  }
}

?>