<?php

function htmlIdFromName($name) {
  $id = str_replace(array('][', ']', '['), '_', $name);
  if ((strrpos($id, '_') + 1) == strlen($id)) {
    $id = substr_replace($id, "", -1);
  }
  return $id;
}

function stripFromHttp($address) {
  return preg_replace('/(https?:\/\/)/', '', $address);
}

function stripFromWww($address) {
  return preg_replace('/^www\./', '', $address);
}

function normaliza($string) {
  $a = array('Ę', 'Ó', 'Ą', 'Ś', 'Ł', 'Ż', 'Ź', 'Ć', 'Ń', 'ę', 'ó', 'ą',
      'ś', 'ł', 'ż', 'ź', 'ć', 'ń');
  $b = array('E', 'O', 'A', 'S', 'L', 'Z', 'Z', 'C', 'N', 'e', 'o', 'a',
      's', 'l', 'z', 'z', 'c', 'n');
  $string = str_replace($a, $b, $string);
  $string = preg_replace('#[^a-z0-9\/]#is', ' ', $string);
  $string = trim($string);
  $string = preg_replace('#\s{2,}#', ' ', $string);
  $string = str_replace(' ', '-', $string);
  return $string;
}

function normalizaFileName($string) {
  $a = array('Ę', 'Ó', 'Ą', 'Ś', 'Ł', 'Ż', 'Ź', 'Ć', 'Ń', 'ę', 'ó', 'ą',
      'ś', 'ł', 'ż', 'ź', 'ć', 'ń');
  $b = array('E', 'O', 'A', 'S', 'L', 'Z', 'Z', 'C', 'N', 'e', 'o', 'a',
      's', 'l', 'z', 'z', 'c', 'n');
  //$string = str_replace($a, $b, $string);
  $string = preg_replace('#[^a-zA-Z0-9ĘÓĄŚŁŻŹĆŃęóąśłżźćń\/]#is', ' ', $string);
  $string = trim($string);
  $string = preg_replace('#\s{2,}#', ' ', $string);
  $string = str_replace(' ', '-', $string);
  return $string;
}

function getLangIcon($styled, $lang) {
  $style = '';
  if (!$styled) {
    $style = 'style="opacity : 0.2; filter: alpha(opacity=20);"';
  }
  return '<img ' . $style . ' alt="' . Yii::t('cms', 'Kliknij aby edytować') . '" title="' . (($style) ? Yii::t('cms', 'Treść w języku ' . $lang->getNameVariety() . ' nie istnieje, kliknij aby dodać.') : 'Treść w języku ' . $lang->getNameVariety() . ' istnieje, kliknij aby edytować.') . '" src="' . $lang->icon . '" />';
}

function normalizeHref($string) {
  preg_match('/(https?:\/\/)/', $string, $matches);
  if ($matches) {
    $string = preg_replace('#' . $matches[0] . '#', '', $string);
  }
  $string = preg_replace('#\/{2,}#', '/', $string);
  if ($string != '/')
    $string = preg_replace('#/$#', '', $string);
  if ($matches)
    $string = $matches[0] . $string;
  if (!preg_match('/(https?:\/\/)/', $string)) {
    if (!preg_match('/^\//', $string))
      $string = '/' . $string;
  }
  return $string;
}

function br2nl($string) {
  return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
}

function emailRegexp() {
  return '@(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")\@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])@';
}

function getElement($module_id, $element_id) {
  $module = Module::model()->findByPk($module_id);
  switch ($module->name) {
    case 'Articles':
      return Article::model()->findByPk($element_id);
      break;
    case 'Pages':
      return Page::model()->findByPk($element_id);
      break;
    case 'Links':
      return Link::model()->findByPk($element_id);
      break;
    case 'Banners':
      return Banner::model()->findByPk($element_id);
      break;
    case 'Gallery':
      return Gallery::model()->findByPk($element_id);
      break;
    case 'Multimedia':
      return Multimedia::model()->findByPk($element_id);
      break;

    default:
      break;
  }
}

function getCurrentUrl() {
  return $_SERVER['REQUEST_URI'];
}

function getStatusExpression($status) {
  if ($status == 1) {
    return Yii::t('cms', 'Opublikowany, kliknij aby deaktywować');
  } else {
    return Yii::t('cms', 'Nieopublikowany, kliknij aby aktywować');
  }
}

function getStatusIcon($status) {
  if ($status == 1) {
    return '/images/icons/active.png';
  } else {
    return '/images/icons/inactive.png';
  }
}

function defaultOnFocus($defaultValue) {
  return "if(jQuery(this).val() == '" . $defaultValue . "') {jQuery(this).val('');}";
}

function defaultOnBlur($defaultValue) {
  return "if(jQuery(this).val()=='') {jQuery(this).val('" . $defaultValue . "');}";
}

function getMimeType($filepath) {
//  if (array_key_exists($ext, $mime_types)) {
//    return $mime_types[$ext];
//  }
//  return mime_content_type($filepath);
  return mime_content_type2($filepath);
//  $file_info = new finfo(FILEINFO_MIME);
//  $mime_type = $file_info->buffer(file_get_contents($filepath));
//  return $mime_type;
}

function mime_content_type2($filename) {

  $mime_types = array(
      'txt' => 'text/plain',
      'htm' => 'text/html',
      'html' => 'text/html',
      'php' => 'text/html',
      'css' => 'text/css',
      'js' => 'application/javascript',
      'json' => 'application/json',
      'xml' => 'application/xml',
      'swf' => 'application/x-shockwave-flash',
      'flv' => 'video/x-flv',
      // images
      'png' => 'image/png',
      'jpe' => 'image/jpeg',
      'jpeg' => 'image/jpeg',
      'jpg' => 'image/jpeg',
      'gif' => 'image/gif',
      'bmp' => 'image/bmp',
      'ico' => 'image/vnd.microsoft.icon',
      'tiff' => 'image/tiff',
      'tif' => 'image/tiff',
      'svg' => 'image/svg+xml',
      'svgz' => 'image/svg+xml',
      // archives
      'zip' => 'application/zip',
      'rar' => 'application/x-rar-compressed',
      'exe' => 'application/x-msdownload',
      'msi' => 'application/x-msdownload',
      'cab' => 'application/vnd.ms-cab-compressed',
      // audio/video
      'mp3' => 'audio/mpeg',
      'qt' => 'video/quicktime',
      'mov' => 'video/quicktime',
      // adobe
      'pdf' => 'application/pdf',
      'psd' => 'image/vnd.adobe.photoshop',
      'ai' => 'application/postscript',
      'eps' => 'application/postscript',
      'ps' => 'application/postscript',
      // ms office
      'doc' => 'application/msword',
      'rtf' => 'application/rtf',
      'xls' => 'application/vnd.ms-excel',
      'ppt' => 'application/vnd.ms-powerpoint',
      // open office
      'odt' => 'application/vnd.oasis.opendocument.text',
      'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
  );

  $ext = strtolower(array_pop(explode('.', $filename)));
  if (array_key_exists($ext, $mime_types)) {
    return $mime_types[$ext];
  } elseif (function_exists('finfo_open')) {
    $finfo = finfo_open(FILEINFO_MIME);
    $mimetype = finfo_file($finfo, $filename);
    finfo_close($finfo);
    return $mimetype;
  } else {
    return 'application/octet-stream';
  }
}

function gridViewsortIcon() {
  return array(
      'value' => '"<img src=\"/images/icons/sort_icon.gif\" alt=\"\" title=\"\" />"',
      'type' => 'html',
      'headerHtmlOptions' => array('style' => 'width: 22px;'),
      'htmlOptions' => array('style' => 'padding-left: 0px !important; cursor:move'),
  );
}

function summaryTextLayout($gridId) {
  return Yii::t('zii', 'Displaying {start}-{end} of {count} result(s).') . '&nbsp;&nbsp;&nbsp;' . 'Ilość wyników na stronie: ' . CHtml::dropDownList('pageSize', Yii::app()->user->getState('pageSize'), array(10 => 10, 20 => 20, 50 => 50, 100 => 100), array(
              'onchange' => "$.fn.yiiGridView.update('" . $gridId . "',{ data:{pageSize: $(this).val() }})"));
}

function getParentPage($module_id, $element_id) {
  $element = getElement($module_id, $element_id);
  if ($element) {
    switch (get_class($element)) {
      case 'Article':
      case 'Banner':
      case 'Link':
        return $element->page;
        break;
      case 'Page':
      case 'Gallery':
        return $element;
        break;
      case 'Multimedia':
        return getElementPage(getElement($element->module_id, $element->element_id));
        break;

      default:
        break;
    }
  }
  else
    return null;
}

function getElementPage($element) {
  switch (get_class($element)) {
    case 'Article':
    case 'Banner':
    case 'Link':
      return $element->page;
      break;
    case 'Page':
    case 'Gallery':
      return $element;
      break;

    default:
      break;
  }
}

function loadingString() {
  return '<img src="/images/icons/loading.gif" alt="" title="" /> trwa ładowanie treści...';
}

function shortText($lenght, $string, $sufix = '') {
  if (strlen($string) > 40) {
    $string = substr($string, 0, 50) . $sufix;
  }
  return $string;
}

function generatePassword($length = 9, $strength = 0) {
  $vowels = 'aeuy';
  $consonants = 'bdghjmnpqrstvz';
  if ($strength & 1) {
    $consonants .= 'BDGHJLMNPQRSTVWXZ';
  }
  if ($strength & 2) {
    $vowels .= "AEUY";
  }
  if ($strength & 4) {
    $consonants .= '23456789';
  }
  if ($strength & 8) {
    $consonants .= '@#$%';
  }

  $password = '';
  $alt = time() % 2;
  for ($i = 0; $i < $length; $i++) {
    if ($alt == 1) {
      $password .= $consonants[(rand() % strlen($consonants))];
      $alt = 0;
    } else {
      $password .= $vowels[(rand() % strlen($vowels))];
      $alt = 1;
    }
  }
  return $password;
}

function getNextStep() {
  $causeId = Yii::app()->user->getState('unloggedCauseId',null);
  if($causeId) {
    $cause = Cause::model()->findByPk($causeId);
    if($cause)
      return $cause->lastComment->step;
  }
  return 1;
}

function getUserId() {
  $userId = null;
  if (!Yii::app()->user->isGuest) {
    $userId = Yii::app()->user->id;
  } elseif (Yii::app()->user->getState('unloggedId', null)) {
    $userId = Yii::app()->user->getState('unloggedId', null);
  }
  return $userId;
}

function emailize($text) {
  $regex = '/( \S+@\S+\.\S+ )/i';
  $replace = "<a href='mailto:$1'>$1</a>";

  $result = preg_replace($regex, $replace, $text);

  return $result;
}

function parseHyperlinks($string) {
    return preg_replace("/[^<>[:space:]]+skutecznysprzeciw.pl/", '<a href="\\0">\\0</a>', $string);
}

function parseEmails($string) {
    return preg_replace("/[^<>[:space:]]+[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*]@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,3})/", '<a href="mailto:\\0">\\0</a>', $string);
}

?>