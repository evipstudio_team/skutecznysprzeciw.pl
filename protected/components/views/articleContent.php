<?foreach($model->activeArticles(array('with'=>'activeTranslation')) as $article):?>
  <div class="header" style="text-transform: uppercase"><h2><?= $article->activeTranslation->title?></h2></div>
  <?if($article->filesCount):?>
    <div style="float:left; border: 1px solid #f8a138"> <img src="<?=$article->files[0]->link(160)?>"  alt="" /> </div>
  <?endif?>
  <div style="width:440px; float: right ">
    <h3><?= $article->activeTranslation->content?></h3>
  </div>
<?endforeach?>