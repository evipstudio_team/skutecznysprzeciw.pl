<?foreach($model->elements as $elementBlock):?>
  <li class="nav_li">
    <a <?if(getCurrentUrl()==Url::getActiveUrl($elementBlock->getElement()) || ($elementBlock->module_id==Page::getModuleId() && strpos(getCurrentUrl(), $elementBlock->getElement()->activeHref)===0)):?>class="mark" <?endif?>href="<?=Url::getActiveUrl($elementBlock->getElement())?>">
      <?=$elementBlock->getElement()->activeAnchor?>
    </a>
  </li>
<? endforeach; ?>
