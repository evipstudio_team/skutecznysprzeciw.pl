<?php

class BlockController extends Controller
{

	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'roles'=>array('CmsUser')
			),
      array('deny',
				'users'=>array('*'),
			),
		);
	}

  public function actionAjaxAssignPage($block_id,$page_id) {
    $block = Block::model()->findByPk($block_id);
    $params = array('block_id'=>$block_id,'page_id'=>$page_id);
    $BlockPage = BlockPage::model()->findByPk($params);
    $status = 0;
    if($BlockPage) $BlockPage->delete();
    else {
      BlockPage::createNew($params);
      $status = 1;
    }
    echo '<img src="'.$block->isAssignedIcon($page_id).'" alt="'.$block->isAssignedTranslate($page_id).'" title="'.$block->isAssignedTranslate($page_id).'" />';
    Yii::app()->end();
  }

  public function actionAjaxAssign($module_id,$element_id,$block_id) {
    $params = array('module_id'=>$module_id,'element_id'=>$element_id,'block_id'=>$block_id);
    $ElementBlock = ElementBlock::model()->findByPk($params);
    if($ElementBlock) $ElementBlock->delete();
    else ElementBlock::createNew($params);
  }

  public function actionAjaxModuleAssign($module_id,$block_id) {
    $params = array('module_id'=>$module_id,'block_id'=>$block_id);
    $ModuleBlock = ModuleBlock::model()->findByPk($params);
    if($ModuleBlock) $ModuleBlock->delete();
    else ModuleBlock::createNew($params);
  }

  public function getTabs($model) {
    $possibleTabs = array(
        'view' => array(
            'translated' => Yii::t('cms', 'Informacje podstawowe'),
            'url' => $this->createUrl('block/view', array('id' => $model->id)),
            'view' => 'view'),
        'edit' => array(
            'translated' => Yii::t('cms', 'Edycja bloku'),
            'url' => $this->createUrl('block/edit', array('id' => $model->id)),
            'view' => 'edit',
            'visible'=> Yii::app()->user->checkAccess('Root')
            ),
        'Assignment' => array(
            'translated' => Yii::t('cms', 'Przypisanie do modułów'),
            'url' => $this->createUrl('block/Assignment', array('id' => $model->id)),
            'view' => 'assignment',
            'visible'=> Yii::app()->user->checkAccess('Root')
            ),
    );
    $tabs = array();
    foreach ($possibleTabs as $action => $translated) {
      if (Yii::app()->getController()->action->id == $action) {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'view' => $translated['view'],
            'visible' => (isset($translated['visible']))? $translated['visible']:true
        );
      } else {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'url' => $translated['url'],
            'visible' => (isset($translated['visible']))? $translated['visible']:true
        );
      }
    }
    return $tabs;
  }

	public function actionView($id) {
    $block = Block::model()->findByPk($id);
    $this->setPageTitle($block->name.' - '.  Yii::t('cms', 'Informacje podstawowe'));

    $blockPage = new BlockPage();
    $blockPage->block_id = $block->id;

    $unsignedPage = new Page();

    $this->render('tabs', array(
        'block' => $block,
        'tabs' => $this->getTabs($block),
        'additionalParams' => array('blockPage'=>$blockPage, 'unsignedPage'=>$unsignedPage)
    ));
  }

  public function actionEdit($id) {
    $block = Block::model()->findByPk($id);
    $this->setPageTitle($block->name.' - '.  Yii::t('cms', 'Edycja'));
    if (Yii::app()->request->isPostRequest) {
      $block->setAttributes($_POST['Block']);
      if ($block->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Zapisano zmiany.'));
        $this->redirect($this->createUrl('edit', array('id' => $block->id)));
      }
    }
    $this->render('tabs', array(
        'block' => $block,
        'tabs' => $this->getTabs($block),
    ));
  }

  public function actionAssignment($id) {
    $block = Block::model()->findByPk($id);
    $this->setPageTitle($block->name.' - '.  Yii::t('cms', 'Przypisanie do modułów'));
    $newBlockAssignment = new BlockAssignment();
    $newBlockAssignment->block_id = $id;
    if(isset($_POST['BlockAssignment'])) {
      $newBlockAssignment->setAttributes($_POST['BlockAssignment']);
      if($newBlockAssignment->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Zapisano.'));
        $this->refresh();
      }
    }

    $blockAssignment = new blockAssignment('search');
    $blockAssignment->block_id = $id;
    $this->render('tabs', array(
        'block' => $block,
        'tabs' => $this->getTabs($block),
        'additionalParams'=>array('newBlockAssignment'=>$newBlockAssignment, 'blockAssignment'=>$blockAssignment)
    ));
  }

  public function actionSortView($id, $module_id, $element_id)
	{
    $elementBlock = new ElementBlock('search');
    $elementBlock->setAttributes(array('block_id'=>$id));

		$this->renderPartial('sortView',array(
      'model'=>$this->loadModel($id),
      'elementBlock'=>$elementBlock,
      'module'=>  Module::model()->findByPk($module_id),
      'element_id'=>$element_id
		),false,true);
	}

	public function actionCreate() {
    $model = new Block;

    if (isset($_POST['Block'])) {
      $model->attributes = $_POST['Block'];
      if ($model->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Nowy blok został utworzony, '));
        $this->redirect($this->createUrl('view', array('id' => $model->id)));
      } else {
        echo '<div>' . CHtml::errorSummary($model) . '</div>';
        Yii::app()->end();
      }
    }
  }

  public function actionSort($block_id) {
    if (isset($_POST['items']) && is_array($_POST['items'])) {
      $positionPrefix = isset($_POST['positionPrefix'])? $_POST['positionPrefix']:0;
      foreach ($_POST['items'] as $i=>$item) {
        $item = explode('x', $item);
        $page_id = $item[0];
        $model = BlockPage::model()->findByPk(array('page_id'=>$page_id,'block_id'=>$block_id));
        $model->position = $positionPrefix+$i+1;
        $model->save();
      }
    }
  }

	public function actionDelete($id) {
    if (Yii::app()->request->isPostRequest) {
      $this->loadModel($id)->delete();
      if (!isset($_GET['ajax']))
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }
    else
      throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
  }

	public function actionIndex()
	{
		$model=new Block('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Block']))
			$model->attributes=$_GET['Block'];

    $newBlock = new Block();
		$this->render('index',array(
			'model'=>$model,
      'newBlock'=>$newBlock
		));
	}

  public function actionIndexByElement($page_id)
	{
    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
    Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
    Yii::app()->clientScript->scriptMap['jquery.yiigridview.js'] = false;
//		$block=new Block('search');
//		$block->unsetAttributes();
//		if(isset($_GET['Block']))
//			$block->attributes=$_GET['Block'];

    $page = Page::model()->findByPk($page_id);
    $pageBlocks = Chtml::listData($page->blocks, 'id', 'id');
    $possibleBlocks = $page->searchPossibleBlocks();

		$this->renderPartial('indexByElement',array(
      'page'=>$page,
      'pageBlocks'=>$pageBlocks,
      'possibleBlocks'=>$possibleBlocks
		),false,true);
	}

	public function loadModel($id) {
    $model = Block::model()->findByPk($id);
    if ($model === null)
      throw new CHttpException(404, 'The requested page does not exist.');
    return $model;
  }

  public function actionDeleteAssignment($block_id, $module_id, $page_id) {
    $blockAssignment = BlockAssignment::model()->findByPk(array('block_id'=>$block_id,'module_id'=>$module_id,'page_id'=>$page_id));
    if($blockAssignment) $blockAssignment->delete();
    $this->redirect($this->createUrl('block/Assignment',array('id'=>$block_id)));
  }

}
