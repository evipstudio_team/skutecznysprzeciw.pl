<?php

class GalleryController extends Controller {

  public function filters() {
    return array(
        'accessControl',
    );
  }

  public function accessRules()
	{
		return array(
			array('allow',
				'roles'=>array('CmsUser')
			),
      array('deny',
				'users'=>array('*'),
			),
		);
	}

  public function actionIndex($id=null,$page_id=null) {
    $page_id = $id? $id:$page_id;
    $page = Page::model()->findByPk($page_id);
    $this->page_id = $page->id;
    $this->setPageTitle(Yii::t('cms', $page->name.' - Lista elementów'));
    $this->render('index', array('page' => $page));
  }

  public function actionCreate($page_id) {
    $page = Page::model()->findByPk($page_id);
    $this->page_id = $page->id;
    $this->setPageTitle(Yii::t('cms', 'Dodaj nowy element'));

    $gallery = new Gallery('search');

    $this->render('create', array(
        'page' => $page,
        'gallery' => $gallery
    ));
  }

  public function actionFiles($id) {
    $this->redirect($this->createUrl('gallery/index',array('page_id'=>$id)));
  }

  public function actionEdit($id) {
    $this->redirect($this->createUrl('gallery/index',array('page_id'=>$id)));
  }

}