<?php

class MultimediaController extends Controller {

  /**
   * @return array action filters
   */
  public function filters() {
    return array(
        'accessControl', // perform access control for CRUD operations
    );
  }

  /**
   * Specifies the access control rules.
   * This method is used by the 'accessControl' filter.
   * @return array access control rules
   */
  public function accessRules()
	{
		return array(
			array('allow',
				'roles'=>array('CmsUser')
			),
      array('deny',
				'users'=>array('*'),
			),
		);
	}

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id) {
    $this->render('view', array(
        'model' => $this->loadModel($id),
    ));
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate() {
    $model = new Multimedia;

    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);

    if (isset($_POST['Multimedia'])) {
      $model->attributes = $_POST['Multimedia'];
      if ($model->save())
        $this->redirect(array('view', 'id' => $model->page_id));
    }

    $this->render('create', array(
        'model' => $model,
    ));
  }

  public function actionUpload($page_id) {
    $page = Page::model()->findByPk($page_id);
    if ($_FILES && $_FILES['files']['name'][0]) {
      $messagesPerFile = array();
      $messages = array('success' => array(), 'error' => array());
      foreach ($_FILES['files']['name'] as $index => $filePost) {
        $fileData = array(
            'name' => $_FILES['files']['name'][$index],
            'type' => $_FILES['files']['type'][$index],
            'tmp_name' => $_FILES['files']['tmp_name'][$index],
            'error' => $_FILES['files']['error'][$index],
            'size' => $_FILES['files']['size'][$index]
        );
        $messagesPerFile[] = Multimedia::uploadFile($fileData, $page_id);
      }
      foreach ($messagesPerFile as $message)
        foreach (array('success', 'error') as $field)
          if (isset($message[$field]))
            $messages[$field][] = $message[$field];
      foreach (array('success', 'error') as $field)
        if ($messages[$field])
          Yii::app()->user->setFlash($field, implode('<br />', $messages[$field]));
    }
    else {
      Yii::app()->user->setFlash('error', Yii::t('cms', 'Nie wskazałeś żadnego pliku'));
    }
    $this->redirect($this->createUrl($_POST['redirectController'].'/'.$_POST['redirectAction'], array('id'=>$page->id)));
  }

  public function actionSort() {
    if (isset($_POST['items']) && is_array($_POST['items'])) {
      $positionPrefix = isset($_POST['positionPrefix'])? intval($_POST['positionPrefix']):0;
      foreach ($_POST['items'] as $i => $item) {
        $project = Multimedia::model()->findByPk($item);
        $project->position = $positionPrefix + $i + 1;
//        $project->position = $i + 1;
        $project->save();
      }
    }
  }

  public function getTabs($model) {
    $possibleTabs = array(
        'edit' => array(
            'translated' => Yii::t('cms', 'Edycja danych podstawowych'),
            'url' => $this->createUrl('multimedia/edit', array('id' => $model->id)),
            'view' => 'edit'),
        'editTranslation' => array(
            'translated' => Yii::t('cms', 'Edycja podpisu pliku'),
            'url' => $this->createUrl('multimedia/editTranslation', array('multimedia_id' => $model->id, 'lang_id'=>Yii::app()->params['lang']->id)),
            'view' => 'editTranslation'),
//        'urls' => array(
//            'translated' => Yii::t('cms', 'Hiperłącze z pliku'),
//            'url' => $this->createUrl('multimedia/urls', array('id' => $model->id, 'lang_id'=>Yii::app()->params['lang']->id)),
//            'view' => 'urls'),
    );
    $tabs = array();
    foreach ($possibleTabs as $action => $translated) {
      if (Yii::app()->getController()->action->id == $action) {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'view' => $translated['view'],
        );
      } else {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'url' => $translated['url'],
        );
      }
    }
    return $tabs;
  }

  public function actionEdit($id) {
    $multimedia = $this->loadModel($id);
    $page = $multimedia->page;
    $this->page_id = $page->id;

    $this->setPageTitle(Yii::t('cms', 'Edycja pliku '.$multimedia->filename));

    if (isset($_POST['Multimedia'])) {
      if ($_FILES && $_FILES['file']['name']) {
        $fileData = array(
            'name' => $_FILES['file']['name'],
            'type' => $_FILES['file']['type'],
            'tmp_name' => $_FILES['file']['tmp_name'],
            'error' => $_FILES['file']['error'],
            'size' => $_FILES['file']['size']
        );
        Multimedia::uploadFile($fileData, $page->id, $multimedia->id);
        $multimedia->refresh();
      }
      $multimedia->attributes = $_POST['Multimedia'];
      if ($multimedia->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Zmiany zostały zapisane.'));
        $this->redirect(array('edit', 'id' => $multimedia->id));
      }
    }

    $this->render('tabs', array(
        'multimedia'=>$multimedia,
        'page'=>$page,
        'tabs'=>$this->getTabs($multimedia)
    ));
  }

//  public function actionUrls($id, $lang_id) {
//    $model = $this->loadModel($id);
//    $relatedElement = getElement($model->module_id, $model->element_id);
//    $relatedElementPage = getParentPage($relatedElement->getModuleId(), $relatedElement->id);
//
//    $this->setPageTitle(Yii::t('cms', 'Edycja pliku '.$model->filename));
//
//    $this->render('tabs', array(
//        'multimedia' => $model,
//        'relatedElementPage' => $relatedElementPage,
//        'tabs'=>$this->getTabs($model),
//        'additionalParams'=>array('lang_id'=>$lang_id)
//    ));
//  }

//  public function actionEdit($id) {
//    $multimedia = $this->loadModel($id);
//    $multimedia->loadTranslations();
//    $multimedia->loadUrls();
//    $relatedElement = getElement($multimedia->module_id, $multimedia->element_id);
//    $relatedElementPage = getParentPage($relatedElement->getModuleId(), $relatedElement->id);
//
//    $this->setPageTitle(Yii::t('cms', 'Edycja pliku '.$multimedia->filename));
//
//    if (isset($_POST['Multimedia'])) {
//      if ($_FILES && $_FILES['file']['name']) {
//        $fileData = array(
//            'name' => $_FILES['file']['name'],
//            'type' => $_FILES['file']['type'],
//            'tmp_name' => $_FILES['file']['tmp_name'],
//            'error' => $_FILES['file']['error'],
//            'size' => $_FILES['file']['size']
//        );
//        Multimedia::uploadFile($fileData, $multimedia->module_id, $multimedia->element_id, $multimedia->id);
//        $multimedia->refresh();
//      }
//      $multimedia->attributes = $_POST['Multimedia'];
//      if ($multimedia->save()) {
//        Yii::app()->user->setFlash('success', Yii::t('cms', 'Zmiany zostały zapisane.'));
//        $this->redirect(array('edit', 'id' => $multimedia->id));
//      }
//    }
//
//    $this->render('edit', array(
//        'multimedia'=>$multimedia,
//        'model' => getElement($multimedia->module_id, $multimedia->element_id),
//        'relatedElementPage' => $relatedElementPage
//    ));
//  }

  public function actionEditTranslation($multimedia_id, $lang_id) {
    $model = $this->loadModel($multimedia_id);
    $model->loadTranslations();
    $page = $model->page;
    $this->page_id = $page->id;

    $this->setPageTitle(Yii::t('cms', 'Edycja pliku '.$model->filename));

    if ($_POST) {
      $model->translations[$lang_id]->attributes = $_POST['MultimediaTranslation'];
      if ($model->translations[$lang_id]->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Zmiany zostały zapisane.'));
        $this->refresh();
//        $this->redirect($this->createUrl('multimedia/editTrans', array('id' => $model->id)) . '#Translation' . $lang_id);
      }
    }


    $this->render('tabs', array(
        'multimedia' => $model,
        'page'=>$page,
        'additionalParams'=>array('translation'=>$model->translations[$lang_id]),
        'tabs'=>$this->getTabs($model)
    ));
  }

  public function actionDeleteTranslation($multimedia_id, $lang_id) {
    $model = MultimediaTranslation::model()->findByPk(array('multimedia_id' => $multimedia_id, 'lang_id' => $lang_id));
    if ($model && $model->delete())
      Yii::app()->user->setFlash('success', Yii::t('cms', 'Element został usunięty.'));
    else
      Yii::app()->user->setFlash('error', Yii::t('cms', 'Element nie został usunięty.'));
    $this->redirect($this->createUrl('multimedia/editTranslation', array('multimedia_id' => $multimedia_id,'lang_id'=>$lang_id)));
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'admin' page.
   * @param integer $id the ID of the model to be deleted
   */
  public function actionDelete($id) {
    $this->loadModel($id)->delete();
//    if (Yii::app()->request->isPostRequest) {
//      // we only allow deletion via POST request
//
//
//      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//      if (!isset($_GET['ajax']))
//        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
//    }
//    else
//      throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
  }

  public function actionIndex($page_id, $redirectAction, $redirectController) {
    $page = Page::model()->findByPk($page_id);
    $multimedia = new Multimedia('search');
    $multimedia->unsetAttributes();
    $multimedia->setAttributes(array(
        'page_id' => $page_id
            ), false);
    if (isset($_GET['Multimedia']))
      $multimedia->attributes = $_GET['Multimedia'];

    if (isset($_GET['pageSize'])) {
      Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
      unset($_GET['pageSize']);
    }

    $this->renderPartial('index', array(
        'multimedia' => $multimedia,
        'page' => $page,
        'redirectAction' => $redirectAction,
        'redirectController' => $redirectController
            ), false, true);
  }

  public function actionListing($page_id) {
    $model = new Multimedia('search');
    $model->unsetAttributes();  // clear any default values
    $model->page_id = $page_id;
    if (isset($_GET['Multimedia']))
      $model->attributes = $_GET['Multimedia'];

    $this->render('_listing', array(
        'model' => $model,
    ));
  }

  /**
   * Manages all models.
   */
  public function actionAdmin() {
    $model = new Multimedia('search');
    $model->unsetAttributes();  // clear any default values
    if (isset($_GET['Multimedia']))
      $model->attributes = $_GET['Multimedia'];

    $this->render('admin', array(
        'model' => $model,
    ));
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id) {
    $model = Multimedia::model()->findByPk($id);
    if ($model === null)
      throw new CHttpException(404, 'The requested page does not exist.');
    return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */
  protected function performAjaxValidation($model) {
    if (isset($_POST['ajax']) && $_POST['ajax'] === 'multimedia-form') {
      echo CActiveForm::validate($model);
      Yii::app()->end();
    }
  }

}
