<?php

class NewsletterController extends Controller {

  public $layout = '//layouts/column2';
  public $menu2;
  public $page_id;

  public function filters() {
    return array(
        'accessControl',
    );
  }

  public function accessRules()
	{
		return array(
			array('allow',
				'roles'=>array('CmsUser')
			),
      array('deny',
				'users'=>array('*'),
			),
		);
	}

  public function actionIndex($page_id) {
    $page = Page::model()->findByPk($page_id);
    $this->page_id = $page->id;
    $this->setPageTitle(Yii::t('cms', 'Lista wiadomości e-mail'));

    $newsletter = new Newsletter('search');
    $newsletter->unsetAttributes();
    if (isset($_GET['Newsletter']))
      $newsletter->attributes = $_GET['Newsletter'];

    $this->render('index', array(
        'page' => $page,
        'newsletter' => $newsletter
    ));
  }

  public function actionCreate($page_id) {
    $page = Page::model()->findByPk($page_id);
    $this->page_id = $page->id;
    $this->setPageTitle(Yii::t('cms', 'Stwórz nową wiadomość e-mail'));
    $newsletter = new Newsletter('create');
    if (Yii::app()->request->isPostRequest) {
      $newsletter->setAttributes($_POST['Newsletter']);
      if ($newsletter->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Dane zostały zapisane.'));
        $this->redirect($this->createUrl('newsletter/index', array('page_id' => $newsletter->page_id)));
      }
    }
    $this->render('create', array(
        'page' => $page,
        'newsletter' => $newsletter
    ));
  }

  public function actionView($id) {
    $this->redirect($this->createUrl('newsletter/edit',array('id'=>$id)));
    Yii::app()->end();
    $newsletter = Newsletter::model()->findByPk($id);
    $this->render('tabs', array(
        'page' => $newsletter->page,
        'newsletter' => $newsletter,
        'tabs'=>$this->getTabs($newsletter),
        'additionalParams' => array()
    ));
  }

  public function actionEdit($id) {
    $newsletter = Newsletter::model()->findByPk($id);
    $this->page_id = $newsletter->page->id;
    $this->setPageTitle(Yii::t('cms', 'Edycja treści wiadomości'));
    $newsletter->setScenario('edit');
    if(Yii::app()->request->isPostRequest) {
      $newsletter->setAttributes($_POST['Newsletter']);
      if($newsletter->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Dane zostały zapisane.'));
        $this->redirect($this->createUrl('newsletter/edit', array('id' => $newsletter->id)));
      }
    }
    $this->render('tabs', array(
        'page' => $newsletter->page,
        'newsletter' => $newsletter,
        'tabs'=>$this->getTabs($newsletter),
        'additionalParams' => array()
    ));
  }

  public function actionAttachments($id) {
    $newsletter = Newsletter::model()->findByPk($id);
    $this->setPageTitle(Yii::t('cms', 'Załączniki wiadomości e-mail'));
    $this->page_id = $newsletter->page->id;
    $this->render('tabs', array(
        'page' => $newsletter->page,
        'newsletter' => $newsletter,
        'tabs'=>$this->getTabs($newsletter),
        'additionalParams' => array()
    ));
  }

  public function actionFiles($id) {
    $this->redirect($this->createUrl('newsletter/attachments',array('id'=>$id)));
  }

  public function actionRecipients($id) {
    $newsletter = Newsletter::model()->findByPk($id);
    $this->page_id = $newsletter->page->id;
    $this->setPageTitle(Yii::t('cms', 'Lista odbiorców wiadomości e-mail'));
    $newsletterRecipient = new NewsletterRecipient('search');
    $newsletterRecipient->newsletter_id = $id;

    $newsletterGroup = new NewsletterGroup('search');
    $recipient = new Recipient('search');

    if(isset($_GET['Recipient'])) $recipient->setAttributes($_GET['Recipient']);
    if(isset($_GET['NewsletterGroup'])) $newsletterGroup->setAttributes($_GET['NewsletterGroup']);

    $params = array('newsletterRecipient'=>$newsletterRecipient,'recipient'=>$recipient, 'newsletterGroup'=>$newsletterGroup);
    $this->render('tabs', array(
        'page' => $newsletter->page,
        'newsletter' => $newsletter,
        'tabs'=>$this->getTabs($newsletter),
        'additionalParams'=>$params
    ));
  }

  public function actionStats($id) {
    $newsletter = Newsletter::model()->findByPk($id);
    $this->page_id = $newsletter->page->id;
    $this->setPageTitle(Yii::t('cms', 'Statystyka wysyłania wiadomości e-mail'));
    $newsletterRecipient = new NewsletterRecipient('search');
    $newsletterRecipient->newsletter_id = $id;

    $newsletterGroup = new NewsletterGroup('search');
    $recipient = new Recipient('search');

    if(isset($_GET['Recipient'])) $recipient->setAttributes($_GET['Recipient']);
    if(isset($_GET['NewsletterGroup'])) $newsletterGroup->setAttributes($_GET['NewsletterGroup']);
    if(isset($_GET['NewsletterRecipient'])) $newsletterRecipient->setAttributes($_GET['NewsletterRecipient']);

    $params = array('newsletterRecipient'=>$newsletterRecipient,'recipient'=>$recipient, 'newsletterGroup'=>$newsletterGroup);
    $this->render('tabs', array(
        'page' => $newsletter->page,
        'newsletter' => $newsletter,
        'tabs'=>$this->getTabs($newsletter),
        'additionalParams' => $params
    ));
  }

  public function getTabs($newsletter) {
    $possibleTabs = array(
//        'view' => array(
//            'translated' => Yii::t('cms', 'Informacje podstawowe'),
//            'url' => $this->createUrl('newsletter/view', array('id' => $newsletter->id)),
//            'view' => 'view'),
        'edit' => array(
            'translated' => Yii::t('cms', 'Edycja'),
            'url' => $this->createUrl('newsletter/edit', array('id' => $newsletter->id)),
            'view' => 'edit'),
        'attachments' => array(
            'translated' => Yii::t('cms', 'Załączniki'),
            'url' => $this->createUrl('newsletter/attachments', array('id' => $newsletter->id)),
            'view' => 'attachments'),
        'recipients' => array(
            'translated' => Yii::t('cms', 'Odbiorcy'),
            'url' => $this->createUrl('newsletter/recipients', array('id' => $newsletter->id)),
            'view' => 'recipients'),
        'stats' => array(
            'translated' => Yii::t('cms', 'Statystyki'),
            'url' => $this->createUrl('newsletter/stats', array('id' => $newsletter->id)),
            'view' => 'stats'),
        'testSend' => array(
            'translated' => Yii::t('cms', 'Wyślij testową wiadomość'),
            'url' => $this->createUrl('newsletter/testSend', array('id' => $newsletter->id)),
            'view' => 'testSend'),

    );
    if($newsletter->status!=2) {
      $possibleTabs = $possibleTabs+array('statusSwitch' => array(
            'translated' => ($newsletter->status==0)? Yii::t('cms', 'Rozpocznij wysyłkę'):Yii::t('cms', 'Wstrzymaj wysyłkę'),
            'url' => $this->createUrl('newsletter/statusSwitch', array('id' => $newsletter->id)),
            'view' => 'statusSwitch'));
    }
    $tabs = array();
    foreach ($possibleTabs as $action => $translated) {
      if (Yii::app()->getController()->action->id == $action) {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'view' => $translated['view'],
        );
      } else {
        $tabs[$action] = array(
            'title' => Yii::t('cms', $translated['translated']),
            'url' => $translated['url'],
        );
      }
    }
    return $tabs;
  }

  public function actionStatusSwitch($id) {
    $newsletter = Newsletter::model()->findByPk($id);
    if($newsletter->status==0) $newsletter->status = 1;
    elseif($newsletter->status==1) $newsletter->status=0;
    if($newsletter->save()) {
      if($newsletter->status==0)
        Yii::app()->user->setFlash('notice', Yii::t('cms', 'Wiadomość jest w tej chwili wstrzymana.'));
      elseif($newsletter->status==1)
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Wiadomość jest w tej chwili wysyłana.'));
      $this->redirect($this->createUrl('newsletter/index',array('page_id'=>$newsletter->page_id)));
    }
    $this->page_id = $newsletter->page->id;
    $this->render('tabs', array(
        'page' => $newsletter->page,
        'newsletter' => $newsletter,
        'tabs'=>$this->getTabs(Newsletter::model()->findByPk($id)),
        'additionalParams' => array()
    ));
  }

  public function actionNewsletterGroups($newsletter_id) {
    $newsletter = Newsletter::model()->findByPk($newsletter_id);
    $this->page_id = $newsletter->page->id;
    $groups = Group::model()->findAll();
    $this->renderPartial('newsletterGroups', array(
        'newsletter'=>$newsletter,
        'newsletterGroups'=>$newsletter->groups,
        'groups'=>$groups
    ),false,true);
  }

  public function actionSwitchNewsletterGroup($newsletter_id,$group_id) {
    $group = Group::model()->findByPk($group_id);
    $params = array('newsletter_id'=>$newsletter_id,'group_id'=>$group_id);
    $newsletterGroup = NewsletterGroup::model()->findByPk($params);
    if($newsletterGroup) {
      if($newsletterGroup->delete()) {
        Yii::app()->user->setFlash('success', '<q>'.$group->name.'</q> - '.Yii::t('cms', 'Grupa odbiorców została usunięta z tej wiadomości.'));
      }
      else {
        Yii::app()->user->setFlash('error', '<q>'.$group->name.'</q> - '.Yii::t('cms', 'Nie można wypisać tej grupy odbiorców.'));
      }
      $this->redirect($this->createUrl('newsletter/NewsletterGroups',array('newsletter_id'=>$newsletter_id)));
    }
    else {
      if(NewsletterGroup::saveNew($newsletter_id, $group_id)) {
        Yii::app()->user->setFlash('success', '<q>'.$group->name.'</q> - '.Yii::t('cms', 'Grupa odbiorców została dopisana do tej wiadomości.'));
      }
      else {
        Yii::app()->user->setFlash('error', '<q>'.$group->name.'</q> - '.Yii::t('cms', 'Nie można dopisać tej grupy odbiorców.'));
      }
      $this->redirect($this->createUrl('newsletter/NewsletterGroups',array('newsletter_id'=>$newsletter_id)));
    }
  }

  public function actionGroupsToSelectWhileRecipientsImport() {
    $groups = Group::model()->findAll();
    $this->renderPartial('groupsToSelectWhileRecipientsImport', array(
        'groups'=>$groups,
        'groupsToSelect'=>  explode(',', $_POST['groupsToSelect'])
    ),false,true);
  }

  public function actionTestSend($id) {
    $newsletter = Newsletter::model()->findByPk($id);
    $this->setPageTitle(Yii::t('cms', 'Wyślij testową wiadomość na wskazany adres e-mail'));
    $this->page_id = $newsletter->page_id;
    $emails = array();
    if(!$newsletter->sender_id) {
      Yii::app()->user->setFlash('error', Yii::t('cms', 'Nie można wysyłać testowych wiadomości - ustaw najpierw nadawcę wiadomości.'));
      if(!Yii::app()->request->isAjaxRequest)
        $this->redirect ($this->createUrl('newsletter/testSend',array('id'=>$id,'ajax'=>true)));
    }
    else {
    $emails = (isset($_POST['emails']))? $_POST['emails']:Varable::pickUp('testEmails',array());
    if($emails) {
      preg_match_all(emailRegexp(),$emails,$matches);
      $emails = $matches[0];
      $emails = array_unique($emails);
    }
    if(!is_array($emails)) $emails = array();

    if(Yii::app()->request->isPostRequest) {
      if(!$emails) {
        $newsletter->addError('id', Yii::t('cms', 'Podaj chociaż jeden adres email na który ma zostać wysłana wiadomość.'));
      }
      else {
        Varable::saveVarable('testEmails', implode("\n", $emails));
        $msg = array();
        foreach ($emails as $email) {
          $sendResult = NewsletterRecipient::sendEmail($newsletter->sender, $email, $newsletter->title, $newsletter->body_text, $newsletter->body_html, $newsletter->files);
          if($sendResult) array_push ($msg, $email.' - wysłano wiadomość.');
        }
        Yii::app()->user->setFlash('success', implode('<br />', $msg));
        $this->redirect($this->createUrl('newsletter/testSend',array('id'=>$newsletter->id)));
      }
    }
    }
$this->render('tabs', array(
        'page' => $newsletter->page,
        'newsletter' => $newsletter,
        'tabs'=>$this->getTabs($newsletter),
        'additionalParams' => array('emails'=>$emails),

    ));
//    $this->renderPartial('testSend', array(
//        'newsletter'=>$newsletter,
//        'emails'=>$emails
//    ),false,true);
  }

  public function actionSenderSelect($id,$selectedId) {
    $newsletter = Newsletter::model()->findByPk($id);
    if($selectedId) $newsletter->sender_id = $selectedId;
    $this->renderPartial('senderSelect', array(
        'newsletter'=>$newsletter
    ),false,true);
  }

  public function actionAddSender($id) {
    $newsletter = Newsletter::model()->findByPk($id);
    $sender = new Sender();
    if(Yii::app()->request->isPostRequest) {
      $sender->setAttributes($_POST['Sender']);
      if($sender->save()) {
        //Yii::app()->user->setFlash('success', Yii::t('cms', 'Nadawca został dodany.'));
      }
    }

    $this->renderPartial('addSender', array(
        'newsletter'=>$newsletter,
        'sender'=>$sender,
    ),false,true);
  }

  public function actionSenders($page_id) {
    $this->page_id = $page_id;
    $this->setPageTitle(Yii::t('cms', 'Zarządzanie nadawcami wiadomosci'));
    $this->render('senders', array('page'=>Page::model()->findByPk($page_id)));
  }

  public function actionSenderEdit($id) {
    $sender = Sender::model()->findByPk($id);
    if(Yii::app()->request->isPostRequest) {
      $sender->setAttributes($_POST['Sender']);
      if($sender->save()) {
        Yii::app()->user->setFlash('success', Yii::t('cms', 'Zapisano pomyślnie.'));
        $this->refresh();
      }
    }
    $this->renderPartial('senderEdit', array(
        'sender'=>$sender,
    ),false,true);
  }

}