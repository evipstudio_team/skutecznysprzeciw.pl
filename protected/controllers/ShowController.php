<?php

class ShowController extends Controller {

  var $menuBlock;
  var $menuFooter;

  public function init() {
    if(!Yii::app()->user->isGuest && !Yii::app()->getRequest()->isSecureConnection) {
      $this->redirect($this->createAbsoluteUrl('show/index',array(),'https').preg_replace('/^\//','',$_SERVER['REQUEST_URI']));
    }
    Yii::app()->theme = 'default';
    Yii::app()->setComponents(array(
        'errorHandler'=>array(
            'errorAction'=>'show/error',
        ),
        'widgetFactory' => array(
            'widgets' => array(
                'CLinkPager' => array(
                    'cssFile' => Yii::app()->theme->baseUrl . '/css/pager.css',
                    'header'=>'',
                    'prevPageLabel'=>Yii::t('yii','Poprzednia'),
                    'nextPageLabel'=>Yii::t('yii','Następna'),

                ),
                'CListView' => array(
                    'summaryText'=>'{start}-{end} z {count}'

                ),
            ),
            )), true);

    $criteria = new CDbCriteria();
    $dependency = new CDbCacheDependency('SELECT MAX(last_update) FROM `varables`');
    $criteria->addInCondition('`name`', array('MetaDescription','MetaKeywords','MetaTitle','FooterAddress'));
    $varables = Varable::model()->cache(3600,$dependency)->findAll($criteria);

    $defaultVarables = array();
    foreach($varables as $varable) $defaultVarables[$varable->name] = $varable->value;
    Yii::app()->setParams(array('varables'=>$defaultVarables));

    $cs=Yii::app()->clientScript;
    $cs->scriptMap=array(
//        'jquery.js'=>$this->createAbsoluteUrl('show/index',array(),'https').Yii::app()->theme->baseUrl.'/js/jquery.min.js',
//        'jquery.min.js'=>$this->createAbsoluteUrl('show/index',array(),'https').Yii::app()->theme->baseUrl.'/js/jquery.min.js',
//        'jquery-ui-i18n.min.js'=>$this->createAbsoluteUrl('show/index').Yii::app()->theme->baseUrl.'/js/jquery-ui-i18n.min.js',
//        'jquery-ui-i18n.min.js'=>$this->createAbsoluteUrl('show/index').Yii::app()->theme->baseUrl.'/js/jquery-ui-i18n.min.js',
//        'jquery.prettyPhoto.js'=>Yii::app()->theme->baseUrl.'/js/all.js',
//        'jquery-ui-timepicker-addon.js'=>Yii::app()->theme->baseUrl.'/js/all.js',

    );
  }

  public function urlhistory() {
    $urls = isset(Yii::app()->session['urls'])? Yii::app()->session['urls']:array();
    if(!isset($urls[0]) || $urls[0]!=getCurrentUrl()) {
      array_unshift($urls, getCurrentUrl());
    }
    if(count($urls)>=2) unset ($urls[2]);
    $urls = Yii::app()->session['urls'] = $urls;
    $this->setPageState('urlHistory', $urls);
  }

  public function actions() {
    return array(
        'captcha' => array(
            'class' => 'CCaptchaAction',
            'backColor' => 0xededed,
            'width' => 80,
            'height' => 35,
        ),
    );
  }

  public function filters() {
    return array(
        'accessControl', // perform access control for CRUD operations
    );
  }

  public function accessRules() {
    return array(
        array('allow',
            'actions' => array('index', 'homeByUrl',
                'ContactForm',
                'Error',
                'Captcha',
                'Login',
                'clearCache'
                ),
            'users' => array('*')
        ),
        array('allow',
            'actions' => array('step2'),
            'expression'=>array($this,'isUserCause'),
        ),
        array('allow',
            'actions' => array('cause'),
            'expression'=>array($this,'isUserCause'),
        ),
        array('allow',
            'actions' => array('logout'),
            'expression' => '!$user->isGuest'
        ),
        array('allow',
            'actions' => array('clientDataForm'),
            'expression' => '!$user->isGuest'
        ),
        array('allow',
            'actions' => array('causes'),
            'expression' => '!$user->isGuest'
        ),
        array('allow',
            'actions' => array('GetFile'),
            'roles'=>array(
                'Show:GetFile'=>array('hash2'=>isset($_GET['hash2'])? $_GET['hash2']:null)
            )
        ),
        array('deny',
          'users'=>array('*'),
        ),
    );
  }

  public function actionError() {
    $this->layout = '//layouts/error';
    if ($error = Yii::app()->errorHandler->error) {
        $this->render('error', array('error'=>$error));
    }
  }

  public function isUserCause($user, $rule) {
    $userId = getUserId();
    if($userId) {
      $cause = Cause::model()->find('`id`=:id AND `user_id`=:user_id',array(':id'=>$_GET['causeId'],':user_id'=>$userId));
      if($cause) return true;
      else return false;
    }
    else return false;
  }

  public function setTitle($url) {
    if ($url && $url->title)
      $this->setPageTitle($url->title);
    else
      $this->setPageTitle(Yii::app()->params['varables']['MetaTitle']);
  }

  public function setDescription($url) {
    if ($url && $url->description)
      $this->setPageState('description', $url->description);
    else
      $this->setPageState('description', Yii::app()->params['varables']['MetaDescription']);
  }

  public function setKeywords($url) {
    if ($url && $url->keywords)
      $this->setPageState('keywords', $url->keywords);
    else
      $this->setPageState('keywords', Yii::app()->params['varables']['MetaKeywords']);
  }

  public function setMeta($url) {
    $this->setTitle($url);
    $this->setDescription($url);
    $this->setKeywords($url);
  }

  public function actionIndex() {
    if(!Yii::app()->user->isGuest) $this->unsetCauseId();
    $this->setMeta(false);
    $pageId = 87;
    $page = Page::model()->with('article','files')->findByPk($pageId);
    $this->page_id = $pageId;
    $this->render('article', array('page'=>$page));
  }

  public function actionContactForm() {
    Yii::app()->clientScript->scriptMap['jquery.js'] = false;
    $contactForm = new ContactForm;
    if (isset($_POST['ContactForm'])) {
      $contactForm->attributes = $_POST['ContactForm'];
      if ($contactForm->validate()) {
        $commit = true;
        $transaction = Yii::app()->db->beginTransaction();
        //załóż konto użytkownika
        if(!Yii::app()->user->isGuest) {
          $user = user::model()->findByPk(Yii::app()->user->id);
        }
        else {
          $user = User::model()->find('email=:email AND status=:status', array(':email' => $contactForm->email, ':status' => 'new'));
          if (!$user) {
            $user = new User('registration');
            $nameAndSurname = explode(' ', $_POST['ContactForm']['name']);
            $password = generatePassword();
            $user->setAttributes(array(
                'name' => $nameAndSurname[0],
                'surname' => isset($nameAndSurname[1]) ? $nameAndSurname[1] : '',
                'email' => $_POST['ContactForm']['email'],
                'password' => $password,
                'repeat_password' => $password,
                'role' => 'client',
                'terms' => 1,
                'phone' => $_POST['ContactForm']['phone'],
//                'verifyCode' => $_POST['ContactForm']['verifyCode']
            ));
            if (!$user->save()) {
              $commit = false;
            }
          }
        }
        //$cause = Cause::model()->find('user_id=:user_id AND rule_date=:rule_date',array(':user_id'=>$user->id,':rule_date'=>$_POST['ContactForm']['date']));
        //if (!$cause) {
        if (1) {
          //dodaj zgłoszenie
          $cause = new Cause();
          $cause->setAttributes(array(
              'user_id' => $user->id,
              'rule_date' => $_POST['ContactForm']['date']
          ));
          if (!$cause->save()) {
            $commit = false;
          } else {
            Email::newCauseInformation(Varable::pickUp('ContactEmail'), $cause);

            if ($contactForm->body && $contactForm->body!=$contactForm->defaultValue('body')) {
              $causeComment = $cause->comments[0];
              $causeComment->comment .= ' Dodatkowa informacja: ' . $contactForm->body;
              $causeComment->save();
            }
            $causeComment = new CauseComment();
            $causeComment->setAttributes(array(
                'cause_id' => $cause->id,
                'created_by' => $cause->user_id,
                'created_at' => date('Y-m-d H:i:s'),
                'comment' => 'Oczekuję na realizację kroku 2.',
                'step' => 2
                    ), false);
            $causeComment->save();
          }
        }
        if ($commit) {
          $transaction->commit();
          CCaptcha::reset_captcha();
          Yii::app()->user->setState('unloggedId', $user->id);
          Yii::app()->user->setState('unloggedCauseId', $cause->id);
          Yii::app()->user->setFlash('success', Yii::t('cms', 'Dziękujemy, zgłoszenie zostało przyjęte, prosimy o realizację kroku 2, co pozwoli Nam na dokładne przeanalizowanie Twojej sprawy.
            <script type="text/javascript">
                $(function() {
                  $("#FlashDialog").dialog({modal: true, width:600, closeOnEscape: false,
                    buttons: { \'Przejdź do kroku 2\': function() { window.location.href = \'' . $this->createAbsoluteUrl('show/step2',array('causeId'=>$cause->id),'https') . '\' } }
                  });
                  $("#FlashDialog").dialog(\'open\');
                });
              </script>
            '));
        } else {
          $transaction->rollback();
          Yii::app()->user->setFlash('error', Yii::t('cms', 'Przepraszamy, wystąpił błąd. Prosimy kontakt na adres administrator@skutecznysprzeciw.pl w celu wyjaśnienia sytuacji.'));
        }
        $this->refresh();
      }
    }

    $this->renderPartial('ContactForm', array(
        'model' => $contactForm,
            ), false, true);
  }

  public function actionStep2($causeId) {
    if(!Yii::app()->getRequest()->isSecureConnection) {
      $this->redirect($this->createAbsoluteUrl('show/step2',array('causeId'=>$causeId),'https'));
    }
    $page = Page::model()->findByPk(101);
    $this->setMeta($page->url);
    $step2Form = new Step2Form();
    Yii::app()->user->setState('unloggedCauseId',$causeId);
    //$causeId = Yii::app()->user->getState('unloggedCauseId');
    $cause = Cause::model()->findBypk($causeId);
    $userId = getUserId();
    if(isset($_POST['Step2Form'])) {
      if(isset($_POST['Step2Form']))
        $step2Form->setAttributes($_POST['Step2Form']);
      if($step2Form->saveFiles) {
        $step2Form->file = CUploadedFile::getInstance($step2Form,'file');
        $step2Form->file2 = CUploadedFile::getInstance($step2Form,'file2');
        $step2Form->file3 = CUploadedFile::getInstance($step2Form,'file3');
      }
      if($step2Form->validate() && $step2Form->serve($cause->id,$userId)) {
        if($step2Form->saveFiles) $comment = 'Użytkownik przesłał dokument.';
        else $comment = 'Użytkownik przesłał elektroniczny kod dostępu.';
        $causeComment = new CauseComment();
        $causeComment->setAttributes(array(
            'cause_id' => $cause->id,
            'created_by' => $cause->user_id,
            'created_at' => date('Y-m-d H:i:s'),
            'comment' => $comment,
            'step' => 3
                ), false);
        $causeComment->save();
        Yii::app()->user->setFlash('success','W ciągu 24 godzin dokonamy bezpłatnej analizy Twojej sprawy oraz skontaktujemy się z Tobą.
          <script type="text/javascript">
                $(function() {
                  $("#FlashDialog").dialog({modal: true, width:600, closeOnEscape: false,
                    buttons: { \'Ok\': function() { $(this).dialog(\'close\') } }
                  });
                  $("#FlashDialog").dialog(\'open\');
                });
              </script>
');
        if(Yii::app()->user->isGuest)
          $this->redirect($this->createUrl('show/index'));
        else
          $this->redirect($this->createUrl('show/cause',array('causeId'=>$cause->id)));
      }
    }
    $this->render('step2', array(
        'page'=>$page,
        'step2Form'=>$step2Form
    ));
  }

  public function actionhomeByUrl($address) {
    if(!Yii::app()->user->isGuest) $this->unsetCauseId();
    $this->urlhistory();
    $url = Url::model()->with('page','page.article','page.files')->find('`address`=:address', array(':address' => $address));
    if ($url) {
      $this->setMeta($url);
      $page = $url->page;
      $this->page_id = $page->id;
      $template = $page->view_template? $page->view_template:'article';
      if($template=='blog' || $page->parent_id == 103) {
        $this->page_id = 103;
        $this->layout = 'blog';
        $mainPage = Page::model()->findByPk(103);
        $searchPage = new Page();
        $searchPage->parent_id = $mainPage->id;
        $this->render('blog',array('page'=>$page, 'mainPage'=>$mainPage, 'searchPage'=>$searchPage));
      } else if ($template=='nasze-wyroki' || $page->parent_id == 145) {
          $this->layout = 'nasze-wyroki';
          $mainPage = Page::model()->findByPk(145);
          $searchPage = new Page();
          $searchPage->parent_id = $mainPage->id;
          $this->render('nasze-wyroki',array('page'=>$page, 'mainPage'=>$mainPage, 'searchPage'=>$searchPage));
      }
      else {
        $this->render($template,array('page'=>$page));
      }


    } else {
      $this->redirect('/');
    }
  }

  public function actionLogin() {
    $model = new LoginForm;
    if (isset($_POST['LoginForm'])) {
      $model->attributes = $_POST['LoginForm'];
      if ($model->validate() && $model->login()) {
        Yii::app()->user->setFlash('error', 'Jesteś zalogowany, zostałeś przekierowany na listę Twoich zgłoszeń. <script type="text/javascript">
                $(function() {
                  $("#FlashDialog").dialog({modal: true, width:600, closeOnEscape: false,
                    buttons: { \'Ok\': function() { $(this).dialog(\'close\') } }
                  });
                  $("#FlashDialog").dialog(\'open\');
                });
              </script>');
        $this->redirect($this->createAbsoluteUrl('show/causes', array(), 'https'));
      } else {
        Yii::app()->user->setFlash('error', 'Logowanie nie powiodło się. <script type="text/javascript">
                $(function() {
                  $("#FlashDialog").dialog({modal: true, width:600, closeOnEscape: false,
                    buttons: { \'Ok\': function() { $(this).dialog(\'close\') } }
                  });
                  $("#FlashDialog").dialog(\'open\');
                });
              </script>');
        $this->redirect($this->createAbsoluteUrl('show/index', array(), 'http'));
      }
    } else {
      $this->redirect($this->createAbsoluteUrl('show/index', array(), 'http'));
    }
  }

  public function actionLogout() {
    Yii::app()->user->logout();
    $this->redirect($this->createAbsoluteUrl('show/index',array(),'http'));
  }

  public function unsetCauseId() {
    Yii::app()->user->setState('unloggedCauseId', null);
  }

  public function unsetUnloggedUserId() {
    Yii::app()->user->setState('unloggedId', null);
  }

  public function actionCause($causeId) {
    Yii::app()->user->setState('newCause',null);
    Yii::app()->user->setState('unloggedCauseId', $causeId);
    $cause = Cause::model()->findByPk($causeId);
    $timeLine = $cause->prepareTimeLineView();

    $causeContactForm = new CauseContactForm();
    if(isset($_POST['CauseContactForm'])) {
      $causeContactForm->setAttributes($_POST['CauseContactForm']);
      $causeContactForm->file = CUploadedFile::getInstance($causeContactForm,'file');
      if($causeContactForm->validate() && $causeContactForm->serve($cause)) {
        Email::newCauseComment(Varable::pickUp('ContactEmail'), $cause);
        Yii::app()->user->setFlash('success','Dziękujemy, Twoj kontakt został zapisany. <script type="text/javascript">
                $(function() {
                  $("#FlashDialog").dialog({modal: true, width:600, closeOnEscape: false,
                    buttons: { \'Ok\': function() { $(this).dialog(\'close\') } }
                  });
                  $("#FlashDialog").dialog(\'open\');
                });
              </script>');
        $this->refresh();
      }
    }
//    print_r($timeLine);
    $this->render('cause', array('cause'=>$cause, 'timeLine'=>$timeLine, 'causeContactForm'=>$causeContactForm));
  }

  public function actionClientDataForm() {
    $clientDataForm = new ClientDataForm();
    if(isset($_POST['ClientDataForm'])) {
      $clientDataForm->setAttributes($_POST['ClientDataForm']);
      if($clientDataForm->validate() && $clientDataForm->serve()) {
        Yii::app()->user->setFlash('success','Dziękujemy, zmiany zostały zapisane. <script type="text/javascript">
                $(function() {
                  $("#FlashDialog").dialog({modal: true, width:600, closeOnEscape: false,
                    buttons: { \'Ok\': function() { $(this).dialog(\'close\') } }
                  });
                  $("#FlashDialog").dialog(\'open\');
                });
              </script>');
        $this->refresh();
      }
    }
    else {
      $user = User::model()->findByPk(Yii::app()->user->id);
      $clientDataForm->setAttributes(array(
          'name'=>$user->name,
          'surname'=>$user->surname,
          'email'=>$user->email,
          'phone'=>$user->phone,
      ));
    }

    $this->render('clientDataForm',array('clientDataForm'=>$clientDataForm));
  }

  public function actionCauses() {
    Yii::app()->user->setState('newCause',null);
    $this->unsetCauseId();
    $user = User::model()->findByPk(Yii::app()->user->id);
    $causes = $user->causes(array('order'=>'created_at DESC'));
    $this->render('causes', array('user'=>$user,'causes'=>$causes));
  }

  public function actionGetFile($hash1, $hash2) {
    $causeFile = CauseFile::model()->find('`hidden_filename`=:hidden_filename',array(':hidden_filename'=>$hash2));
    if($causeFile) {
      if($causeFile->createLinkHash1()==$hash1) {
        $filepath = Yii::app()->basePath.'/'.CauseFile::getFolderPath($causeFile->cause_id).'/'.$causeFile->hidden_filename;
        if(file_exists($filepath)) {
          header('Content-type: '.mime_content_type2($causeFile->filename));
          header('Content-Disposition: attachment; filename="'.$causeFile->filename.'"');
          readfile($filepath);
          Yii::app()->end();
        }
      }
    }
    throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
  }

  public function actionClearCache() {
    $this->unsetCauseId();
    $this->unsetUnloggedUserId();
    Yii::app()->user->setState('newCause',true);
    Yii::app()->user->setFlash('success','Aby zgłosić kolejną sprawę wypełnij ponownie formularz kontaktowy.<script type="text/javascript">
                $(function() {
                  $("#FlashDialog").dialog({modal: true, width:600, closeOnEscape: false,
                    buttons: { \'Ok\': function() { $(this).dialog(\'close\') } }
                  });
                  $("#FlashDialog").dialog(\'open\');
                });
              </script>');
    $this->redirect($this->createUrl('show/index'));
  }

}
