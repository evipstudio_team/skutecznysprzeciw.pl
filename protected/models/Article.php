<?php

class Article extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'articles';
  }

  public function rules() {
    return array(
        array('page_id, lang_id, title', 'required'),
        array('content,short_content', 'default', 'setOnEmpty'=>true, 'value'=>null),
        array('title', 'length', 'max' => 255),
        array('article_id, lang_id, title, content, status', 'safe', 'on' => 'search'),
    );
  }

  public function relations() {
    return array(
        'lang' => array(self::BELONGS_TO, 'Lang', 'lang_id'),
        'page' => array(self::BELONGS_TO, 'Page', 'page_id'),
        'url' => array(self::HAS_ONE, 'Url', array('page_id','lang_id')),
    );
  }

  public function attributeLabels() {
    return array(
        'article_id' => 'Article',
        'lang_id' => 'Język',
        'title' => Yii::t('cms', 'Tytuł'),
        'content' => 'Treść',
        'short_content' => 'Treść skrócona',
    );
  }

  public function beforeValidate() {
    if ($this->short_content)
      $this->short_content = strip_tags($this->short_content);
    return parent::beforeValidate();
  }

  public function afterDelete() {
    $page = $this->page;
      if($page && $page->module_id==Module::getModuleId('Article') && !$page->articles) $page->delete();
    $url = $this->url;
      if($url) $url->delete();

    return parent::afterDelete();
  }


  public function search($pageIds) {
    $criteria = new CDbCriteria;
    $criteria->compare('title', $this->title, true);
    $criteria->addInCondition('`page_id`', $pageIds);

    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
                ),
            ));
  }

  public function beforeSave() {
    $this->buildLink();
    return parent::beforeSave();
  }

  public function buildLink() {
    $this->content = preg_replace('/ skutecznysprzeciw.pl/',' www.skutecznysprzeciw.pl',$this->content);
    $this->content = preg_replace('/ www.skutecznysprzeciw.pl/',' <a href="http://www.skutecznysprzeciw.pl">www.skutecznysprzeciw.pl</a>',$this->content);

    //$this->content = preg_replace('/ ([^ >]+@skutecznysprzeciw.pl)/',' <a href="mailto:$1">$1</a>',$this->content);
    //preg_match_all('/ ([^ >]+@skutecznysprzeciw.pl)/',$this->content, $matches);

//    $this->content = parseHyperlinks($this->content);
//    $this->content = parseEmails($this->content);
//var_dump($this->content);
//die();
  }

  public function afterSave() {

    $url = Url::model()->findByPk(array('page_id'=>$this->page_id,'lang_id'=>$this->lang_id));
    if(!$url) {
      $url = new Url();
      $url->setAttributes(array(
          'page_id'=>$this->page_id,
          'title'=>$this->title,
          'anchor'=>$this->title,
          'lang_id'=>$this->lang_id,
          'autoupdate'=>1
      ));
      $url->save();
    }
    elseif($url->autoupdate) {
      $url->setAttributes(array(
          'anchor'=>$this->title,
      ));

      $url->save();
    }
    return parent::afterSave();
  }

}