<?php

class BlockPage extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function relations() {
    return array(
        'page'=>array(self::BELONGS_TO,'page','page_id'),
        'block'=>array(self::BELONGS_TO,'Block','block_id')
    );
  }
  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'pages_blocks';
  }

  static public function createNew($params) {
    $blockPage = new BlockPage();
    $blockPage->setAttributes($params, false);
    $blockPage->save();
    return $blockPage;
  }

  public function search() {
    $criteria = new CDbCriteria;
    $criteria->compare('block_id', $this->block_id, true);
    $criteria->order = 'position';
    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
  }

  public function afterSave() {
    $block = $this->block->save();
    return parent::afterSave();
  }

}