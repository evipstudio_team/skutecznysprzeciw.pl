<?php

class CauseComment extends CActiveRecord {

  public $sendEmail;
  public $addAttachments;

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'cause_comments';
  }

  public function rules() {
    return array(
        array('cause_id, step', 'required'),
        array('created_by, created_at, comment', 'default', 'value' => null),
        array('addAttachments', 'safe'),
        array('sendEmail', 'required', 'on' => 'addComment'),
    );
  }

  public function relations() {
    return array(
        'author' => array(self::BELONGS_TO, 'User', 'created_by'),
        'cause' => array(self::BELONGS_TO, 'Cause', 'cause_id'),
    );
  }

  public function attributeLabels() {
    return array(
        'step' => 'Następny krok',
        'comment' => 'Komentarz',
        'sendEmail' => 'Wyślij wiadomość email do użytkownika',
        'addAttachments' => 'Załącznik do wiadomości'
    );
  }

  public function beforeValidate() {
    if ($this->isNewRecord) {
      if($this->scenario == 'addComment' && $this->addAttachments && (!$this->sendEmail || ($this->sendEmail==2))) {
        $this->addError('addAttachments', "Nieporozumienie - mam dodać załączniki i jednocześnie nie wysyłać wiadomości?");
      }
      if($this->addAttachments && !$this->cause->filesCount) {
        $this->addError('addAttachments', "Sprawa nie posiada żadnych załączników.");
      }
      if (!$this->created_at) {
        $this->created_at = date('Y-m-d H:i:s');
      }
    }

    return parent::beforeValidate();
  }

  public function afterSave() {
    $cause = $this->cause;
//    $cause->setAttributes(array('actual_step'=>$this->step),false);
    $cause->actual_step = $this->step;
    $cause->last_response_date = $this->created_at;
    $cause->save();
//    $lastComment = $this->cause->lastComment;
    if ($this->created_by) {
//      $causeComment = new CauseComment();
//      $causeComment->setAttributes(array(
//          'cause_id'=>$this->cause_id,
//          'step'=>$this->step+1,
//      ));
//      $causeComment->save();
    }
    return parent::afterSave();
  }

}