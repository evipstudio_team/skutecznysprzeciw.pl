<?php

class CauseContactForm extends CFormModel {

  public $comment;
  public $file;

  public function rules() {
    return array(
        array('comment', 'safe'),
        array('file', 'file', 'types' => 'jpg, gif, png, pdf, doc, docx', 'allowEmpty' => true),
    );
  }

  public function attributeLabels() {
    return array(
        'comment' => 'Wiadomość',
        'file' => 'Dokument',
    );
  }

  public function beforeValidate() {
    if (!$this->comment && !$this->file) {
      $this->addError('comment', 'Proszę podać treść komunikatu i/lub plik');
    }
    return parent::beforeValidate();
  }

  public function serve($cause) {
    $commit = true;
    $transaction = Yii::app()->db->beginTransaction();
    if ($this->file) {
      $causeFile = new CauseFile();
      $causeFile->file = $this->file;
      $causeFile->cause_id = $cause->id;
      $causeFile->created_by = Yii::app()->user->id;
      if (!$causeFile->save())
        $commit = false;
    }
    $causeComment = new CauseComment();
    $causeComment->setAttributes(array(
        'cause_id' => $cause->id,
        'created_by' => Yii::app()->user->id,
        'created_at' => date('Y-m-d H:i:s'),
        'step' => $cause->actual_step
    ));
    if ($this->comment) {
      $causeComment->comment = $this->comment;
    } else {
      $causeComment->comment = 'Użytkownik przesłał dokument.';
    }
    if (!$causeComment->save())
      $commit = false;
    if ($commit) {
      $transaction->commit();
    } else {
      $transaction->rollback();
    }
    return $commit;
  }

}
