<?php

class CauseFile extends CActiveRecord {

  public $file;

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'cause_files';
  }

  public function rules() {
    return array(
        array('cause_id, filename, created_by, created_at, extension, hidden_filename', 'required'),
    );
  }

  public function relations() {
    return array(
        'cause' => array(self::BELONGS_TO, 'Cause', 'cause_id'),
        'author' => array(self::BELONGS_TO, 'User', 'created_by'),
    );
  }

  public function attributeLabels() {
    return array(
        'created_at'=>'Data utworzenia',
        'filename'=>'Nazwa pliku'
    );
  }

  public function beforeValidate() {
    if ($this->IsNewRecord) {
      $this->created_at = date('Y-m-d H:i:s');
      $this->seen_at = null;

      $this->extension = $this->file->ExtensionName;
      $this->filename = $this->file->Name;
      $this->hidden_filename = uniqid($this->cause_id);
      if (!$this->file->saveAs(Yii::app()->basePath . '/' . self::getFolderPath($this->cause_id) . '/' . $this->hidden_filename)) {
        $this->addError('file', $this->file->error);
      }
    }
    return parent::beforeValidate();
  }

  public function getImageTypes() {
    return array('jpg', 'png', 'jpeg', 'bmp', 'gif');
  }

  public function getDocumentTypes() {
    return array('doc', 'docx', 'xlsx', 'xls', 'pdf');
  }

  static public function getPossibleTypes() {
    return array('Image', 'Document');
  }

  public function getType() {
    foreach ($this->possibleTypes as $type) {
      $methodName = "get{$type}Types";
      if (in_array($this->extension, $this->$methodName())) {
        return $type;
      }
    }
    return 'Other';
  }

  public function getTypeTranslated($many = false) {
    return self::staticGetTypeTranslated($this->type, $many);
  }

  static public function staticGetTypeTranslated($type, $many = false) {
    switch ($type) {
      case 'Image':
        if ($many)
          return Yii::t('cms', 'Zdjęcia');
        else
          return Yii::t('cms', 'Zdjęcie');
        break;
      case 'Video':
        if ($many)
          return Yii::t('cms', 'Filmy');
        else
          return Yii::t('cms', 'Film');
        break;
      case 'Document':
        if ($many)
          return Yii::t('cms', 'Dokumenty');
        else
          return Yii::t('cms', 'Dokument');
        break;
      case 'Other':
        if ($many)
          return Yii::t('cms', 'Inne');
        else
          return Yii::t('cms', 'Inne');
        break;
      default:
        return $type;
        break;
    }
  }

  public function getMini() {
    if ($this->type == 'Image')
      return '<img src="' . $this->link(100) . '" alt="" title="" />';
    else
      return '<a href="' . $this->link() . '" target="_blank"><img src="/images/icons/preview.png" alt="" title="" /></a>';
  }

  public function link($width = false, $height = false) {
    $filepath = $this->path . $this->filename;
    if (file_exists($filepath)) {
      if ($this->type == 'Image' && ($width !== false || $height !== false)) {
        $href_path_format = '{FILENAME_WITHOUT_EXTENSION}';
        $save_path_base_format = dirname($filepath);
        $save_path_format = "{FILENAME_WITHOUT_EXTENSION}_{WIDTH}x{HEIGHT}.jpg";
        $filename_without_extension = basename($filepath, end(explode('.', $filepath)));
        $save_path_format = str_replace('{FILENAME_WITHOUT_EXTENSION}', $filename_without_extension, $save_path_format);

        if ($height !== false) {
          $save_path_format = str_replace('{HEIGHT}', $height, $save_path_format);
        } else {
          $save_path_format = str_replace('{HEIGHT}', '', $save_path_format);
        }
        if ($width !== false) {
          $save_path_format = str_replace('{WIDTH}', $width, $save_path_format);
        } else {
          $save_path_format = str_replace('{WIDTH}', '', $save_path_format);
        }
        $href_path_format = str_replace('{FILENAME_WITHOUT_EXTENSION}', $save_path_format, $href_path_format);
        if (!file_exists($save_path_base_format . '/' . $save_path_format)) {
          Yii::import('ext.simpleimage.simpleimage');
          $image = new simpleimage();
          $image->load($filepath);
          if ($height !== false && $width !== false) {
            if ($image->getWidth() > $width || $image->getHeight() > $height)
              $image->resize($height, $width);
          } elseif ($height !== false) {
            if ($image->getHeight() > $height)
              $image->resizeToHeight($height);
          } elseif ($width !== false) {
            if ($image->getWidth() > $width)
              $image->resizeToWidth($width);
          }
          $image->save($save_path_base_format . '/' . $save_path_format, IMAGETYPE_JPEG, 100, 0777);
          if (!file_exists($save_path_base_format . '/' . $save_path_format)) {
            die('Kompresja zdjęcia nie powiodła się');
          }
        }
        return '/' . $save_path_base_format . '/' . $save_path_format;
      } else {
        return '/' . $filepath;
      }
    }
    else
      return $filepath;
  }

  static public function getFolderPath($causeId) {
    $folderPath = "causeFiles/$causeId";
    if (!file_exists(Yii::app()->basePath . '/' . $folderPath)) {
      $oldumask = umask(0);
      mkdir(Yii::app()->basePath . '/' . $folderPath, 0777, true);
      umask($oldumask);
    }
    return $folderPath;
  }

  public function afterDelete() {
    $filepath = Yii::app()->basePath.'/'.CauseFile::getFolderPath($this->cause_id).'/'.$this->hidden_filename;
    if (file_exists($filepath))
      unlink($filepath);
    parent::afterDelete();
  }

  public function getFullPath() {
    return Yii::app()->basePath . '/../' . $this->path;
  }

  public function getMimeType() {
    return getMimeType($this->getFullPath() . $this->filename);
  }

  public function createLinkHash1() {
    return md5($this->cause_id . $this->created_by . $this->created_at . $this->hidden_filename);
  }

  public function createLinkHash2() {
    return $this->hidden_filename;
  }

  public function search() {
    $criteria = new CDbCriteria;
    $criteria->compare('`cause_id`', $this->cause_id);

    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
                ),
                'sort' => array(
                    'defaultOrder' => 'created_at DESC',
                ),
            ));
  }

}