<?php

class ClientDataForm extends CFormModel {

  public $name;
  public $surname;
  public $email;
  public $phone;
  public $password;
  public $password_repeat;

  public function rules() {
    return array(
        array('name, email, phone', 'required'),
        array('phone', 'numerical'),
        array('phone', 'match', 'pattern'=>'/^[0-9]{9,11}$/', 'message'=>'Proszę wprowadzić prawidłowy numer telefonu.'),
        array('email', 'email'),
        array('email', 'unique', 'className'=>'User','criteria'=>array('condition'=>'`id`!='.Yii::app()->user->id)),
        array('password', 'compare', 'compareAttribute' => 'password_repeat', 'on'=>'editPass'),
        array('password, password_repeat', 'length', 'max' => 40, 'min' => 6),

        array('surname, password_repeat', 'safe'),
    );
  }

  public function attributeLabels() {
    return array(
        'name' => 'Imię',
        'surname' => 'Nazwisko',
        'email'=>'Adres e-mail',
        'phone'=>'Numer telefonu',
        'password'=>'Hasło',
        'password_repeat'=>'Powtórzone hasło'
    );
  }

  public function beforeValidate() {
    $this->phone = preg_replace('/[^0-9]/', '', $this->phone);
    if($this->password_repeat) {
      $this->setScenario('editPass');
    }
    return parent::beforeValidate();
  }

  public function serve() {
    $commit = true;
    $transaction = Yii::app()->db->beginTransaction();

    $user = User::model()->findByPk(Yii::app()->user->id);
    $user->setScenario('edit');
    $user->name = $this->name;
    $user->surname = $this->surname;
    $user->email = $this->email;
    $user->phone = $this->phone;
    if($this->scenario == 'editPass') {
      $user->password = $this->password;
      $user->repeat_password = $this->password_repeat;
    }
    else {
      $user->password = '';
    }
    if(!$user->save()) {
      $commit = false;

    }

    if ($commit) {
      $transaction->commit();
    } else {
      $transaction->rollback();
    }
    return $commit;
  }

}
