<?php

class Gallery extends Page {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  static public function getModuleId() {
    return Module::model()->find('`name`=:name', array(':name' => 'Gallery'))->id;
  }

  public function tableName() {
    return 'pages';
  }

  public function search() {
    $criteria = new CDbCriteria;
    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
  }

}