<?php

class Group extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'groups';
  }

  public function rules() {
    return array(
        array('name', 'required'),
        array('name', 'length', 'max' => 255),
    );
  }

  public function relations() {
    return array(
        'recipientsActiveCount' => array(self::STAT, 'Recipient', 'recipient_group(group_id,recipient_id)', 'condition'=>'`status`=1'),
        'recipientsInActiveCount' => array(self::STAT, 'Recipient', 'recipient_group(group_id,recipient_id)', 'condition'=>'`status`=0')
    );
  }

  public function attributeLabels() {
    return array(
        'id' => 'ID',
        'name' => 'Nazwa',
        'recipientsActiveCount' => Yii::t('cms', 'Suma aktywnych odbiorców'),
        'recipientsInActiveCount' => Yii::t('cms', 'Suma wypisanych odbiorców'),
    );
  }

  public function search() {
    $criteria = new CDbCriteria;
    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
  }

  static public function pickUpDefaultGroupId($name = 'Newsletter') {
    $group = Group::model()->find('`name`=:name',array(':name'=>$name));
    if(!$group) {
      $group = new Group();
      $group->setAttributes(array(
          'name'=>$name
      ));
      $group->save();
    }
    return $group->id;
  }

}