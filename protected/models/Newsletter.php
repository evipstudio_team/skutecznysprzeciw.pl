<?php

class Newsletter extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  static public function getModuleId() {
    return Module::model()->find('`name`=:name', array(':name' => 'Newsletters'))->id;
  }

  public function tableName() {
    return 'newsletters';
  }

  public function relations() {
    return array(
        'page' => array(self::BELONGS_TO, 'Page', 'page_id'),
        'sender' => array(self::BELONGS_TO, 'Sender', 'sender_id'),
        'files' => array(self::HAS_MANY, 'Multimedia', 'element_id', 'order' => '`position`', 'condition' => 'module_id=:module_id', 'params' => array(':module_id' => Newsletter::getModuleId()), 'order' => 'position'),
        'groups' => array(self::MANY_MANY, 'Group', 'newsletter_group(newsletter_id, group_id)'),
        'recipientsCount' => array(self::STAT, 'NewsletterRecipient', 'newsletter_id'),
        'sendedCount' => array(self::STAT, 'NewsletterRecipient', 'newsletter_id', 'condition' => '`sent_at` IS NOT NULL')
    );
  }

  public function rules() {
    return array(
        array('page_id, title', 'required'),
        array('title', 'length', 'min' => 1, 'max' => 255),
        array('status', 'in', 'range' => array_keys($this->getEnumOptions('status')), 'allowEmpty' => false),
        array('body_text, body_html, sender_id', 'required', 'on' => 'edit'),
        array('title, created_at', 'safe', 'on' => 'search'),
        array('status', 'statusValidator'),
        array('body_text, body_html, sender_id', 'required', 'on' => 'statusSending'),
    );
  }

  public function statusValidator($attribute, $params) {
    if ($this->status == 1) {
      $this->setScenario('statusSending');
      $this->validate(array('body_text', 'body_html', 'sender_id'));
      if (!$this->recipientsCount)
        $this->addError('recipientsCount', Yii::t('cms', 'Wskaż odbiorców do których wiadomość ma zostać wysłana'));
    }
  }

  public function attributeLabels() {
    return array(
        'id' => 'ID',
        'page_id' => Yii::t('cms', 'Kategoria'),
        'title' => Yii::t('cms', 'Tytuł'),
        'created_at' => Yii::t('cms', 'Data utworzenia'),
        'body_html' => Yii::t('cms', 'Treść HTML'),
        'body_text' => Yii::t('cms', 'Treść tekstowa'),
        'sender_id' => Yii::t('cms', 'Nadawca wiadomości'),
        'recipientsCount' => Yii::t('cms', 'Suma odbiorców'),
        'sendedCount' => Yii::t('cms', 'Wysłanych'),
    );
  }

  public function getEnumOptions($fieldName) {
    $values = false;
    switch ($fieldName) {
      case 'status':
        $values = array(
            0 => Yii::t('cms', 'Niewysyłana'),
            1 => Yii::t('cms', 'W trakcie wysyłki'),
            2 => Yii::t('cms', 'Wysłana'),
        );
        break;
      default:
        break;
    }
    return $values;
  }

  public function beforeValidate() {
    if ($this->getIsNewRecord()) {
      $this->created_at = date('Y-m-d H:i:s');
      if (!isset($this->status))
        $this->status = 0;
    }
    if ($this->body_text)
      $this->body_text = strip_tags(br2nl($this->body_text));
    return parent::beforeValidate();
  }

  public function TranslateEnumValue($fieldName) {
    $values = $this->getEnumOptions($fieldName);
    if (isset($values[$this->$fieldName])) {
      return $values[$this->$fieldName];
    }
    else
      return $this->$fieldName;
  }

  public function search() {
    $criteria = new CDbCriteria;
    $criteria->compare('title', $this->title, true);
    $criteria->compare('status', $this->status, true);
    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'sort' => array(
                    'defaultOrder' => 'id DESC',
                ),
            ));
  }

  public function getBodyTextWithBr() {
    return nl2br($this->body_text);
  }

  static public function getSubMenu($page_id) {
    return array(
        array(
            'label' => Yii::t('cms', 'Lista wiadomości'),
            'url' => Yii::app()->createUrl('newsletter/index', array('page_id' => $page_id)),
            'active'=>(getCurrentUrl()==Yii::app()->createUrl('newsletter/index', array('page_id' => $page_id)))? true:false,
        ),
        array(
            'label' => Yii::t('cms', 'Stwórz nową wiadomość'),
            'url' => Yii::app()->createUrl('newsletter/create', array('page_id' => $page_id)),
            'active'=>(getCurrentUrl()==Yii::app()->createUrl('newsletter/create', array('page_id' => $page_id)))? true:false,
        ),
        array(
            'label' => Yii::t('cms', 'Zarządzaj odbiorcami'),
            'url' => Yii::app()->createUrl('recipient/index', array('page_id' => $page_id)),
            'active'=>(Yii::app()->getController()->id=='recipient')? true:false,
        ),
        array(
            'label' => Yii::t('cms', 'Zarządzaj nadawcami'),
            'url' => Yii::app()->createUrl('newsletter/senders', array('page_id' => $page_id)),
            'active'=>(getCurrentUrl()==Yii::app()->createUrl('newsletter/senders', array('page_id' => $page_id)))? true:false,
        ),
        array(
            'label' => Yii::t('cms', 'Edycja kategorii'),
            'url' => Yii::app()->createUrl('page/edit', array('id' => $page_id)),
            'active'=>(getCurrentUrl()==Yii::app()->createUrl('page/edit', array('id' => $page_id)))? true:false,
//            'visible' => Yii::app()->user->checkAccess('Administrator')
        ),
    );
  }

}