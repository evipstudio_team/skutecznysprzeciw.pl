<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class NewsletterAddForm extends CFormModel {

  public $email;
  public $confirm;

  public function init() {
    parent::init();
    $this->setDefaultValues();
  }

  public function getAttributes() {
    return get_class_vars(get_class($this));
  }

  public function defaultValue($fieldName) {
    switch ($fieldName) {
      case 'email':
        return Yii::t('cms', 'Podaj swój adres E-mail');
        break;

      default:
        break;
    }
  }

  public function setDefaultValues() {
    foreach ($this->attributes as $fieldName => $value)
      if (!$this->$fieldName)
        $this->$fieldName = $this->defaultValue($fieldName);
  }

  /**
   * Declares the validation rules.
   */
  public function rules() {
    return array(
        array('email', 'required'),
        array('confirm', 'required', 'requiredValue'=>1, 'message'=>  Yii::t('cms', 'Musisz zaznaczyć.')),
        array('email', 'email'),
    );
  }

  public function beforeValidate() {
    foreach ($this->attributes as $fieldName => $value) {
      if ($this->$fieldName == $this->defaultValue($fieldName))
        $this->$fieldName = '';
    }
    return parent::beforeValidate();
  }

  public function afterValidate() {
    $this->setDefaultValues();
    return parent::afterValidate();
  }

  /**
   * Declares customized attribute labels.
   * If not declared here, an attribute would have a label that is
   * the same as its name with the first letter in upper case.
   */
  public function attributeLabels() {
    return array(
        'email' => Yii::t('cms', 'Adres e-mail'),
    );
  }

  public function serve() {
    $transaction = Yii::app()->db->beginTransaction();
    $commit = true;

    $recipient = new Recipient();
    $recipient->setAttributes(array(
        'email'=>$this->email,
        'status'=>-1
    ));
    if($recipient->save()) {
      RecipientGroup::saveNew($recipient->id, Group::pickUpDefaultGroupId());
    }
    else {
      $commit = false;
    }
    if($commit) {
      $transaction->commit();
      $confirmationData = new ConfirmationData;
      $confirmationData->setAttributes(array(
          'obj_name' => 'Recipient',
          'field_name' => 'status',
          'obj_id' => $recipient->id,
          'new_value' => 1
              ), false);
      $confirmationData->save();
      Email::newsletterRecipientAdd($recipient->email, $confirmationData->checksum);
      return true;
    }
    else {
      $transaction->rollback();
      $this->addError('email', $recipient->getError('email'));
      return false;
    }
  }
}
