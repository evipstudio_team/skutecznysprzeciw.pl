<?php

class NewsletterGroup extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'newsletter_group';
  }

  public function rules() {
    return array(
        array('group_id','safe', 'on'=>'search')
    );
  }

  static public function saveNew($newsletter_id,$group_id) {
    $params = array('newsletter_id'=>$newsletter_id,'group_id'=>$group_id);
    if(!NewsletterGroup::model()->findByPk($params)) {
      $newsletterGroup = new NewsletterGroup();
      $newsletterGroup->setAttributes($params, false);
      return($newsletterGroup->save());
    }
    else return true;
  }

  public function afterSave() {
    $sql = "
      INSERT INTO newsletter_recipient (newsletter_id,recipient_id,count)
      SELECT $this->newsletter_id,`recipient_id`,1 FROM `recipient_group` INNER JOIN `recipients` ON `recipient_group`.`recipient_id`=`recipients`.`id` WHERE `recipient_group`.`group_id` = $this->group_id AND `recipients`.`status`=1
      ON DUPLICATE KEY UPDATE `count`=`count`+1
      ";
    Yii::app()->db->createCommand($sql)->query();
    return parent::afterSave();
  }

  public function afterDelete() {
    $sql = "
      UPDATE newsletter_recipient SET `count`=`count`-1 WHERE newsletter_id = $this->newsletter_id AND recipient_id IN (SELECT `recipient_id` FROM `recipient_group` WHERE `recipient_group`.`group_id` = $this->group_id)
      ";
    Yii::app()->db->createCommand($sql)->query();
    $sql = "DELETE FROM `newsletter_recipient` WHERE `sent_at` IS NULL AND `count` <= 0";
    Yii::app()->db->createCommand($sql)->query();
    return parent::afterDelete();
  }

}