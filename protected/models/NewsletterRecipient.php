<?php

class NewsletterRecipient extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'newsletter_recipient';
  }

  public function relations() {
    return array(
        'newsletter' => array(self::BELONGS_TO, 'Newsletter', 'newsletter_id'),
        'recipient' => array(self::BELONGS_TO, 'Recipient', 'recipient_id'),
    );
  }

  public function rules() {
    return array(
        array('sent_at', 'safe', 'on' => 'search')
    );
  }

  public function attributeLabels() {
    return array(
        'sent_at' => Yii::t('cms', 'Data dostarczenia'),
    );
  }

  public function search($email = false, $groupId = false) {
    $with = array();
    $criteria = new CDbCriteria;
    $criteria->compare('newsletter_id', $this->newsletter_id, true);
    if ($email) {
      array_push($with, 'recipient');
      $criteria->compare('email', $email, true);
    }
//    if($groupId) {
//      $with = array('recipient'=>array('with'=>'groups'));
//      $criteria->compare('group_id', $groupId, false);
//    }
    $criteria->with = $with;
    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
  }

  public function getSentAtNormalized() {
    if ($this->sent_at)
      return date('d/m/Y H:i', strtotime($this->sent_at));
    else
      return 'n/a';
  }

  public function send() {
    $newsletter = $this->newsletter;
    $recipient = $this->recipient;

    $this->run_count++;
    $this->save();

    if (!$this->sent_at && $recipient->status == 1) {
      $exitConfirmationData = $this->pickUpExitConfirmationData();
      $body_html = $this->insertExitMailingHref($newsletter->body_html, $exitConfirmationData);
      $result = self::sendEmail($newsletter->sender, $recipient->email, $newsletter->title, $newsletter->body_text, $body_html, $newsletter->files, false);
      if($result[0]) {
        $this->sent_at = date('Y-m-d H:i:s');
        $this->save();
      }
    }
  }

  static public function sendEmail($sender, $to, $subject, $body_text, $body_html, $files, $backupEmail = false) {
    Yii::import('ext.phpmailer.JPhpMailer');
    $phpmailer = new JPhpMailer;
    $phpmailer->IsSMTP();

    $phpmailer->Host = $sender->host;
    $phpmailer->Username = $sender->login;
    if($sender->sender)
      $phpmailer->Sender = $sender->sender;
    $phpmailer->Port = 587;
    $phpmailer->Password = $sender->password;
    $phpmailer->SetFrom($sender->email, $sender->name);
    $phpmailer->SMTPSecure = false;

    $phpmailer->SMTPAuth = true;
    $phpmailer->SMTPKeepAlive = true;
    $phpmailer->Mailer = "smtp";
    $phpmailer->SMTPAuth = true;
    $phpmailer->CharSet = 'utf-8';
    $phpmailer->SMTPDebug = 0;

    $phpmailer->AddAddress($to);

    $phpmailer->Subject = $subject;

    foreach ($files as $file) {
      $fileFullPath = $file->getFullPath() . $file->filename;
      if (file_exists($fileFullPath)) {
        $phpmailer->AddAttachment($fileFullPath, $file->filename, 'base64', $file->getMimeType());
      }
    }

    $phpmailer->AltBody = $body_text;



    $phpmailer->MsgHTML($body_html);

    $phpmailer->imgToCid();
    
    $result = $phpmailer->Send(true);
    if (is_array($result)) {
      if ($result[0]) {
        if ($backupEmail) {
          //$path = Yii::app()->basePath . '/sended_emails/' . $recipient->id;
          $path = dirname($backupEmail);
          if (!file_exists($path)) {
            $oldumask = umask(0);
            mkdir($path, 0777, true);
            umask($oldumask);
          }
          file_put_contents($path, $result['string'], 0777);
        }
      }
    }
    return $result;
  }

  public function insertExitMailingHref($body, $exitConfirmationData) {
    return str_replace('{Exit_Mailing_Link_Address}', Yii::app()->createAbsoluteUrl('/site/ServeConfirmationData', array('checksum'=>$exitConfirmationData->checksum)), $body);
  }

  public function pickUpExitConfirmationData() {
    $params = array(
        'obj_name'=>'Recipient',
        'field_name'=>'status',
        'obj_id'=>$this->recipient->id,
        'new_value'=>'0'
    );
    $confirmationData = ConfirmationData::model()->find('`obj_name`=:obj_name AND `field_name`=:field_name AND `obj_id`=:obj_id AND `new_value`=:new_value AND `served_at` IS NULL',array(
        ':obj_name'=>$params['obj_name'],
        ':field_name'=>$params['field_name'],
        ':obj_id'=>$params['obj_id'],
        ':new_value'=>$params['new_value']
    ));
    if($confirmationData) return $confirmationData;
    else {
      $confirmationData = new ConfirmationData;
      $confirmationData->setAttributes($params, false);
      $confirmationData->save();
    }
    return $confirmationData;
  }

}