<?php

class Recipient extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'recipients';
  }

  public function relations() {
    return array(
        'groups' => array(self::MANY_MANY, 'Group', 'recipient_group(recipient_id, group_id)')
    );
  }

  public function rules() {
    return array(
        array('email, status', 'required'),
        array('email','email'),
        array('email','unique','caseSensitive'=>false),
        array('email, status','safe', 'on'=>'search')
    );
  }

  public function attributeLabels() {
    return array(
        'id' => 'ID',
        'email' => Yii::t('cms', 'Adres email'),
    );
  }

//  public function checkUnique($attribute,$params) {
//    $exist = Recipient::model()->find('`email`=:email',array(':email'=>$this->email));
//    if($exist) {
//      switch ($this->status) {
//        case -1:
//          $msg = Yii::t('cms', 'Podany adres e-mail już istnieje i nie został jeszcze potwierdzony.');
//          break;
//        case 0:
//          $msg = Yii::t('cms', 'Podany adres e-mail jest wypisany z listy mailingowej.');
//          break;
//        default:
//          $msg = Yii::t('cms', 'Podany adres e-mail już istnieje.');
//          break;
//      }
//    }
//    if(isset($msg)) $this->addError ('email', $msg);
//  }

  public function getEnumOptions($fieldName) {
    $values = false;
    switch ($fieldName) {
      case 'status':
        $values = array(
            -1 => Yii::t('cms', 'Niepotwierdzony'),
            0 => Yii::t('cms', 'Nieaktywny'),
            1 => Yii::t('cms', 'Aktywny'),
        );
        break;
      default:
        break;
    }
    return $values;
  }

  public function TranslateEnumValue($fieldName) {
    $values = $this->getEnumOptions($fieldName);
    if (isset($values[$this->$fieldName])) {
      return $values[$this->$fieldName];
    }
    else
      return $this->$fieldName;
  }

  public function beforeValidate() {
    if($this->getIsNewRecord()) {
      if(!isset($this->status)) $this->status = 1;
    }
    return parent::beforeValidate();
  }

  public function search($group_id = false) {
    $criteria = new CDbCriteria;
    $criteria->compare('email', $this->email, true);
    $criteria->compare('status', $this->status, true);
    if($group_id) {
      $criteria->join = 'INNER JOIN `recipient_group` `recipient_group` ON `t`.`id`=`recipient_group`.`recipient_id`';
      $criteria->compare('`recipient_group`.`group_id`', $group_id, true);
    }
    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
  }

}