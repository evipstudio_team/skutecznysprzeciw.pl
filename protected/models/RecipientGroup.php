<?php

class RecipientGroup extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'recipient_group';
  }

  public function rules() {
    return array(
    );
  }

  static public function saveNew($recipient_id,$group_id) {
    $params = array('recipient_id'=>$recipient_id,'group_id'=>$group_id);
    if(!RecipientGroup::model()->findByPk($params)) {
      $recipientGroup = new RecipientGroup();
      $recipientGroup->setAttributes($params, false);
      $recipientGroup->save();
    }
  }

}