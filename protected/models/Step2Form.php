<?php

class Step2Form extends CFormModel {

  public $passCode;
  public $savePassCode;

  public $file;
  public $file2;
  public $file3;
  public $saveFiles;


  public function rules() {
    return array(
        array('savePassCode, saveFiles, file, file2, file3, passCode', 'safe'),
        array('file','file', 'types'=>'jpg, gif, png, pdf, doc, docx', 'on'=>'saveFile'),
//        array('file2','file', 'types'=>'jpg, gif, png, pdf, doc, docx', 'on'=>'saveFile'),
//        array('file3','file', 'types'=>'jpg, gif, png, pdf, doc, docx', 'on'=>'saveFile'),
        array('passCode','required','on'=>'savePassCode'),
        array(
            'passCode',
            'match', 'pattern'=>'/^[0-9a-zA-Z]{8,}$/' ,
            'on'=>'savePassCode',
            'message'=>'Elektroniczny kod dostepu powinien składać się z co najmniej 8 cyfr/liter w jednym ciągu.'
            ),
    );
  }

//  public function passCodeValidation($attribute, $params) {
//    $checkSumOfInt = $this->$attribute;
//    if(!preg_match('/[0-9]{8,}/', $checkSumOfInt)) {
//      $this->addError('passCode','Elektroniczny kod dostepu powinien składać się z co najmniej 8 cyfr/ liczb w jednym ciągu.');
//    }
//  }

  public function attributeLabels() {
    return array(
        'passCode' => 'Elektroniczny kod dostepu',
        'file' => 'Dokument',
    );
  }

  public function beforeValidate() {
//    if(!$this->files && $this->saveFiles) {
//      $this->addError('files', "Musisz wskazać pliki dokumentów.");
//    }
//    elseif(!$this->passCode && $this->savePassCode) {
//      $this->addError('passCode', "Musisz podać elektroniczny kod dostępu do akt.");
//    }
    if($this->saveFiles) {
      $this->setScenario('saveFile');
    }
    elseif($this->savePassCode) {
      $this->setScenario('savePassCode');
    }

    return parent::beforeValidate();
  }

  public function serve($causeId, $userId) {
    $commit = true;
    $transaction = Yii::app()->db->beginTransaction();
    if($this->scenario=='savePassCode') {
      $cause = Cause::model()->findByPk($causeId);
      $cause->pass_code = $this->passCode;
      if(!$cause->save()) $commit = false;
    }
    elseif($this->scenario=='saveFile') {

        if ($this->file) {
            $causeFile = new CauseFile();
            $causeFile->file = $this->file;
            $causeFile->cause_id = $causeId;
            $causeFile->created_by = $userId;
            if(!$causeFile->save()) {
                $commit = false;
            }
        }

        if ($this->file2) {
            $causeFile = new CauseFile();
            $causeFile->file = $this->file2;
            $causeFile->cause_id = $causeId;
            $causeFile->created_by = $userId;
            if(!$causeFile->save()) {
                $commit = false;
            }
        }

        if ($this->file3) {
            $causeFile = new CauseFile();
            $causeFile->file = $this->file3;
            $causeFile->cause_id = $causeId;
            $causeFile->created_by = $userId;
            if(!$causeFile->save()) {
                $commit = false;
            }
        }


    }
//    if($saved) {
////      $cause = Cause::model()->findByPk($causeId);
////      $lastComment = $cause->lastComment;
//////      $lastComment->created_by = $userId;
////      if($this->scenario=='saveFile') {
////        $lastComment->comment = 'Użytkownik wysłał plik dokumentu.';
////      }
////      elseif($this->scenario=='savePassCode') {
////        $lastComment->comment = 'Użytkownik wysłał elektroniczny kod dostepu.';
////      }
////      if(!$lastComment->save()) $commit = false;
//    }
    if ($commit) {
      $transaction->commit();
    }
    else {
      $transaction->rollback();
    }
    return $commit;
  }

}
