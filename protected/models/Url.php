<?php

class Url extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'urls';
  }

  public function rules() {
    return array(
        array('page_id, lang_id, anchor, address', 'required'),
        array('inheritance, title, description, keywords', 'safe'),
        array('blank, autoupdate', 'numerical', 'integerOnly' => true),
    );
  }

  public function relations() {
    return array(
        'page'=>array(self::BELONGS_TO,'Page','page_id'),
        'translation'=>array(self::HAS_ONE,'Article',array('page_id','lang_id'))
    );
  }

  public function getEnumOptions($fieldName) {
    $values = false;
    switch ($fieldName) {
      case 'blank':
      case 'autoupdate':
      case 'inheritance':
        $values = array(
            1 => Yii::t('cms', 'Tak'),
            0 => Yii::t('cms', 'Nie')
        );
        break;
      default:
        break;
    }
    return $values;
  }

  public function TranslateEnumValue($fieldName) {
    $values = $this->getEnumOptions($fieldName);
    if (isset($values[$this->$fieldName])) {
      return $values[$this->$fieldName];
    }
    else
      return $this->$fieldName;
  }

  public function calculateAnchor() {
    $this->anchor = $this->translation->title;
  }

  public function calculateTitle() {
    $this->title = $this->translation->title;
  }

  public function beforeValidate() {
    if ($this->isNewRecord) {
      $page = $this->page;
      if($page && $page->inheritance_url) $this->inheritance = 1;
      else $this->inheritance = 0;
      $this->address = normaliza($this->anchor);
      $this->calculateAddress();
    }
    else {
      if($this->autoupdate) {
        $this->calculateAnchor();
        $this->calculateTitle();
        if (!preg_match('/^https?:\/\//', $this->address)) {
          $this->address = normaliza($this->anchor);
          $exist = self::model()->find('`address`=:address AND `page_id`!=:page_id',array(
            ':address' => $this->address,
              ':page_id' => $this->page_id,
          ));
          if($exist && $exist->page) $this->calculateAddress();
          elseif($exist) $exist->delete();
        }
      }
    }
    return parent::beforeValidate();
  }

  public function attributeLabels() {
    return array(
        'page_id' => 'Podstrona',
        'lang_id' => 'Lang',
        'anchor' => 'Anchor',
        'address' => 'Adres',
        'blank' => 'Otwieraj w nowym oknie',
        'autoupdate' => 'Automatyczna aktualizacja adresu URL',
        'title' => Yii::t('cms', 'Tytuł (title) strony w meta tagach'),
        'description' => Yii::t('cms', 'Opis (description) strony w meta tagach'),
        'keywords' => Yii::t('cms', 'Słowa kluczowe (keywords) strony w meta tagach'),
        'inheritance' => Yii::t('cms', 'Dziedziczenie adresu'),
    );
  }

  public function calculateAddress() {
    $i = '';
    if (self::isExist($this->address)) {
      $i = 2;
      while ($exist = self::isExist($this->address . $i) && $exist->page) {
        ++$i;
      }
    }
    if ($i)
      $this->address .= $i;
  }

  static public function isExist($address) {
    $exist = self::model()->find('`address`=:address', array(
        ':address' => $address,
            ));
    return $exist;
  }

  public function getCalculatedAddress() {
    $page = $this->page;
    $href = '';
    if ($this->inheritance) {
      $parent = $page->parent;
      if($parent && $parent->inheritance_url) {
        $url = $parent->url;
        if($url) {
          $href = $url->calculatedAddress . '/' . $this->address;
        }
        else {
          $href = '/' . $this->address;
        }
      }
      else {
        $href = '/' . $this->address;
      }
    }
    else {
      $href = '/' . $this->address;
    }
    return normalizeHref($href);
  }

  public function getActiveUrl() {
    $href = $this->calculatedAddress;
    if($href && $href!='/') {
      $href .= '';
    }
    return $href;
  }

}