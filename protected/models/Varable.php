<?php

class Varable extends CActiveRecord {

  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  public function tableName() {
    return 'varables';
  }

  public function primaryKey() {
    return 'name';
  }

  public function rules() {
    return array(
        array('name', 'required'),
        array('user_id, value, description, last_update', 'safe'),
    );
  }

  public function relations() {
    return array(
        'user' => array(self::BELONGS_TO, 'User', 'user_id')
    );
  }

  public function attributeLabels() {
    return array(
        'user_id' => 'Przypisana do użytkownika',
        'User' => 'Przypisana do użytkownika',
        'description' => Yii::t('cms', 'Opis'),
        'value' => Yii::t('cms', 'Wartość'),
        'name' => Yii::t('cms', 'Nazwa kodowa parametru'),
    );
  }

  static public function pickUp($name, $notExistReturnElement = null, $user_id = 0) {
    if ($user_id === false)
      $user_id = Yii::app()->user->id;
    $varable = Varable::model()->findByPk(array('user_id' => $user_id, 'name' => $name));
    if ($varable)
      return $varable->value;
    else
      return $notExistReturnElement;
  }

  public function beforeSave() {
    $this->last_update = date('Y-m-d H:i:s');
    return parent::beforeSave();
  }

  static public function saveVarable($name, $value) {
    $varable = Varable::model()->findByPk(array('user_id' => Yii::app()->user->id, 'name' => $name));
    if ($varable) {
      if ($varable->value != $value) {
        $varable->value = $value;
        $varable->save();
      }
    } else {
      $varable = new Varable();
      $varable->user_id = Yii::app()->user->id;
      $varable->name = $name;
      $varable->value = $value;
      $varable->save();
    }
  }

  public function search() {
    $criteria = new CDbCriteria;
    $criteria->compare('name', $this->name, true);
    $criteria->compare('user_id', $this->user_id, false);

    return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'sort' => array(
                    'defaultOrder' => 'name,user_id',
                ),
                'pagination' => array(
                    'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
                ),
            ));
  }

  public function getUserIdentity() {
    if ($this->user)
      return $this->user->email;
    else
      return false;
  }

}