<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $parentPage));?>
<?$this->breadcrumbs = array_merge($this->breadcrumbs, array($this->pageTitle));?>
<div class="form">
  <? $form = $this->beginWidget('CActiveForm'); ?>
  <?= $form->errorSummary($page)?>
  <?if($parentPage->childCount(array('condition'=>'module_id=:module_id','params'=>array('module_id'=>Module::getModuleId('Articles'))))):?>
    <div class="row">
      <?=$form->labelEx($page,'parent_id'); ?>
      <?=$form->dropDownList($page, 'parent_id', CHtml::listData(Page::model()->findAll('`module_id` =:module_id',array(':module_id'=> Module::getModuleId('Articles'))),'id','name','parent.name')) ?>
      <?php echo $form->error($page,'parent_id'); ?>
    </div>
  <?else:?>
    <?= $form->hiddenField($page,'parent_id')?>
  <?endif?>
  <?= $this->renderPartial('_content_edit',array('model'=>$article,'form'=>$form))?>

  <? $this->endWidget(); ?>
</div>