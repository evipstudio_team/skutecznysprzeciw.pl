<h1><?= Yii::t('cms', 'Formularz nowej kategorii') ?></h1>
<div class="form">
  <? $form = $this->beginWidget('CActiveForm'); ?>
  <?php echo $form->errorSummary($newPage); ?>
  <div class="row">
    <?=$form->labelEx($newPage,'name'); ?>
    <?=$form->textField($newPage, 'name', array('size'=>60)) ?>
    <?=$form->error($newPage,'name'); ?>
  </div>
  <div class="row">
		<?=$form->labelEx($newPage,'parent_id'); ?>
    <?=$form->dropDownList($newPage,'parent_id',  CHtml::listData(Page::model()->findAll('`type`=:type',array('type'=>'Articles')), 'id', 'name','parent.name'))?>
		<?=$form->error($page,'type'); ?>
	</div>
  <div class="row">
    <?= $form->hiddenField($newPage,'type')?>
	</div>
  <div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('cms', 'Zapisz')); ?>
	</div>
  <? $this->endWidget(); ?>
</div>