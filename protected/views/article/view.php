<p>
  <strong>Link do artykułu:</strong> <?= CHtml::link($article->translations[0]->url->anchor,Url::getActiveUrl($article), array('target'=>'_blank'))?>
</p>
<br />
<p>
  <strong>Tytuł:</strong> <?= $article->translations[0]->title?>
</p>
<br />
<p>
  <strong>Treść:</strong> <?= $article->translations[0]->content?>
</p>
<br />
<p>
  <strong>Krótki opis:</strong> <?= $article->translations[0]->short_content?>
</p>