<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $page));?>
<?$this->breadcrumbs = array_merge($this->breadcrumbs, array($this->pageTitle));?>
<?$this->renderPartial('_leftMenu', array('page' => $page));?>

<div class="form">
  <?$form=$this->beginWidget('CActiveForm',array('htmlOptions' => array('enctype' => 'multipart/form-data'))); ?>
  <?= $form->errorSummary($banner)?>
    <div class="row">
      <?$id = 'files'?>
      <?= CHtml::label(Yii::t('cms', 'Obrazek lub obrazki'), $id)?>
      <input type="file" name="files[]" multiple="multiple" id="<?=$id?>" />
    </div>
    <div class="row buttons">
      <?php echo CHtml::submitButton(Yii::t('cms', 'Zapisz')); ?>
    </div>
  <?$this->endWidget();?>
</div>