<div class="form">
  <? $form=$this->beginWidget('CActiveForm'); ?>
    <?= $form->errorSummary($banner); ?>
    <div class="row">
      <?=$form->labelEx($banner,'time'); ?>
      <?=$form->textField($banner, 'time', array('size'=>10)) ?>
      <?=$form->error($banner,'time'); ?>
    </div>
    <div class="row buttons">
      <?php echo CHtml::submitButton(Yii::t('cms', 'Zapisz')); ?>
    </div>
  <? $this->endWidget(); ?>
</div>