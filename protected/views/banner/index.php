<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $page));?>
<?$this->breadcrumbs = array_merge($this->breadcrumbs, array($this->pageTitle));?>
<?$this->renderPartial('_leftMenu', array('page' => $page));?>

<?Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' );?>
<?Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');?>
<?
$str_js = "installSortable('banner-grid','".$this->createUrl('//banner/sort')."');";
Yii::app()->clientScript->registerScript('sortable-project', $str_js);

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'banner-grid',
    'dataProvider' => $banner->search(),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/gridview/gridview.css',
    'filter' => $banner,
    'enableSorting'=>false,
    'afterAjaxUpdate' => 'function reInstallSortable(id, data) { '.$str_js.' }',
    'rowCssClassExpression'=>'"items_{$data->id}"',
    'columns' => array(
        gridViewsortIcon(),
//        'time',
        array(
            'name'=>'FilesPreview',
            'type'=>'raw',
            'filter'=>false
        ),
        array(
            'name'=>'page',
            'value'=>'$data->page->name',
            'filter'=>  CHtml::dropDownList('Banner[page_id]', $banner->page_id, CHtml::listData(Page::model()->findAll('`module_id`=:module_id',array(':module_id'=>Banner::getModuleId())), 'id', 'name', 'parent.name')),
            'visible'=>($page->childCount)? true:false
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{status}',
            'htmlOptions'=>array('class'=>'icons'),
            'header'=>  Yii::t('cms', 'Status'),
            'buttons' => array(
                'status' => array(
                    'expressionImage'=>true,
                    'expressionLabel'=>true,
                    'label'=>'getStatusExpression($data->status)',
                    'type' => 'html',
                    'url' => 'Yii::app()->createUrl("banner/statusSwitch", array("id"=>$data->id))',
                    'imageUrl' => 'getStatusIcon($data->status)',
                    'click' => "function() {
                      $.ajax({
                        url: $(this).attr('href'),
                        success: function(data) {
                          if($(data).first('.errorSummary').length) {
                            $('#banner-grid').prepend('<div class=\"form\">'+data+'</div>');
                          }
                          else {
                            $.fn.yiiGridView.update('banner-grid');
                          }
                        }
                      });
                      return false;}
                      ",
                ),
            ),
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{update}',
            'htmlOptions'=>array('class'=>'icons'),
            'header'=>  Yii::t('cms', 'Edycja'),
            'buttons' => array(
                'update' => array(
                    'label' => Yii::t('cms', 'Edytuj'),
                    'url' => 'Yii::app()->createUrl("banner/edit", array("id"=>$data->id))',
                ),
            ),
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{delete}',
            'htmlOptions'=>array('class'=>'icons'),
            'header'=>  Yii::t('cms', 'Usuń'),
            'buttons' => array(
                'delete' => array(
                    'label' => Yii::t('cms', 'Usuń'),
                    'url' => 'Yii::app()->createUrl("banner/delete", array("id"=>$data->id))',
                ),
            ),
        ),

    ),
));
?>