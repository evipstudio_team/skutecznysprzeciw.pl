<?$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'BlockCreateDialog',
    'options'=>array(
        'title'=>  Yii::t('cms', 'Formularz nowego bloku'),
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>'600',
        'buttons'=>array(
            Yii::t('cms', 'Anuluj')=>'js:function(){$(this).dialog("close")}',
            Yii::t('cms', 'Dodaj nowy blok')=>'js:function(){
              jQuery.ajax({
                "url": $(\'#block-form\').attr(\'action\'),
                "data":$(\'#block-form\').serializeArray(),
                "type":"POST",
                "success":function(data){
                  if($(data).find(\'.errorSummary\').length) {
                    $(\'#block-form\').prepend(data);
                  }
                  else {
                    if($(data).find(\'.flashes\').length) {
                      $(\'#BlockCreateDialog\').dialog(\'option\',\'buttons\',{});
                      $(\'#block-form\').html($(data).find(\'.flashes\'));
                    }
                  }
                }
              });
              return false;}'
        )
    ),
));?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
  'id'=>'block-form',
  'action'=>$this->createUrl('block/create'),
)); ?>
	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>40, 'style'=>'width:90%','maxlength'=>128)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
  <div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>10, 'cols'=>40, 'style'=>'width:90%')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

<?php $this->endWidget(); ?>

</div>
<?$this->endWidget('zii.widgets.jui.CJuiDialog');?>