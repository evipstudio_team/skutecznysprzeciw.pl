<div class="form">
  <?php $form=$this->beginWidget('CActiveForm'); ?>
    <?= $form->errorSummary($block); ?>

    <div class="row">
      <?= $form->labelEx($block,'name'); ?>
      <?= $form->textField($block,'name',array('size'=>60,'maxlength'=>128)); ?>
      <?= $form->error($block,'name'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($block,'description'); ?>
      <?= $form->textArea($block,'description',array('rows'=>'15','cols'=>'60'))?>
      <?= $form->error($block,'description'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($block,'managed'); ?>
      <?= ZHtml::enumDropDownList($block, 'managed') ?>
      <?= $form->error($block,'managed'); ?>
    </div>
  <div class="button_bar">
    <div class="button_add">
      <?= CHtml::submitButton(Yii::t('cms', 'Zapisz'))?>
    </div>
  </div>
  <?php $this->endWidget(); ?>
</div>