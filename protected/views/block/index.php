<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Zarządzanie blokami'),
);
?>
<div class="form">
  <form method="get" action="<?= $this->createUrl('page/create')?>">
    <div class="row buttons">
      <?= CHtml::Button(Yii::t('cms', 'Dodaj nowy blok'),array('name'=>'','onclick'=>'$("#BlockCreateDialog").dialog("open"); return false;')); ?>
    </div>
  </form>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'block-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'name'=>'id',
            'htmlOptions'=>array('style'=>'width: 30px')
        ),
        'name',
        'description',
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}',
            'header'=>  Yii::t('cms', 'Edycja'),
            'htmlOptions'=>array('class'=>'center'),
            'buttons' => array(
                'update' => array(
                    'url' => 'Yii::app()->createUrl("block/view", array("id"=>$data->id))',
                    'visible' => 'Yii::app()->user->checkAccess("Root")'
                ),
            )
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete}',
            'header'=>  Yii::t('cms', 'Usuń'),
            'htmlOptions'=>array('class'=>'center'),
            'buttons' => array(
                'delete' => array(
                    'url' => 'Yii::app()->createUrl("block/delete", array("id"=>$data->id))',
                    'visible' => 'Yii::app()->user->checkAccess("Root")'
                ),
            )
        ),
    ),
));
?>
<?$this->renderPartial('../block/_form',array('model'=>new Block()))?>