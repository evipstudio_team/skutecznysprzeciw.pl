<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'block-grid',
    'dataProvider' => $possibleBlocks,
    'enablePagination' => false,
    'enableSorting' => false,
    'ajaxUrl'=>$this->createUrl('block/indexByElement',array('page_id'=>$page->id)),
    'summaryText'=>'',
    'columns' => array(
        'name',
        array(
            'value'=>'"<a href=\"".Yii::app()->createUrl("block/AjaxAssignPage", array("page_id"=>"' . $page->id . '","block_id"=>$data->id))."\" onclick=\"loadingBoxOpen();var element = $(this);$.ajax({url: $(this).attr(\'href\'),
                        success: function(data) {
                          $(element).html(data);
                          loadingBoxClose();
                        }
                      });
                return false;\"><img src=\"".$data->isAssignedIcon("' . $page->id . '")."\" alt=\"".$data->isAssignedTranslate("' . $page->id . '")."\" title=\"".$data->isAssignedTranslate("' . $page->id . '")."\" /></a>"',
            'type'=>'raw',
            'filter'=>false,
            'htmlOptions'=>array('class'=>'center', 'style'=>'width: 95px'),
            'header' => Yii::t('cms', 'Przynależność'),
        ),
//        array(
//            'class' => 'SButtonColumn',
//            'template' => '{edit_assignment}',
//            'header' => Yii::t('cms', 'Przynależność'),
//            'buttons' => array(
//                'edit_assignment' => array(
//                    'expressionLabel' => true,
//                    'expressionImage' => true,
//                    'label' => '$data->isAssignedTranslate("' . $page->id . '")',
//                    'url' => 'Yii::app()->createUrl("block/AjaxAssignPage", array("page_id"=>"' . $page->id . '","block_id"=>$data->id))',
//                    'visible' => 'Yii::app()->user->checkAccess("Block:AjaxAssignPage")',
//                    'type' => 'html',
//                'click' => "function() {
//                  $.ajax({
//                    type: 'POST',
//                    url: $(this).attr('href'),
//                    success: function() {
//                      $.fn.yiiGridView.update('block-grid');
//                    }
//                  })
////                  $.fn.yiiGridView.update('block-grid', {
////
////                  });
//                return false;}",
//                    'imageUrl' => '$data->isAssignedIcon("' . $page->id . '")'
//                )
//            )
//        ),
//        array(
//            'class' => 'CButtonColumn',
//            'header' => Yii::t('cms', 'Podgląd'),
//            'template' => '{view}',
//            'buttons' => array(
//                'view' => array(
//                    'url' => 'Yii::app()->createUrl("block/sortView", array("id"=>$data->id,"module_id"=>'.$module->id.',"element_id"=>'.$model->id.'))',
//                    'visible' => 'Yii::app()->user->checkAccess("block:View")',
//                    'click' => "function() {
//                      $.ajax({
//                        'url': $(this).attr('href'),
//                        success: function(data) {
//                          $('#block-grid').html(data);
//                        }
//                      })
//
//                      return false;}",
//                ),
//            )
//        ),
    ),
));
?>