<?Yii::app()->clientScript->registerCoreScript('jquery.ui');?>
<?Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');?>
<?
Yii::app()->clientScript->registerScript('sortable-project', "installSortable('block-grid','".$this->createUrl('//block/sort',array('block_id'=>$model->id))."');");
?>
<p style="margin-top: 20px">
  <?= CHtml::link(Yii::t('cms', 'Wróc do listy bloków'), $this->createUrl($module->controller.'/blocks',array('id'=>$element_id)))?>
</p>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'block-grid',
	'dataProvider'=>$elementBlock->search(),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/gridview/gridview.css',
  'enablePagination'=>false,
  'rowCssClassExpression'=>'"items_{$data->module_id}x{$data->element_id}"',
  'afterAjaxUpdate' => 'installSortable(\'block-grid\',\''.$this->createUrl('//block/sort',array('block_id'=>$model->id)).'\')',
  'ajaxUrl'=>$this->createUrl('block/view',array('id'=>$model->id)),
	'columns'=>array(
      gridViewsortIcon(),
		array(
        'name'=>  Yii::t('cms', 'Typ powiązanego elementu'),
        'value'=>'$data->module->translatedName',
        'filter'=>false
        ),
      array(
        'name'=>  Yii::t('cms', 'Tytuł powiązanego elementu'),
        'value'=>'$data->defaultTitle',
        'filter'=>false
        ),
    ),
)); ?>
