<div class="form">
  <p class="note">
    Zmień status zgłoszenia
  </p>
</div>
<?=$this->renderPartial('//shared/_flash',array('dialogBox'=>false))?>

<?= $this->renderPartial('_commentForm',array('model'=>$causeComment, 'nextSteps'=>$nextSteps))?>
<script type="text/javascript">
  $("#DialogBox").dialog({ buttons: {
      Zapisz: function() {
        $('#LoadingDialogBox').dialog('open');
        $.ajax({
          url : '<?=$this->createUrl('cause/addComment',array('id'=>$cause->id))?>',
          data: $('#commentForm').serializeArray(),
          type: 'post',
          success: function(data) {
            $('#DialogBox').html(data);
            $('#LoadingDialogBox').dialog('close');
          }
        });
      },
      Zamknij: function() { $( this ).dialog( "close" ); }
    } });
</script>
