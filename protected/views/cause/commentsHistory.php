<div class="form">
  <p class="note">
    Historia zmian zgłoszenia
  </p>
</div>
<?foreach($cause->comments as $comment):?>
  <p style="margin-top: 10px; font-size: 12px">
    <?= date('d/m/Y H:i:s',strtotime($comment->created_at))?> - <?= $comment->author->fullName?> -
    <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <?if($comment->comment):?>
      <?= $comment->comment?>
    <?else:?>
      brak notatki
    <?endif?>
  </p>
<? endforeach; ?>
<script type="text/javascript">
    $("#DialogBox").dialog("option","width","700");
    $("#DialogBox").dialog("option","position","center");
  </script>