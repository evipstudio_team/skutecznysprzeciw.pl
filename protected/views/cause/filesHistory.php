<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Zgłoszenia')=>$this->createUrl('cause/index'),
//  Yii::t('cms', 'Dokumenty zgłoszenia')=>$this->createUrl('cause/filesHistory',array('cause_id'=>$cause->id)),
    Yii::t('cms', 'Dokumenty zgłoszenia')
);
?>

<?$form=$this->beginWidget('CActiveForm', array('action'=>$this->createUrl('cause/filesHistory',array('id'=>$cause->id,'ajax'=>0)),'htmlOptions'=>array('enctype'=>'multipart/form-data')))?>
  <div class="form">
    <span class="input_label">Załącz skan lub plik pdf dokumentów:</span>
    <?= $form->fileField($step2Form,'file',array('class'=>"file"))?>
    <?=$form->error($step2Form,'file'); ?>
    <input type="submit" name="Step2Form[saveFiles]" value="Wyślij" />
  </div>
<?php $this->endWidget(); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'causeFiles-grid',
	'dataProvider'=>$causeFile->search(),
  'summaryText'=>summaryTextLayout('causeFiles-grid'),
	'columns'=>array(
		'filename',
    'created_at',
    array(
      'name' => Yii::t('cms', 'Pobierz'),
      'value' => '\'<a href="\'.Yii::app()->createUrl(\'show/getFile\',array(\'hash1\'=>$data->createLinkHash1(),\'hash2\'=>$data->createLinkHash2())).\'"><img src="\'.Yii::app()->theme->baseUrl.\'/images/download.png" alt="" title="" /></a>\'',
      'htmlOptions'=>array('class'=>'center'),
      'filter' => false,
      'type' => 'raw'
    ),
    array(
          'class' => 'CButtonColumn',
          'template' => '{delete}',
          'header' => Yii::t('cms', 'Usuń'),
          'htmlOptions' => array('class' => 'center'),
          'buttons' => array(
              'delete' => array(
                  'url' => 'Yii::app()->createUrl("cause/fileDelete", array("id"=>$data->id))',
              ),
          )
      ),
	),
)); ?>
<?if($ajax):?>
  <script type="text/javascript">
    $(function() {
      $("#DialogBox").dialog("option","width","700");
      $("#DialogBox").dialog("option","position","center");
    });
  </script>
<?endif?>