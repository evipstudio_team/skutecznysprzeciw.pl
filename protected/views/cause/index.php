<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Zgłoszenia'),
);
?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'causes-grid',
    'dataProvider' => $cause->search($user),
    'summaryText' => summaryTextLayout('causes-grid'),
    'filter' => $cause,
    'columns' => array(
        array(
            'name' => 'id',
            'htmlOptions' => array('style' => 'width: 30px')
        ),
        array(
            'name'=>'user.emailAndPhone',
			'type'=>'html',
			'header'=>'Email i numer telefonu',
            'filter'=>CHtml::activeTextField($user,'email')
        ),
        array(
            'name'=>'user.fullName',
            'filter'=>CHtml::activeTextField($user,'name')
        ),
        array(
            'name'=>'created_at',
            'value'=>'date(\'d/m/Y\',strtotime($data->created_at))',
            'htmlOptions'=>array('class'=>'center'),
            'filter'=>false
        ),
        array(
            'name'=>'rule_date',
            'value'=>'date(\'d/m/Y\',strtotime($data->rule_date))',
            'htmlOptions'=>array('class'=>'center'),
            'filter'=>false
        ),
        array(
            'name'=>'pass_code',
            'filter'=>false,
            'sortable'=>false
        ),
        array(
            'name'=>'actual_step',
            'value'=>'$data->StepTranslated',
            'filter'=>CHtml::activeDropDownList($cause,'actual_step',array(''=>'')+$cause->getNextPossibleSteps(0))
        ),
        array(
            'name'=>'last_response_date',
            'filter'=>false
        ),
//        array(
//            'name'=>'Ostatnio dołączony dokument',
//            'value'=>'$data->files? date(\'d/m/Y\',strtotime($data->files[0]->created_at)).\' \'.$data->files[0]->filename:\'\'',
//            'type'=>'raw',
//            'cssClassExpression' => '$data->id',
////            'htmlOptions' => array('onclick' => '$(\'#DialogBox\').data(\'url\', \''.$this->createUrl('cause/filesHistory').'\'+\'\/id\/\'+$(this).attr(\'class\').replace(/[^0-9]+/g,\'\'));$(\'#DialogBox\').dialog(\'open\'); $(".ui-dialog-titlebar").hide();', 'style' => 'cursor: pointer;', 'class' => 'center clickable', 'title' => Yii::t('cms', 'Kliknij aby zobaczyć')),
//            'htmlOptions' => array('onclick' => 'window.location.href=\''.$this->createUrl('cause/filesHistory').'\'+\'\/id\/\'+$(this).attr(\'class\').replace(/[^0-9]+/g,\'\')', 'style' => 'cursor: pointer;', 'class' => 'center clickable', 'title' => Yii::t('cms', 'Kliknij aby zobaczyć')),
//            'filter'=>false
//        ),
        array(
            'name'=>'Ostatnio dołączony dokument',
            'value'=>'$data->files? date(\'d/m/Y\',strtotime($data->files[0]->created_at)).\' \'.$data->files[0]->filename:\'\'',
            'type'=>'raw',
            'cssClassExpression' => '$data->id',
            'htmlOptions' => array('onclick' => '$(\'#DialogBox\').data(\'url\', \''.$this->createUrl('cause/filesHistory').'\'+\'\/id\/\'+$(this).attr(\'class\').replace(/[^0-9]+/g,\'\')+\'/ajax/true\');$(\'#DialogBox\').dialog(\'open\'); $(".ui-dialog-titlebar").hide();', 'style' => 'cursor: pointer;', 'class' => 'center clickable', 'title' => Yii::t('cms', 'Kliknij aby zobaczyć')),
//            'htmlOptions' => array('onclick' => 'window.location.href=\''.$this->createUrl('cause/filesHistory').'\'+\'\/id\/\'+$(this).attr(\'class\').replace(/[^0-9]+/g,\'\')', 'style' => 'cursor: pointer;', 'class' => 'center clickable', 'title' => Yii::t('cms', 'Kliknij aby zobaczyć')),
            'filter'=>false
        ),
        array(
            'header' => Yii::t('cms', 'Historia zmian'),
            'value' => '\'pokaż\'',
            'cssClassExpression' => '$data->id',
            'htmlOptions' => array('onclick' => '$(\'#DialogBox\').data(\'url\', \''.$this->createUrl('cause/commentsHistory').'\'+\'\/id\/\'+$(this).attr(\'class\').replace(/[^0-9]+/g,\'\'));$(\'#DialogBox\').dialog(\'open\'); $(".ui-dialog-titlebar").hide();', 'style' => 'cursor: pointer;', 'class' => 'center clickable', 'title' => Yii::t('cms', 'Kliknij aby zobaczyć')),
            'filter' => false,
            'type' => 'raw'
        ),
        array(
            'header' => Yii::t('cms', 'Konto aktywne'),
            'value' => '$data->user->status==\'active\'? \'tak\':\'nie\'',
            'cssClassExpression' => '$data->id',
            'htmlOptions' => array('onclick' => '$(\'#DialogBox\').data(\'url\', \''.$this->createUrl('cause/activateUser').'\'+\'\/id\/\'+$(this).attr(\'class\').replace(/[^0-9]+/g,\'\'));$(\'#DialogBox\').dialog(\'open\'); $(".ui-dialog-titlebar").hide();', 'style' => 'cursor: pointer;', 'class' => 'center clickable', 'title' => Yii::t('cms', 'Kliknij aby edytować')),
            'filter' => false,
            'type' => 'raw'
        ),
        array(
            'header' => Yii::t('cms', 'Nowy status'),
            'value' => '\'Zmień status zgłoszenia\'',
            'cssClassExpression' => '$data->id',
            'htmlOptions' => array('onclick' => '$(\'#DialogBox\').data(\'url\', \''.$this->createUrl('cause/addComment').'\'+\'\/id\/\'+$(this).attr(\'class\').replace(/[^0-9]+/g,\'\'));$(\'#DialogBox\').dialog(\'open\'); $(".ui-dialog-titlebar").hide();', 'style' => 'cursor: pointer;', 'class' => 'center clickable', 'title' => Yii::t('cms', 'Kliknij aby edytować')),
            'filter' => false,
            'type' => 'raw'
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete}',
            'header' => Yii::t('cms', 'Usuń'),
            'htmlOptions' => array('class' => 'center'),
            'buttons' => array(
                'delete' => array(
                    'url' => 'Yii::app()->createUrl("cause/delete", array("id"=>$data->id))',
                ),
            )
        ),
    ),
));
?>

<?$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'DialogBox',
    'options'=>array(
        'title'=>  '',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>'500',
        'open' => 'js:function(){
            jQuery.ajax({
              "url": $(this).data(\'url\'),
              "success":function(data){
                $(\'#DialogBox\').html($(data));
              }
            });
          return false;}',
        'close'=>'js:function(){
            $.fn.yiiGridView.update(\'causes-grid\');}',
        'buttons'=>array(
            Yii::t('cms', 'Zamknij')=>'js:function(){$(this).dialog("close");}',
        )
    ),
));?>
<?$this->endWidget('zii.widgets.jui.CJuiDialog');?>
