<div class="form">
  <?$form=$this->beginWidget('CActiveForm', array('htmlOptions'=>array('enctype'=>'multipart/form-data')))?>
  <div id="uploadFormDiv">
      <div class="form">
          <span class="input_label"><?= Yii::t('cms', 'Wskaż plik lub pliki do zapisania')?></span>
          <input type="file" name="files[]" multiple="" />
          <input type="submit" value="Zapisz plik" />
      </div>
  </div>
  <?php $this->endWidget(); ?>
</div>