<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $page));?>
<?$this->breadcrumbs = array_merge($this->breadcrumbs, array($this->pageTitle));?>

<?Yii::app()->clientScript->registerScript('loadAttachments', '
  jQuery.ajax({
  "url":"'.$this->createUrl('multimedia/index',array('page_id'=>$page->id,'redirectAction'=>Yii::app()->controller->getAction()->getId(),'redirectController'=>Yii::app()->controller->getId())).'",
    "success":function(data){
              $(\'#multimediaListing\').html(data);
            }
})');?>
<div id="multimediaListing">
  <?= loadingString()?>
</div>