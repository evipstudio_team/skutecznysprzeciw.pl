<div class="form">
  <p class="note">Kliknij na podstronę do której ma kierować link.</p>
</div>
<?
$globalId = 'CreateExistingPage';
$gridId = $globalId.'PageGrid';
$gridDialogId = $gridId.'Dialog';
$gridDialogArticleId = $gridId.'ArticleDialog';
$gridFormId = $globalId.'Form';
$gridFormRelatedToId = $gridFormId.'_related_element_id';
$this->widget('zii.widgets.grid.CGridView', array(
  'id'=>$gridId,
  'dataProvider'=>$articleTranslation->search($pageIds),
  'filter'=>$articleTranslation,
  'ajaxUrl'=>$this->createUrl('link/AssignArticleListing'),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/gridview/gridview.css',
  'enableSorting'=>false,
  'selectionChanged'=>'
    function(id){
      element_id=$.fn.yiiGridView.getSelection(id);
      trInst = $(\'#\'+id).find(\'tr.selected\');
      var element_name = $(trInst).children(\'td:first\').text();
      if(element_id.length>0) {
        element_id = element_id[0];
        element_id = element_id.split(\',\');
        element_id = element_id[0];
        $(\'#Link_related_module_id\').val(\''.Article::getModuleId().'\');
        $(\'#'.$gridFormRelatedToId.'\').prepend(\'<option value="\'+element_id+\'">\'+element_name+\'</option>\');
        $(\'#'.$gridFormRelatedToId.'\').val(element_id);
      }
      $(\'#'.$gridDialogArticleId.'\').dialog(\'close\');
    }',
  'columns'=>array(
      'title',
      array(
            'name' => 'parent.page_id',
            'value' => '$data->parent->page->name',
            'filter' => CHtml::dropDownList('Article[page_id]', $article->page_id, CHtml::listData(Page::model()->findAll('`id` IN (' . implode(',', Page::getTreeIds($page->id, false)) . ')'), 'id', 'name', 'parent.name')),
        ),
    ),
));?>

<?
Yii::app()->clientScript->scriptMap['jquery.js'] = false;
//Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
?>
