<div class="form">
  <p class="note">Kliknij na podstronę do której ma kierować link.</p>
</div>
<?
$globalId = 'CreateExistingPage';
$gridId = $globalId.'PageGrid';
$gridDialogId = $gridId.'Dialog';
$gridFormId = $globalId.'Form';
$gridFormRelatedToId = $gridFormId.'_related_element_id';
$this->widget('zii.widgets.grid.CGridView', array(
  'id'=>$gridId,
  'dataProvider'=>$page->searchToLink('DataProvider'),
  'filter'=>$page,
  'ajaxUrl'=>$this->createUrl('link/AssignPageListing'),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/gridview/gridview.css',
  'enableSorting'=>false,
  'selectionChanged'=>'
    function(id){
      element_id=$.fn.yiiGridView.getSelection(id);
      trInst = $(\'#\'+id).find(\'tr.selected\');
      var element_name = $(trInst).children(\'td:first\').text();
      if(element_id.length>0) {
        element_id = element_id[0];
        $(\'#Link_related_module_id\').val(\''.Page::getModuleId().'\');
        $(\'#'.$gridFormRelatedToId.'\').prepend(\'<option value="\'+element_id+\'">\'+element_name+\'</option>\');
        $(\'#'.$gridFormRelatedToId.'\').val(element_id);
      }
      $(\'#'.$gridDialogId.'\').dialog(\'close\');
    }',
  'columns'=>array(
      'name',
      array(
        'name'=>'parent_id',
        'value'=>'$data->parentName',
        'filter'=>CHtml::listData(Page::model()->findAll(Page::getTreeCriteria(true)), 'id', 'name', 'parent.name'),
      )
    ),
));?>

<?
Yii::app()->clientScript->scriptMap['jquery.js'] = false;
Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
?>