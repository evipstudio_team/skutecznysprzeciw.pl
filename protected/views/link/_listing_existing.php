<?
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'links-grid',
    'cssFile' => Yii::app()->theme->baseUrl.'/css/gridview/gridview.css',
    'dataProvider' => $link->search($pageIds, 'existing'),
    'afterAjaxUpdate' => 'installSortable',
    'rowCssClassExpression'=>'"items_{$data->id}"',
    'columns' => array(
        gridViewsortIcon(),
        array(
            'name' => Yii::t('cms', 'Podstrona na którą wskazuje link'),
            'type' => 'raw',
            'value' => '$data->module->translatedName." - ".$data->title'
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{status}',
            'htmlOptions'=>array('class'=>'icons'),
            'header'=>  Yii::t('cms', 'Status'),
            'buttons' => array(
                'status' => array(
                    'expressionImage'=>true,
                    'expressionLabel'=>true,
                    'label'=>'$data->getStatusExpression()',
                    'type' => 'html',
                    'url' => 'Yii::app()->createUrl("link/statusSwitch", array("id"=>$data->id))',
                    'imageUrl' => '$data->getStatusIcon()',
                    'click' => "function() {
                      $.ajax({
                        url: $(this).attr('href'),
                        success: function(data) {
                          if($(data).first('.errorSummary').length) {
                            $('#links-grid').prepend('<div class=\"form\">'+data+'</div>');
                          }
                          else {
                            $.fn.yiiGridView.update('links-grid');
                          }
                        }
                      });
                      return false;}
                      ",
                ),
            ),
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{update}',
            'htmlOptions'=>array('class'=>'icons'),
            'header'=>  Yii::t('cms', 'Edycja'),
            'buttons' => array(
                'update' => array(
                    'label' => Yii::t('cms', 'Edytuj'),
                    'url' => 'Yii::app()->createUrl("link/CreateRelated", array("page_id"=>$data->page_id,"id"=>$data->id))',
                ),
            ),
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{delete}',
            'htmlOptions'=>array('class'=>'icons'),
            'header'=>  Yii::t('cms', 'Usuń'),
            'buttons' => array(
                'delete' => array(
                    'label' => Yii::t('cms', 'Usuń'),
                    'url' => 'Yii::app()->createUrl("link/delete", array("id"=>$data->id))',
                ),
            ),
        ),
    ),
));
?>