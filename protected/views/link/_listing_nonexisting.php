<?

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'links-grid',
    'dataProvider' => $link->search($pageIds, 'nonexisting'),
    'afterAjaxUpdate' => 'installSortable',
    'rowCssClassExpression'=>'"items_{$data->id}"',
    'columns' => array(
        array(
            'name' => Yii::t('cms', 'Adres na który wskazuje link'),
            'type' => 'raw',
            'value' => '"<a href=\"".$data->defaultUrl->address."\">".$data->defaultUrl->address."</a>"'
        ),
        array(
            'name' => 'defaultUrl.blank',
            'type' => 'raw',
            'value' => '$data->defaultUrl->TranslateEnumValue(\'blank\')'
        ),
        array(
            'name' => Yii::t('cms', 'Podgląd obrazka'),
            'type' => 'raw',
            'value' => '$data->multimedia[0]->Mini'
        ),
        array(
          'name'=>'urlsLangIcons',
          'type'=>'raw',
          'filter'=>false,
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '<span style="white-space: nowrap">{status}&nbsp;&nbsp;&nbsp;{update}&nbsp;&nbsp;&nbsp;{delete}</span>',
            'buttons' => array(
                'update' => array(
                    'label' => Yii::t('cms', 'Edytuj'),
                    'url' => 'Yii::app()->createUrl("link/createNotRelated", array("id"=>$data->id,"page_id"=>$data->page_id))',
                ),
                'delete' => array(
                    'label' => Yii::t('cms', 'Usuń'),
                    'url' => 'Yii::app()->createUrl("link/delete", array("id"=>$data->id))',
                ),
                'status' => array(
                    'expressionImage'=>true,
                    'expressionLabel'=>true,
                    'label'=>'$data->getStatusExpression()',
                    'type' => 'html',
                    'url' => 'Yii::app()->createUrl("link/statusSwitch", array("id"=>$data->id))',
                    'imageUrl' => '$data->getStatusIcon()',
                    'click' => "function() {
                      $.ajax({
                        url: $(this).attr('href'),
                        success: function(data) {
                          if($(data).first('.errorSummary').length) {
                            $('#links-grid').prepend('<div class=\"form\">'+data+'</div>');
                          }
                          else {
                            $.fn.yiiGridView.update('links-grid');
                          }
                        }
                      });
                      return false;}
                      ",
                ),
            ),
        ),
    ),
));
?>