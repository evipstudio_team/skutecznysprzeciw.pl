<div class="form">
  <?$form=$this->beginWidget('CActiveForm',array('action'=>$this->createUrl('link/createNotRelated',array('page_id'=>$page->id,'id'=>$id)),'htmlOptions' => array('enctype' => 'multipart/form-data'))); ?>
    <?= $form->errorSummary(array($url,$link))?>
    <div class="row">
      <?php echo $form->labelEx($url, 'anchor'); ?>
      <?php echo $form->textField($url, 'anchor', array('size' => 60)); ?>
      <?php echo $form->error($url, 'anchor'); ?>
    </div>
    <div class="row">
      <?php echo $form->labelEx($url, 'address'); ?>
      <div class="note">Jeśli link ma prowadzić do strony zewnętrznej, podaj adres wraz z http://</div>
      <?php echo $form->textField($url, 'address', array('size' => 60)); ?>
      <?php echo $form->error($url, 'address'); ?>
    </div>
    <div class="row">
      <?php echo $form->labelEx($url, 'blank'); ?>
      <?= ZHtml::enumDropDownList($url, 'blank') ?>
      <?php echo $form->error($url, 'blank'); ?>
    </div>
    <div class="row">
      <?= $form->hiddenField($url,'lang_id')?>
    </div>
    <div class="row">
      <?= CHtml::label(Yii::t('cms', 'Obrazek (szerokość: 555px, wysokość: 225px)'), 'nonExistingPageFile')?>
      <input type="file" name="files[]" id="nonExistingPageFile" />
    </div>
    <div class="row buttons">
      <?php echo CHtml::submitButton(Yii::t('cms', 'Zapisz')); ?>
    </div>
  <?$this->endWidget();?>
</div>