<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $page));?>
<?$this->breadcrumbs = array_merge($this->breadcrumbs, array($this->pageTitle));?>
<?$this->renderPartial('_leftMenu', array('page' => $page));?>
<?
$possibleTabs = array(
  'createRelated' => array(
    'translated' => 'Istniejąca podstrona',
    'url' => CHtml::encode($this->createUrl('Link/createRelated', array('page_id' => $page->id))),
    'view' => '_existing_page'),
//  'createNotRelated' => array(
//    'translated' => 'Adres zewnętrzny',
//    'url' => CHtml::encode($this->createUrl('Link/createNotRelated', array('page_id' => $page->id))),
//    'view' => '_nonexisting_page')
);
$tabs = array();
foreach ($possibleTabs as $action => $translated) {
  if (Yii::app()->getController()->action->id == $action) {
    $tabs[$action] = array(
        'title' => Yii::t('cms', $translated['translated']),
        'view' => $translated['view'],
        'data' => array('link' => $link, 'page' => $page, 'url'=>$url, 'id'=>$id),
    );
  } elseif(!$id) {
    $tabs[$action] = array(
        'title' => Yii::t('cms', $translated['translated']),
        'url' => $translated['url'],
        'data' => array('link' => $link, 'page' => $page, 'url'=>$url, 'id'=>$id),
    );
  }
}
?>
<p class="note">Wskaż już istniejącą podstronę na którą ma prowadzić link, lub podaj adres ręcznie.</p>
<?
$this->widget('CTabView', array(
    'tabs' => $tabs,
    'cssFile' => Yii::app()->theme->baseUrl.'/css/jquery.yiitab.css',
    'activeTab'=>Yii::app()->getController()->action->id
))
?>
