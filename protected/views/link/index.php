<?Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' );?>
<?Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');?>
<?Yii::app()->clientScript->registerScript('installSortable', "installSortable('links-grid','".$this->createUrl('/link/sort')."');");?>
<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $page));?>
<?$this->breadcrumbs = array_merge($this->breadcrumbs, array($this->pageTitle));?>
<?
$possibleTabs = array(
  'existing' => array(
    'translated' => 'Linki do istniejących podstron',
    'url' => CHtml::encode($this->createUrl('link/index', array('page_id' => $page->id,'mode'=>'existing'))),
    'view' => '_listing_existing'),
//  'nonexisting' => array(
//    'translated' => 'Linki do adresów zewnętrznych',
//    'url' => CHtml::encode($this->createUrl('link/index', array('page_id' => $page->id,'mode'=>'nonexisting'))),
//    'view' => '_listing_nonexisting')
);
$tabs = array();
foreach ($possibleTabs as $action => $translated) {
  if ($mode == $action) {
    $tabs[$action] = array(
        'title' => Yii::t('cms', $translated['translated']),
        'view' => $translated['view'],
        'data' => array('link' => $link, 'page' => $page, 'pageIds'=>$pageIds),
    );
  } else {
    $tabs[$action] = array(
        'title' => Yii::t('cms', $translated['translated']),
        'url' => $translated['url'],
        'data' => array('link' => $link, 'page' => $page, 'pageIds'=>$pageIds),
    );
  }
}
?>
<?
$this->widget('CTabView', array(
    'cssFile' => Yii::app()->theme->baseUrl.'/css/jquery.yiitab.css',
    'tabs' => $tabs,
    'activeTab'=>$mode
))
?>