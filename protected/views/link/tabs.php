<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $model->page));?>
<?$this->breadcrumbs = array_merge(
        $this->breadcrumbs,
        array($model->module->translatedName." - ".$model->title=>$this->createUrl('link/edit',array('page_id'=>$model->page_id,'id'=>$model->id))),
        array($this->pageTitle)
);?>

<?
$this->widget('CTabView', array(
    'cssFile' => Yii::app()->theme->baseUrl.'/css/jquery.yiitab.css',
    'tabs' => $tabs,
    'cssFile' => Yii::app()->theme->baseUrl.'/css/jquery.yiitab.css',
    'activeTab'=>Yii::app()->getController()->action->id,
    'viewData'=>  isset($additionalParams)? array_merge($additionalParams,array('model'=>$model)):array('model'=>$model)
))
?>