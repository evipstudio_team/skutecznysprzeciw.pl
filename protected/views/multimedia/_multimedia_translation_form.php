<div class="form">
  <?php $form=$this->beginWidget('CActiveForm', array(
    'action'=>$this->createUrl('multimedia/editTranslation',array('multimedia_id'=>$model->multimedia_id,'lang_id'=>$model->lang_id))
  )); ?>


  <?= $form->errorSummary($model); ?>
  <div class="row">
    <? $div_id = 'title_' . $lang->id ?>
    <label for="<?= $div_id ?>"><?= $model->getAttributeLabel('title'); ?> w języku <?= strtolower($lang->name) ?>m</label>
    <?= $form->textField($model, 'title', array('id' => $div_id, 'size' => '60')) ?>
  </div>
  <div class="row">
  <? $div_id = 'description_' . $lang->id ?>
  <label for="<?= $div_id ?>"><?= $model->getAttributeLabel('description'); ?> w języku <?= strtolower($lang->name) ?>m</label>
  <div>
    <?= $form->textArea($model,'description',array('rows'=>10,'cols'=>50, 'id' => $div_id)) ?>
    </div>
  </div>
  <div class="button_bar">
    <?if($model->getIsNewRecord()==false):?>
      <div class="button_delete">
          <?= CHtml::link(Yii::t('cms', 'Usuń treść w języku').' '.$lang->getNameVariety(), $this->createUrl('multimedia/DeleteTranslation',array('multimedia_id'=>$model->multimedia_id,'lang_id'=>$model->lang_id)), array('onclick'=>'return confirm(\''.  Yii::t('cms', 'Czy jesteś pewien że chcesz usunąć treść w języku').' '.$lang->getNameVariety().'?\')'))?>
      </div>
    <?endif?>
    <div class="button_add">
      <?php echo CHtml::submitButton(Yii::t('cms', 'Zapisz dane w języku'.' '.$lang->getNameVariety())); ?>
    </div>
  </div>
  <?php $this->endWidget(); ?>
</div>