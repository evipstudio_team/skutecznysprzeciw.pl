<?Yii::app()->getClientScript()->registerCoreScript( 'jquery.ui' );?>
<?Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');?>
<?
$str_js = "installSortable('multimedia-grid','".$this->createUrl('//multimedia/sort')."');";
Yii::app()->clientScript->registerScript('sortable-project', $str_js);
?>
<div id="uploadFormDiv">
  <form id="uploadForm" action="<?= CHtml::encode($this->createUrl('multimedia/upload', array('page_id' => $multimedia->page_id))) ?>" method="post" enctype="multipart/form-data" id="uploadForm">
    <div class="form">
        <span class="input_label"><?= Yii::t('cms', 'Wskaż plik lub pliki do zapisania')?></span>
        <input type="file" name="files[]" multiple="" />
        <input type="submit" value="Zapisz plik" />
        <input type="hidden" name="redirectAction" value="<?= $redirectAction?>" />
        <input type="hidden" name="redirectController" value="<?= $redirectController?>" />
    </div>
  </form>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'multimedia-grid',
	'dataProvider'=>$multimedia->search(),
  'enableSorting'=>false,
  'rowCssClassExpression'=>'"items_{$data->id}"',
    'afterAjaxUpdate' => 'function reInstallSortable(id, data) { '.$str_js.' }',
  'ajaxUrl'=>$this->createUrl('multimedia/index',array('page_id'=>$multimedia->page_id,'redirectAction'=>$redirectAction, 'redirectController'=>$redirectController)),
  'summaryText'=>summaryTextLayout('multimedia-grid'),
	'columns'=>array(
    gridViewsortIcon(),
		'filename',
    'typeTranslated',
    'defaultTitle',
    'defaultDescription',
    array(
      'name'=>  Yii::t('cms', 'Podgląd'),
      'type'=>'raw',
      'value'=>'$data->mini',
    ),
      array(
        'class' => 'CButtonColumn',
        'template' => '{update}',
        'header'=>  Yii::t('cms', 'Edycja'),
        'htmlOptions'=>array('class'=>'center'),
        'buttons' => array(
            'update' => array(
                'label'=>  Yii::t('cms', 'Edytuj'),
                'url' => 'Yii::app()->createUrl("multimedia/edit", array("id"=>$data->id))',
            ),
        )
      ),
		array(
        'class' => 'CButtonColumn',
        'template' => '{delete}',
        'htmlOptions'=>array('class'=>'center'),
        'header'=>  Yii::t('cms', 'Usuń'),
        'buttons' => array(
            'delete' => array(
                'url' => 'Yii::app()->createUrl("multimedia/delete", array("id"=>$data->id))',
            ),
        )
      ),
	),
)); ?>