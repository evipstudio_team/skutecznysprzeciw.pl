<?
$this->menu = array(
    array(
        'label' => Yii::t('cms', 'Stwórz nową wiadomość'),
        'url' => $this->createUrl('newsletter/create', array('page_id' => $page->id))
    ),
    array(
        'label' => Yii::t('cms', 'Lista wiadomości'),
        'url' => $this->createUrl('newsletter/index', array('page_id' => $page->id))
    ),
    array(
        'label' => Yii::t('cms', 'Zarządzaj odbiorcami'),
        'url' => $this->createUrl('recipient/index', array('page_id' => $page->id))
    ),
    array(
        'label' => Yii::t('cms', 'Zarządzaj nadawcami'),
        'url' => $this->createUrl('newsletter/senders', array('page_id' => $page->id))
    ),
//    array(
//        'label' => Yii::t('cms', 'Edycja kategorii'),
//        'url' => $this->createUrl('page/edit', array('id' => $page->id)),
//        'visible'=>Yii::app()->user->checkAccess('Administrator')
//    ),
);
?>