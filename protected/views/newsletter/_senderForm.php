<?= $form->errorSummary($sender) ?>
<div class="row">
  <?= $form->labelEx($sender, 'name'); ?>
  <?= $form->textField($sender, 'name', array('size' => 60)) ?>
  <?= $form->error($sender, 'name'); ?>
</div>
<div class="row">
  <?= $form->labelEx($sender, 'email'); ?>
  <?= $form->textField($sender, 'email', array('size' => 60)) ?>
  <?= $form->error($sender, 'email'); ?>
</div>
<br />
<p class="info">Poniższe dane potrzebne są do zalogowania się na konto pocztowe z którego będą wysyłane wiadomości.</p>
<div class="row">
  <?= $form->labelEx($sender, 'host'); ?>
  <?= $form->textField($sender, 'host') ?>
  <?= $form->error($sender, 'host'); ?>
</div>
<div class="row">
  <?= $form->labelEx($sender, 'login'); ?>
  <?= $form->textField($sender, 'login') ?>
  <?= $form->error($sender, 'login'); ?>
</div>
<div class="row">
  <?= $form->labelEx($sender, 'password'); ?>
  <?= $form->textField($sender, 'password') ?>
  <?= $form->error($sender, 'password'); ?>
</div>
<p class="info">Poniższa informacja dotyczy adresu na który mają trafiać zwroty wiadomości przy których podczas dostarczania wystąpiły błędy związane z niewłaściwym/nieistniejącym adresem email etc.<br />Informacja i obsługa takich wiadomości jest ważna z punktu widzenia utrzymania aktualnej bazy odbiorców.<br />Pozostawienie tego pola pustego lub niewpisanie żadnego adresu spowoduje iż wszystkie tego typu wiadomości będą wysyłane na adres nadawcy.<br /><strong>Zaleca się utworzenie osobnej skrzynki na tego typu wiadomości, np. zwroty@example.com.</strong></p>
<div class="row">
  <?= $form->labelEx($sender, 'sender'); ?>
  <?= $form->textField($sender, 'sender') ?>
  <?= $form->error($sender, 'sender'); ?>
</div>