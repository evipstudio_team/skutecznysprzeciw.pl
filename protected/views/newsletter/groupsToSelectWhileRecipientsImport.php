<div style="margin: 10px">
  <?foreach($groups as $index=>$group):?>
    <?$id = 'group_'.$group->id?>
    <span style="white-space: nowrap; margin-right: 10px">
      <label for="<?= $id?>" style="white-space:nowrap;">
        <input type="checkbox" name="groups[]" id="<?=$id?>" value="<?= $group->id?>"<?if(in_array($group->id, $groupsToSelect)):?>checked="checked"<?endif?> /> - <?= $group->name?>
      </label>
    </span>
  <?endforeach?>
</div>