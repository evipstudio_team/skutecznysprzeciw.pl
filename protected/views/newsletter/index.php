<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $page));?>
<?$this->breadcrumbs = array_merge($this->breadcrumbs, array($this->pageTitle));?>

<?
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'newsletters-grid',
    'dataProvider' => $newsletter->search(),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/gridview/gridview.css',
    'filter' => $newsletter,
    'columns' => array(
        'title',
        array(
            'name'=>'created_at',
            'filter'=>false
            ),
        array(
            'name'=>'status',
            'value'=>'$data->TranslateEnumValue(\'status\')',
            'filter'=>  CHtml::activeDropDownList($newsletter, 'status', array(''=>'')+ZHtml::enumItem($newsletter, 'status'))
            ),
        array(
            'name'=>'recipientsCount',
            'filter'=>false
            ),
        array(
            'name'=>'sendedCount',
            'filter'=>false
            ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}',
            'htmlOptions'=>array('class'=>'icons'),
            'header'=>  Yii::t('cms', 'Edycja'),
            'buttons' => array(
                'update' => array(
                    'label'=>  Yii::t('cms', 'Edytuj wiadomość'),
                    'url' => 'Yii::app()->createUrl("newsletter/view", array("id"=>$data->id))',
                ),
            )
        ),
    ),
));
?>