<div id="newsletterGroups"></div>
<?
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'recipients-grid',
    'cssFile' => Yii::app()->theme->baseUrl.'/css/gridview/gridview.css',
    'dataProvider' => $newsletterRecipient->search($recipient->email,$newsletterGroup->group_id),
    'filter' => $newsletterRecipient,
    'columns' => array(
        array(
            'name'=>  Yii::t('cms', 'Adres email'),
            'value'=>'$data->recipient->email',
            'filter'=>  CHtml::activeTextField($recipient, 'email')
        ),
        array(
            'name'=>  Yii::t('cms', 'Grupa(y) z której pochodzi odbiorca'),
            'value'=>'implode(\', \',CHtml::listData($data->recipient->groups(array(\'condition\'=>\'`id` IN ('.implode(',', CHtml::listData($newsletter->groups, 'id', 'id')).')\')),\'id\',\'name\'))',
            'filter'=>  CHtml::activeDropDownList($newsletterGroup, 'group_id', array(''=>'')+CHtml::listData($newsletter->groups, 'id', 'name'))
        )
    ),
));
?>
<?Yii::app()->clientScript->registerScript('loadNewsletterGroups', 'jQuery.ajax({
  "url":"'.$this->createUrl('newsletter/newsletterGroups',array('newsletter_id'=>$newsletter->id)).'",
    "success":function(data){
              $(\'#newsletterGroups\').html(data);
            }
})');?>