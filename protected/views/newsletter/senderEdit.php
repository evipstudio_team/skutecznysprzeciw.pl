<?= $this->renderPartial('//shared/_flash')?>
<div class="form">
<? $form = $this->beginWidget('CActiveForm',array('action'=>$this->createUrl('newsletter/senderEdit',array('id'=>$sender->id)),'id'=>'SenderEditForm')); ?>
  <?= $this->renderPartial('_senderForm',array('form'=>$form,'sender'=>$sender))?>
<? $this->endWidget(); ?>
</div>

<?
$cs = Yii::app()->getClientScript();
$cs->registerScript(
  'addbuttons','
    $(\'#SenderEditDialog\').dialog( "option", "buttons", {
      "Zamknij": function() { $(this).dialog("close"); },
      "Zapisz": function() {
          $.ajax({
            \'url\':$(\'#SenderEditForm\').attr(\'action\'),
            \'type\':\'post\',
            \'data\':$(\'#SenderEditForm\').serializeArray(),
            \'success\':function(data){
              $(\'#SenderEditDialog\').html(data);
            }
          })
        }
      } );
  ',
  CClientScript::POS_END
);
?>