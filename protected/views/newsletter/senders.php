<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $page));?>
<?$this->breadcrumbs = array_merge($this->breadcrumbs, array($this->pageTitle));?>
<?$this->renderPartial('_leftMenu', array('page' => $page));?>

<?
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'senders-grid',
    'dataProvider' => Sender::model()->search(),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/gridview/gridview.css',
    'enablePagination'=>false,
    'columns' => array(
        'name',
        'email',
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}',
            'htmlOptions'=>array('class'=>'icons'),
            'header'=>  Yii::t('cms', 'Edycja'),
            'buttons' => array(
                'update' => array(
                    'label'=>  Yii::t('cms', 'Edytuj nadawcę'),
                    'url' => 'Yii::app()->createUrl("newsletter/senderEdit", array("id"=>$data->id))',
                    'click' => "function() {
                      $.ajax({
                        url: $(this).attr('href'),
                        success: function(data) {
                          $('#SenderEditDialog').html(data);
                          $('#SenderEditDialog').dialog('open');
                        }
                      })
                    return false;}",
                  ),
            )
        ),
    ),
));
?>

<?
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'SenderEditDialog',
    'options'=>array(
        'autoOpen'=>false,
        'title'=>  Yii::t('cms', 'Edycja odbiorcy'),
        'modal'=>true,
        'width'=>'900px',
        'close'=>'js:function() {$.fn.yiiGridView.update(\'senders-grid\');}'
    ))
);
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>