<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $page));?>
<?$this->breadcrumbs = array_merge(
        $this->breadcrumbs,
        array($newsletter->title=>$this->createUrl('newsletter/edit',array('id'=>$newsletter->id))),
        array($this->pageTitle)
        );?>


<?
$this->widget('CTabView', array(
    'cssFile' => Yii::app()->theme->baseUrl.'/css/jquery.yiitab.css',
    'tabs' => $tabs,
    'activeTab'=>Yii::app()->getController()->action->id,
    'viewData'=>  array_merge(array('newsletter'=>$newsletter),$additionalParams)
))
?>

<?
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'sendTestDialog',
    'options'=>array(
        'autoOpen'=>false,
        'title'=>'Wysyłka testowa',
        'modal'=>true,
        'width'=>'auto',
        'open'=>'js:function(){
          jQuery.ajax({
            "url": "'.$this->createUrl('newsletter/testSend', array('id' => $newsletter->id)).'",
            "success":function(data){
              $(\'#sendTestDialog\').html($(data));
            }
          });
          return false;}',
        'buttons'=>array(
            Yii::t('cms', 'Zamknij')=>'js:function(){$(this).dialog("close")}',
              Yii::t('cms','Wyślij')=>'js:function(){
                $(\'#sendTestDialog\').prepend(\'<div style="text-align: center;"><img src="'.Yii::app()->theme->baseUrl.'/images/loading.gif" alt="" title="" /> - proszę czekać, trwa wysyłanie...</div><br /><br />\');
                jQuery.ajax({
                  "url":$(\'#sendTestForm\').attr(\'action\'),
                  "type":"POST",
                  "data":$(\'#sendTestForm\').serializeArray(),
                  "success":function(data){
                    $(\'#sendTestDialog\').html(data);
                  }
                })
              }',
            )
    ))
);

$this->endWidget('zii.widgets.jui.CJuiDialog');
?>