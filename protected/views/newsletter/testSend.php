<?= $this->renderPartial('//shared/_flash')?>
<div class="form">
  <? $form = $this->beginWidget('CActiveForm',array('id'=>'sendTestForm','action'=>$this->createUrl('newsletter/testSend',array('id'=>$newsletter->id)))); ?>
  <?= $form->errorSummary($newsletter)?>
  <div class="row">
  	<div class="note">
              <?= Yii::t('cms', 'Adresy mogą być oddzielone spacją, znakiem enter, przecinkami i wiele innych.') ?>
          </div>
        <legend><?= Yii::t('cms', 'Wprowadź adresy email') ?></legend>
          <div style="margin-right:23px">
          <textarea cols="10" style="width: 100%; margin-right:20px" rows="15" name="emails"><?= implode(chr(13), $emails)?></textarea>
          </div>
    </div>
    <div class="button_bar">
  <div class="button_add" style="text-align: center">
		<?php echo CHtml::submitButton(Yii::t('cms','Wyślij')); ?>
	</div>
	</div>
  <? $this->endWidget(); ?>
</div>