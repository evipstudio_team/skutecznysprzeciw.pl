<div class="form">
  <? $form = $this->beginWidget('CActiveForm'); ?>
  <? if (count(Yii::app()->params['langs']) > 1): ?>
    <? $tabs = array() ?>
    <? foreach (Yii::app()->params['langs'] as $lang): ?>
      <? $url = $this->createUrl('page/contentEdit', array('id' => $page->id, 'lang_id' => $lang->id)); ?>
      <? $title = getLangIcon(Article::model()->findByPk(array('page_id'=>$page->id,'lang_id'=>$lang->id)), $lang) . ' ' . Yii::t('cms', 'Język') . ' ' . $lang->name; ?>
      <? if ($lang->id == $translation->lang_id): ?>
        <?
        $tabs['translation_' . $lang->id] = array(
            'title' => $title,
            'view' => '_content_edit',
            'data' => array('model' => $translation, 'form' => $form, 'lang' => $lang)
                )
        ?>
      <? else: ?>
        <?
        $tabs['translation_' . $lang->id] = array(
            'title' => $title,
            'url' => CHtml::encode($url),
                )
        ?>
      <? endif ?>
    <? endforeach ?>
    <? $this->widget('CTabView', array('tabs' => $tabs, 'activeTab' => 'translation_' . $translation->lang_id)); ?>

  <? else: ?>
    <?= $this->renderPartial('_content_edit', array('model' => $translation, 'form' => $form, 'lang' => Yii::app()->params['lang'])) ?>
  <? endif ?>
  <? $this->endWidget(); ?>
</div>
