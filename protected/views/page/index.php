<?Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');?>
<?php
$this->breadcrumbs=array(
    $this->pageTitle,
);
?>
<div class="form">
  <form method="get" action="<?= $this->createUrl('page/create')?>">
    <div class="row buttons">
      <?= CHtml::submitButton(Yii::t('cms', 'Dodaj nową podstronę'),array('name'=>'')); ?>
    </div>
  </form>
</div>


<?$depth = $page->countDepth()?>
<?$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'pages-grid',
    'dataProvider' => $page->search(),
    'filter' => $page,
    'enablePagination'=>false,
    'rowCssClassExpression'=>(isset($_GET['ajax'])? '"expanded items_{$data->id}"':'"items_{$data->id}"'),
    'summaryText'=>$page->countDepth()? '<a href="'.$this->createUrl('page/index',array('parent_id'=>$page->parent->parent_id)).'"><img src="'.Yii::app()->theme->baseUrl.'/images/expand_up.png" alt="" title="" /> - Idź do kategorii nadrzędnej</a>':'',
    'emptyText'=>$page->countDepth()? Yii::t('cms','Brak wyników').'.&nbsp;&nbsp;&nbsp; <a href="'.$this->createUrl('page/index',array('parent_id'=>$page->parent->parent_id)).'"><img src="'.Yii::app()->theme->baseUrl.'/images/expand_up.png" alt="" title="" /> - Idź do kategorii nadrzędnej</a>':Yii::t('cms','Brak wyników'),
    'columns' => array(
        //(isset($_GET['ajax'])? array('name'=>'','value'=>'','filter'=>false):gridViewsortIcon()),
        array(
            'name'=>'name',
            'value'=>(isset($_GET['ajax'])? '$data->nameDepthSeparator(\'---\','.$depth.').\' \'. ':'').'CHtml::link($data->getName(1),Yii::app()->createUrl(\'page/index\',array(\'parent_id\'=>$data->id)))',
            'type'=>'html'
        ),
        array(
            'name'=>  Yii::t('cms', 'Moduł'),
            'value'=>'$data->module->TranslatedName',
            'htmlOptions'=>array('style'=>'text-align: right; padding-right: 10px'),
            'filter'=>false
        ),
        array(
            'name'=>  Yii::t('cms', 'Ilość podkategorii'),
            'value'=>'$data->childCount? $data->childCount." <a onclick=\"expandPages(this,$data->id);return false;\" href=\"".Yii::app()->createUrl(\'page/index\',array("parent_id"=>$data->id,"ajax"=>true))."\" class=\"expand plus\"></a>":"n/a"',
            'htmlOptions'=>array('style'=>'text-align: right; padding-right: 10px'),
            'type'=>'raw',
            'filter'=>false
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{status}',
            'header'=>  Yii::t('cms', 'Status'),
            'htmlOptions'=>array('class'=>'icons'),
            'buttons' => array(
                'status' => array(
                    'expressionImage' => true,
                    'expressionLabel' => true,
                    'label' => 'getStatusExpression($data->status)',
                    'type' => 'html',
                    'url' => 'Yii::app()->createUrl("page/statusSwitch", array("id"=>$data->id))',
                    'imageUrl' => 'getStatusIcon($data->status)',
                    'click' => "function() {
                      loadingBoxOpen();
                      var element = $(this);
                      $.ajax({
                        url: $(this).attr('href'),
                        success: function(data) {
                          $(element).html(data);
                          loadingBoxClose();
                        }
                      });
                      return false;}
                      ",

                ),
            )
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{add}',
            'header'=>  Yii::t('cms', 'Dodaj'),
            'htmlOptions'=>array('class'=>'icons'),
            'buttons' => array(
                'add' => array(
                    'label' => Yii::t('cms', 'Dodaj podkategorię'),
                    'url' => 'Yii::app()->createUrl("page/create", array("parent_id"=>$data->id))',
                    'imageUrl'=>Yii::app()->theme->baseUrl.'/css/gridview/add.png'
                ),
            )
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}',
            'header'=>  Yii::t('cms', 'Podgląd'),
            'htmlOptions'=>array('class'=>'icons'),
            'buttons' => array(
                'view' => array(
                    'label' => Yii::t('cms', 'Podgląd'),
                    'url' => 'Yii::app()->createUrl($data->module->controller."/index", array("page_id"=>$data->id))',
                ),
            )
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}',
            'header'=>  Yii::t('cms', 'Edycja'),
            'htmlOptions'=>array('class'=>'icons'),
            'buttons' => array(
                'update' => array(
                    'label' => Yii::t('cms', 'Edycja kategorii'),
                    'url' => 'Yii::app()->createUrl("page/edit", array("id"=>$data->id))',
                ),
            )
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{delete1}',
            'header'=>  Yii::t('cms', 'Usuń'),
            'htmlOptions'=>array('class'=>'icons'),
            'buttons' => array(
                'delete1' => array(
                    'expressionImage' => true,
                    'label' => Yii::t('cms', 'Usuń element'),
                    'type' => 'html',
                    'url' => 'Yii::app()->createUrl("page/delete", array("id"=>$data->id))',
                    'imageUrl' => 'Yii::app()->theme->baseUrl."/images/delete.png"',
                ),
            )
        ),
//        array(
//            'class' => 'CButtonColumn',
//            'template' => '{delete}',
//            'header'=>  Yii::t('cms', 'Usuń'),
//            'htmlOptions'=>array('class'=>'icons'),
//            'buttons' => array(
//                'delete' => array(
//                    'label' => Yii::t('cms', 'Usuń podkategorię'),
//                    'url' => 'Yii::app()->createUrl("page/delete", array("id"=>$data->id))',
//                ),
//            )
//        ),
    ),
));
?>