<?$this->renderPartial('../shared/_breadcrumbs', array('page' => $page));?>
<?$this->breadcrumbs = array_merge(
        $this->breadcrumbs,
        array(Yii::t('cms', 'Edycja kategorii')=>$this->createUrl('page/edit',array('id'=>$page->id))),
        array($this->pageTitle)
);?>
<?
$this->widget('CTabView', array(
    'tabs' => $tabs,
    'activeTab'=>Yii::app()->getController()->action->id,
    'viewData'=>  isset($additionalParams)? array_merge($additionalParams,array('page'=>$page)):array('page'=>$page)
))
?>