<div class="form">
  <?if($errors):?>
    <div class="<?=CHtml::$errorSummaryCss?>">
      <p><?=Yii::t('yii','Please fix the following input errors:')?></p>
      <ul>
        <?foreach($errors as $error):?>
          <li><?= $error?></li>
        <?endforeach?>
      </ul>
    </div>
  <?endif?>
  <? $form = $this->beginWidget('CActiveForm'); ?>
    <div class="row">
        <legend><?= Yii::t('cms', 'Wskaż do jakich grup zostaną dodani odbiorcy') ?></legend>
        <div id="GroupsToSelectWhileRecipientsImport"></div>
        <div style="float: left; height: 20px; width: 100%;"></div>
        <div class="button_bar">
        <div class="button_add">
        <input type="button" name="addGroup" value="Dodaj nową grupę" onclick="$('#addGroupDialog').dialog('open')" />
        </div>
        </div>
    </div>
    <div class="row">
    	<div style="float: left; height: 20px; width: 100%;"></div>
    	  <div class="note">
              <?= Yii::t('cms', 'Adresy mogą być oddzielone spacją, znakiem enter, przecinkami i wiele innych.') ?>
          </div>
        <legend><?= Yii::t('cms', 'Wprowadź adresy email.') ?></legend>

          <div style="margin-right:20px">
            <textarea cols="80" style="width: 100%" rows="20" name="emails"><?= implode(chr(13), $emails)?></textarea>
          </div>  
        <div class="button_bar">
        	<div class="button_add">
              <?php echo CHtml::submitButton(Yii::t('cms', 'Wczytaj odbiorców'),array('name'=>'refresh')); ?>
            </div>
        	<div class="button_add">
        	  <?php echo CHtml::submitButton(Yii::t('cms', 'Zapisz odbiorców'),array('name'=>'save')); ?>
     		</div>            
        </div>
    </div>
  <? $this->endWidget(); ?>
</div>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
  'id'=>'addGroupDialog',
  'options'=>array(
      'title'=>Yii::t('cms','Dodaj grupę'),
      'autoOpen'=>false,
      'modal'=>'true',
      'width'=>'auto',
      'height'=>'auto',
      'close'=>'js:function(){reInstall()}',
      'open'=>'js:function(){
        jQuery.ajax({
          "url":"'.$this->createUrl('recipient/GroupAdd').'",
            "type":"get",
            "success":function(data){
              $(\'#addGroupDialog\').html(data);
              $(\'#Group_name\').focus();
            }
        });
      }',
      'buttons' => array(
          Yii::t('cms','Zamknij')=>'js:function(){$(this).dialog(\'close\')}',
          Yii::t('cms','Zapisz nową grupę')=>'js:function(){
              jQuery.ajax({
                "url":"'.$this->createUrl('recipient/GroupAdd').'",
                "type":"POST",
                "data":$(\'#AddGroupForm\').serializeArray(),
                "success":function(data){
                  $(\'#addGroupDialog\').html(data);
                }
              })
            }',
          ),
  ),
  ));
?>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>
<?Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');?>
<?$js = 'function reInstall(){GroupsToSelectWhileRecipientsImport(\''.$this->createUrl('newsletter/GroupsToSelectWhileRecipientsImport').'\','.json_encode($groups).')}';?>
<?Yii::app()->clientScript->registerScript('search',$js.'reInstall();')?>