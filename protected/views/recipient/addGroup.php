<? $form = $this->beginWidget('CActiveForm',array('id'=>'AddGroupForm', 'action'=>$this->createUrl('recipient/GroupAdd'))); ?>
<div class="form">
  <?= $this->renderPartial('//shared/_flash')?>
  <?= $form->errorSummary($group)?>
  <div class="row">
		<?= $form->labelEx($group,'name'); ?>
    <?= $form->textField($group,'name')?>
		<?= $form->error($group,'name'); ?>
	</div>
  <input type="submit" name="save" value="save" onclick="return false;" style="display: none" />
</div>
<? $this->endWidget(); ?>