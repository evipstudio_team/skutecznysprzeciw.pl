<?= $this->renderPartial('//shared/_flash')?>
<div class="form">
  <?php $form=$this->beginWidget('CActiveForm', array('id'=>'GroupEditForm','action'=>$this->createUrl('recipient/groupEdit',array('id'=>$group->id)))); ?>
    <?= $form->errorSummary($group); ?>
    <div class="row">
      <?= $form->labelEx($group,'name'); ?>
      <?= $form->textField($group,'name',array('size'=>60,'maxlength'=>128)); ?>
      <?= $form->error($group,'name'); ?>
    </div>
  <?php $this->endWidget(); ?>
</div>