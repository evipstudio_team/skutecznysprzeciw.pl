<div class="click_here">
	<?= CHtml::link(Yii::t('cms', 'Kliknij tutaj'),'#addGroup',array('onclick'=>'$(\'#AddGroup\').dialog(\'open\')')) ?> 
</div>
<div class="to_add">
	aby dodać nową grupę.
</div>

<?

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'groups-grid',
    'dataProvider' => $group->search(),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/gridview/gridview.css',
    'enablePagination'=> false,
    'columns' => array(
        'name',
        'recipientsActiveCount',
        'recipientsInActiveCount',
//        array(
//            'name'=>  Yii::t('cms', 'Usuń odbiorców'),
//            'value'=>'"<a href=\"\">wyczyść</a>"',
//            'htmlOptions'=>array('onclick'=>'if(confirm("Czy jesteś pewien że chcesz usunąć odbiorców z wybranej grupy?"))','style'=>'padding-right: 10px; padding-left: 0px !important;text-align: right'),
//            'type'=>'html',
//            'filter'=>false
//        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}',
            'header'=>  Yii::t('cms', 'Edycja'),
            'buttons' => array(
                'update' => array(
                    'label'=>  Yii::t('cms', 'Edycja'),
                    'url' => 'Yii::app()->createUrl("recipient/groupEdit", array("id"=>$data->id))',
                    'click' => "function() {
                      $.ajax({
                        'url': $(this).attr('href'),
                        success: function(data) {
                          $('#GroupEdit').html(data);
                          $('#GroupEdit').dialog('open');
                        }
                      })
                      return false;}",
                ),
            )
        ),
    ),
));
?>

<?$this->beginWidget('zii.widgets.jui.CJuiDialog', array('id'=>'AddGroup','options'=>array(
    'autoOpen'=>false,
    'title'=>  Yii::t('cms', 'Dodaj nową grupę'),
    'modal'=>true,
    'width'=>'auto',
    'close'=>'js:function(){$.fn.yiiGridView.update(\'groups-grid\');}',
    'open'=>'js:function(){
      $.ajax({
            \'url\': \''.$this->createUrl('recipient/groupAdd').'\',
            success: function(data) {
              $(\'#AddGroup\').html(data);
            }
          })
    }',
    'buttons'=>array(
        Yii::t('cms', 'Zamknij')=>'js:function(){$(this).dialog("close")}',
        Yii::t('cms', 'Zapisz')=>'js:function(){
          $.ajax({
            \'url\': $(\'#AddGroupForm\').attr(\'action\'),
            \'type\':\'post\',
            \'data\':$(\'#AddGroupForm\').serializeArray(),
            success: function(data) {
              $(\'#AddGroup\').html(data);
            }
          })
          }',
        )
    ))
);
$this->endWidget('zii.widgets.jui.CJuiDialog');?>

<?$this->beginWidget('zii.widgets.jui.CJuiDialog', array('id'=>'GroupEdit','options'=>array(
    'autoOpen'=>false,
    'title'=>  Yii::t('cms', 'Edycja grupy odbiorców'),
    'modal'=>true,
    'width'=>'auto',
    'close'=>'js:function(){$.fn.yiiGridView.update(\'groups-grid\');}',
    'buttons'=>array(
        Yii::t('cms', 'Zamknij')=>'js:function(){$(this).dialog("close")}',
        Yii::t('cms', 'Zapisz zmiany')=>'js:function(){
          $.ajax({
            \'url\': $(\'#GroupEditForm\').attr(\'action\'),
            \'type\':\'post\',
            \'data\':$(\'#GroupEditForm\').serializeArray(),
            success: function(data) {
              $(\'#GroupEdit\').html(data);
            }
          })
          }',
        )
    ))
);
$this->endWidget('zii.widgets.jui.CJuiDialog');?>