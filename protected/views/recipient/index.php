<?

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'recipients-grid',
    'dataProvider' => $recipient->search($group->id),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/gridview/gridview.css',
    'filter' => $recipient,
    'columns' => array(
        'email',
        array(
            'name'=>  Yii::t('cms', 'Grupy'),
            'value'=>'implode(\', \',CHtml::listData($data->groups,\'id\',\'name\'))',
            'filter'=>  CHtml::activeDropDownList($group, 'id', array(''=>'')+CHtml::listData(Group::model()->findAll(), 'id', 'name'))
        ),
        array(
            'name'=>'status',
            'value'=>'$data->TranslateEnumValue(\'status\')',
            'filter'=>  CHtml::activeDropDownList($recipient, 'status', array(''=>'')+ZHtml::enumItem($recipient, 'status'))
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}',
            'header'=>  Yii::t('cms', 'Edytuj'),
            'buttons' => array(
                'update' => array(
                    'label'=>  Yii::t('cms', 'Edytuj'),
                    'url' => 'Yii::app()->createUrl("recipient/view", array("id"=>$data->id))',
                    'click' => "function() {
                      $.ajax({
                        'url': $(this).attr('href'),
                        success: function(data) {
                          $('#RecipientView').html(data);
                          $('#RecipientView').dialog('open');
                        }
                      })
                      return false;}",
                ),
            )
        ),
    ),
));
?>

<?$this->beginWidget('zii.widgets.jui.CJuiDialog', array('id'=>'RecipientView','options'=>array(
    'autoOpen'=>false,
    'title'=>  Yii::t('cms', 'Karta odbiorcy'),
    'modal'=>true,
    'width'=>'auto',
    'close'=>'js:function(){$.fn.yiiGridView.update(\'recipients-grid\');}',
    'buttons'=>array(
        Yii::t('cms', 'Zamknij')=>'js:function(){$(this).dialog("close")}',
        Yii::t('cms', 'Zapisz zmiany')=>'js:function(){
          $.ajax({
            \'url\': $(\'#RecipientViewForm\').attr(\'action\'),
            \'type\':\'post\',
            \'data\':$(\'#RecipientViewForm\').serializeArray(),
            success: function(data) {
              $(\'#RecipientView\').html(data);
            }
          })
          }',
        )
    ))
);
$this->endWidget('zii.widgets.jui.CJuiDialog');?>