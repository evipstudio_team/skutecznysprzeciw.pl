<?= $this->renderPartial('//shared/_flash')?>
<div class="form">
  <?php $form=$this->beginWidget('CActiveForm', array('id'=>'RecipientViewForm','action'=>$this->createUrl('recipient/view',array('id'=>$recipient->id)))); ?>
    <?= $form->errorSummary($recipient); ?>
    <div class="row">
      <?= $form->labelEx($recipient,'email'); ?>
      <?= $form->textField($recipient,'email',array('size'=>60,'maxlength'=>128)); ?>
      <?= $form->error($recipient,'email'); ?>
    </div>
    <div class="row">
      <?= $form->labelEx($recipient,'status'); ?>
      <?= ZHtml::enumDropDownList($recipient, 'status') ?>
      <?= $form->error($recipient,'status'); ?>
    </div>
  <fieldset>
    <legend><?= Yii::t('cms', 'Grupy do których należy odbiorca') ?></legend>
    <?foreach(Group::model()->findAll() as $group):?>
      <?$id = 'group_'.$group->id?>
      <span style="white-space: nowrap; margin-right: 10px">
        <label for="<?= $id?>" style="white-space:nowrap;">
          <input type="checkbox" name="groups[]" id="<?=$id?>" value="<?= $group->id?>"<?if(in_array($group->id, $recipient_groups)):?>checked="checked"<?endif?> /> - <?= $group->name?>
        </label>
      </span>
    <?endforeach?>
  </fieldset>
  <?php $this->endWidget(); ?>
</div>