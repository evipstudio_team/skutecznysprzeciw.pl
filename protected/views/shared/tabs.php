<?
$this->widget('CTabView', array(
    'tabs' => $tabs,
    'cssFile' => Yii::app()->theme->baseUrl.'/css/jquery.yiitab.css',
    'activeTab'=>Yii::app()->getController()->action->id,
    'viewData'=>  isset($additionalParams)? array_merge($additionalParams,array('model'=>$model)):array('model'=>$model)
))
?>