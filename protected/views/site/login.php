<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('cms', 'Panel logowania');
Yii::app()->clientScript->registerScript('LoginForm_username', "$('#LoginForm_username').blur();$('#LoginForm_password').blur()");
?>
<div class="kontener">
  <div class="header">
    <img class="arrow" src="<?= Yii::app()->theme->baseUrl; ?>/images/login/header_arrow.png" alt=""/>
    <span class="text"> Panel logowania</span>
    <img class="logo" src="<?= Yii::app()->theme->baseUrl; ?>/images/login/header_logo.png" alt=""/>
  </div>
  <p class="info">Wypełnij poniższe dane aby się zalogować.</p>
  <?if($model->getErrors('password')):?>
    <p class="info" style="color: red"><?= implode('<br />',$model->getErrors('password'))?></p>
  <?endif?>
  <div class="formularz">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'formularz',
        'enableClientValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),));?>
    <div class="input">
      <?php echo $form->textField($model, 'username', array('onfocus'=>  defaultOnFocus('Login'),'onblur'=> defaultOnBlur('Login'))); ?>
      <img class="icon" src="<?= Yii::app()->theme->baseUrl; ?>/images/login/user.png" alt="<?= Yii::t('cms', 'Login') ?>"/>
    </div>
    <div class="input">
      <?php echo $form->passwordField($model, 'password', array('onfocus'=>  defaultOnFocus('Hasło'),'onblur'=> defaultOnBlur('Hasło'))); ?>
      <img class="icon" src="<?= Yii::app()->theme->baseUrl; ?>/images/login/pass.png" alt="<?= Yii::t('cms', 'Hasło') ?>"/>
    </div>

    <div style="text-align:center; margin-top:8px;">
      <input type="submit" name="send" class="button" style="float: left" value="Wyślij"/>
      <input type="reset" name="reset" class="button" style="float: right" value="Wyczyść" />
    </div>
    <?php $this->endWidget(); ?>
  </div>
  <p class="copyright">Wszelkie prawa zastrzeżone. Powered by <a href="http://www.evipstudio.pl" target="_blank">Evipstudio.pl</a></p>
</div>
