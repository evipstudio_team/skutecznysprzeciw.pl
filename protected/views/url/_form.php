<div class="form">
<?$form=$this->beginWidget('CActiveForm'); ?>
  <?= $form->errorSummary($model)?>
  <div id="additional">
    <div class="row">
      <?php echo $form->labelEx($model, 'anchor'); ?>
      <?php echo $form->textField($model, 'anchor', array('size' => 60)); ?>
      <?php echo $form->error($model, 'anchor'); ?>
    </div>
    <div class="row">
      <?php echo $form->labelEx($model, 'address'); ?>
      <?php echo $form->textField($model, 'address', array('size' => 60)); ?>
      <?php echo $form->error($model, 'address'); ?>
    </div>
  </div>
  <div class="row">
    <?php echo $form->labelEx($model, 'autoupdate'); ?>
    <?= ZHtml::enumDropDownList($model, 'autoupdate',array('onchange'=>'deactiveForm();')) ?>
    <?php echo $form->error($model, 'autoupdate'); ?>
  </div>

  <div class="row">
    <?php echo $form->labelEx($model, 'blank'); ?>
    <?= ZHtml::enumDropDownList($model, 'blank') ?>
    <?php echo $form->error($model, 'blank'); ?>
  </div>
  <div class="row">
    <?php echo $form->labelEx($model, 'inheritance'); ?>
    <?= $form->dropDownList($model, 'inheritance', ZHtml::enumItem($model, 'inheritance')) ?>
    <?php echo $form->error($model, 'inheritance'); ?>
  </div>
  <div class="row">
    <?php echo $form->labelEx($model, 'title'); ?>
    <?php echo $form->textField($model, 'title', array('size' => 60)); ?>
    <?php echo $form->error($model, 'title'); ?>
  </div>
  <div class="row">
    <?php echo $form->labelEx($model, 'description'); ?>
    <?php echo $form->textArea($model, 'description', array('rows' => 10, 'cols'=>49)); ?>
    <?php echo $form->error($model, 'description'); ?>
  </div>
  <div class="row">
    <?php echo $form->labelEx($model, 'keywords'); ?>
    <?php echo $form->textArea($model, 'keywords', array('rows' => 10, 'cols'=>49)); ?>
    <?php echo $form->error($model, 'keywords'); ?>
  </div>




<div class="button_bar">
  <?if($model->getIsNewRecord()==false):?>
	  <div class="button_delete">
      <?= CHtml::link(Yii::t('cms', 'Usuń adres w języku').' '.$lang->getNameVariety(), $this->createUrl('url/delete',array('page_id'=>$model->page_id,'lang_id'=>$model->lang_id)), array('onclick'=>'return confirm(\''.  Yii::t('cms', 'Czy jesteś pewien że chcesz usunąć element w języku').' '.$lang->getNameVariety().'?\')'))?>
    </div>
  <?endif?>
  <div class="button_add">
    <?= CHtml::button(Yii::t('cms', 'Zapisz dane w języku'.' '.$lang->getNameVariety()),array('onclick'=>'$.ajax({
      "url":"'.$this->createUrl('url/edit',array('page_id'=>$model->page_id,'lang_id'=>$model->lang_id)).'",
        "type":"post",
        "data":$(this).closest("form").serializeArray(),
        "success":function(data) {$("#UrlsListing").html(data)}
      })')) ?>
    </div>
</div>
<?php $this->endWidget(); ?>
</div>
<?
Yii::app()->clientScript->registerScript('deactiveForm', "
function deactiveForm() {
  if($('#Url_autoupdate').val()==1) $('#additional').slideUp(300);
  else $('#additional').slideDown(300);
}
$(function(){
  deactiveForm();
});",0);
?>