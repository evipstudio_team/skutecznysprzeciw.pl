<?= $this->renderPartial('//shared/_flash')?>
<?Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/helpers.js');?>
<?if(count(Yii::app()->params['langs'])>1):?>
  <?$tabs = array();?>
  <?foreach (Yii::app()->params['langs'] as $lang):?>
    <?$href = $this->createUrl($page->module->controller.'/urls',array('id'=>$page->id,'lang_id'=>$lang->id));?>
    <?$title = getLangIcon(Url::model()->findByPk(array('page_id'=>$attributes['page_id'],'lang_id'=>$lang->id)), $lang).' '.Yii::t('cms', 'Język').' '.$lang->name;?>
    <?if($lang->id==$url->lang_id):?>
      <?  $tabs['Url_'.$lang->id]=array(
        'title' => $title,
        'view'=>$template,
        'data'=>array('model'=>$url, 'lang'=>$lang, 'page_id'=>$attributes['page_id'])
      )?>
    <?else:?>
      <?  $tabs['ArticleTranslation_'.$lang->id]=array(
        'title' => $title,
        'url' => $href,
      )?>
    <?endif?>
  <?endforeach?>
  <?$this->widget('CTabView',array('tabs'=>$tabs,'id'=>'urls','activeTab'=>'Url_'.$url->lang_id));?>
<?else:?>
  <?= $this->renderPartial($template,array('model'=>$url, 'lang'=>Yii::app()->params['lang'], 'page_id'=>$attributes['page_id']))?>
<?endif?>



