<div class="form">
<?$form=$this->beginWidget('CActiveForm'); ?>
  <?= $form->errorSummary($model)?>
  <div id="additional">
    <div class="row">
      <?php echo $form->labelEx($model, 'anchor'); ?>
      <?php echo $form->textField($model, 'anchor', array('size' => 60)); ?>
      <?php echo $form->error($model, 'anchor'); ?>
    </div>
    <div class="row">
      <?php echo $form->labelEx($model, 'address'); ?>
      <?php echo $form->textField($model, 'address', array('size' => 60)); ?>
      <?php echo $form->error($model, 'address'); ?>
    </div>
  </div>

  <div class="row">
    <?php echo $form->labelEx($model, 'blank'); ?>
    <?= ZHtml::enumDropDownList($model, 'blank') ?>
    <?php echo $form->error($model, 'blank'); ?>
  </div>

  <div class="button_bar">
<?if($model->getIsNewRecord()==false):?>


	  <div class="button_delete">
      <?= CHtml::link(Yii::t('cms', 'Usuń adres w języku').' '.$lang->getNameVariety(), $this->createUrl('url/delete',array('module_id'=>$module_id,'element_id'=>$model->element_id,'lang_id'=>$model->lang_id)), array('onclick'=>'return confirm(\''.  Yii::t('cms', 'Czy jesteś pewien że chcesz usunąć element w języku').' '.$lang->getNameVariety().'?\')'))?>
    </div>
<?endif?>
<div class="button_add">
  <?= CHtml::button(Yii::t('cms', 'Zapisz dane w języku'.' '.$lang->getNameVariety()),array('onclick'=>'$.ajax({
    "url":"'.$this->createUrl('url/edit',array('module_id'=>$module_id,'element_id'=>$model->element_id,'lang_id'=>$model->lang_id)).'",
      "type":"post",
      "data":$(this).closest("form").serializeArray(),
      "success":function(data) {$("#UrlsListing").html(data)}
    })')) ?>
	</div>
</div>
<?php $this->endWidget(); ?>
</div>