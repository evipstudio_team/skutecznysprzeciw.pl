<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

  <p class="note"><?= Yii::t('Fields required', 'Pola oznaczone <span class="required">*</span> są wymagane')?>.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

  <div class="row">
		<?php echo $form->labelEx($model,'surname'); ?>
		<?php echo $form->textField($model,'surname',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'surname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
  <div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>
  <div class="row">
    <?php echo $form->labelEx($model,'password'); ?>
    <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>40, 'value'=>'')); ?>
    <?php echo $form->error($model,'password'); ?>
  </div>
  <div class="row">
    <?php echo $form->labelEx($model,'repeat_password'); ?>
    <?php echo $form->passwordField($model,'repeat_password',array('size'=>60,'maxlength'=>40, 'value'=>'')); ?>
    <?php echo $form->error($model,'password'); ?>
  </div>
  <div class="row">
    <?php echo $form->labelEx($model,'role'); ?>
    <?php echo $form->dropDownList($model,'role',array(''=>'')+User::model()->findByPk(Yii::app()->user->id)->getPossibleRoles())?>
    <?php echo $form->error($model,'role'); ?>
  </div>
  <div class="row">
    <?php echo $form->labelEx($model,'status'); ?>
    <?php echo $form->dropDownList($model,'status',ZHtml::enumItem($model, 'status'))?>
    <?php echo $form->error($model,'status'); ?>
  </div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('cms','Utwórz nowego użytkownika') : Yii::t('cms','Zapisz zmiany')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->