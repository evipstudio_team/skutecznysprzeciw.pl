<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

  <p class="note"><?= Yii::t('Fields required', 'Pola oznaczone <span class="required">*</span> są wymagane')?>.</p>

	<?php echo $form->errorSummary($model); ?>
  <?if(!$model->isNewRecord):?>
    <div class="row">
      <?php echo $form->labelEx($model,'password'); ?>
      <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>40)); ?>
      <?php echo $form->error($model,'password'); ?>
    </div>

    <div class="row">
      <?php echo $form->labelEx($model,'repeat_password'); ?>
      <?php echo $form->passwordField($model,'repeat_password',array('size'=>60,'maxlength'=>40)); ?>
      <?php echo $form->error($model,'password'); ?>
    </div>
  <?endif?>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('cms','Utwórz nowego użytkownika') : Yii::t('cms','Zapisz zmiany')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->