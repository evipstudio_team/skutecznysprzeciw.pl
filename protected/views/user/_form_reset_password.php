<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'focus' => array($model, 'email'),
)); ?>

  <p class="note">
    <?= Yii::t('cms', 'Poniższy formularz pomaga w procesie resetu hasła. W przypadku gdy nie pamiętasz swojego hasła użytkownika, wprowadź adres e-mail na który zostało założone konto. Na wskazany adres zostanie wysłana wiadomość z linkiem po kliknięciu którego przypisane zostanie nowe hasło, które również zostanie wysłane na Twój adres e-mail')?>.
    <br />
    <br />
    <?= Yii::t('cms', 'Pola oznaczone <span class="required">*</span> są wymagane')?>.
  </p>

	<?php echo $form->errorSummary($model); ?>
    <div class="row">
      <?php echo $form->labelEx($model,'email'); ?>
      <?php echo $form->textField($model,'email',array('size'=>40,'maxlength'=>128)); ?>
      <?php echo $form->error($model,'email'); ?>
    </div>
    <?php if(CCaptcha::checkRequirements()): ?>
      <div class="row">
        <div class="hint"><?= Yii::t('cms','Prosimy o przepisanie treści z obrazka, system nie rozróżnia wielkości liter.')?><br /><?= Yii::t('cms','W przypadku problemów z odczytaniem obrazka, kliknij w link "Pobierz nowy kod".')?></div>
        <div>
          <?php $this->widget('CCaptcha'); ?>
          <?php echo $form->textField($model,'verifyCode'); ?>
        </div>
        <?php echo $form->error($model,'verifyCode'); ?>
      </div>
    <?php endif; ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('cms','Resetuj hasło')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->