<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

  <p class="note"><?= Yii::t('Fields required', 'Pola oznaczone <span class="required">*</span> są wymagane')?>.</p>
	<?php echo $form->errorSummary($model); ?>

  <div class="row">
      <?php echo $form->labelEx($model,'status'); ?>
      <?= ZHtml::enumDropDownList($model,'status' );?>
      <?php echo $form->error($model,'status'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('cms','Zapisz zmiany')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->