<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Użytkownicy')=>$this->createUrl('/user/index'),
	Yii::t('cms', 'Dodaj użytkownika'),
);
?>

<h1><?= Yii::t('cms', 'Dodaj użytkownika')?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>