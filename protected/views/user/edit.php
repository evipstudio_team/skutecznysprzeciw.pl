<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Użytkownicy')=>$this->createUrl('user/index'),
  Yii::t('cms', 'Edycja użytkownika')
);
?>

<?= $this->renderPartial('_form',array('model'=>$user))?>