<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Użytkownicy') => $this->createUrl('/user/index'),
	$model->name." ".$model->surname." ".$model->email=>$this->createUrl('/user/view', array('id'=>$model->id)),
	Yii::t('cms', 'Edycja'),
);

$this->menu=array(
  array('label'=>Yii::t('cms', 'Zmień hasło'), 'url'=>$this->createUrl('/user/ChangePass', array('id'=>$model->id)), 'visible'=>Yii::app()->user->checkAccess('User:ChangePass')),
    array('label'=>Yii::t('cms', 'Zmień status'), 'url'=>$this->createUrl('/user/ChangeStatus', array('id'=>$model->id)), 'visible'=>Yii::app()->user->checkAccess('User:ChangeStatus')),
	array('label' => Yii::t('cms', 'Lista użytkowników'), 'url' => $this->createUrl('/user/index'), 'visible' => Yii::app()->user->checkAccess('User:Index')),
	array('label' => Yii::t('cms', 'Dodaj użytkownika'), 'url' => $this->createUrl('/user/create'), 'visible' => Yii::app()->user->checkAccess('User:Create')),
	array('label'=>Yii::t('cms', 'Karta użytkownika'), 'url'=>$this->createUrl('/user/view', array('id'=>$model->id)), 'visible' => Yii::app()->user->checkAccess('User:View')),
	array('label'=>Yii::t('cms', 'Zarządzaj użytkownikami'), 'url'=>$this->createUrl('/user/admin'), 'visible'=>Yii::app()->user->checkAccess('User:Admin')),
);
?>

<h1><?= Yii::t('cms', 'Edycja użytkownika')?>:<br />#<?=$model->id?> <?= $model->name?> <?= $model->surname?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>