<?php
$this->breadcrumbs=array(
	Yii::t('cms', 'Użytkownicy') => $this->createUrl('/user/index'),
	$model->name." ".$model->surname." ".$model->email,
);

$this->menu=array(
	array('label' => Yii::t('cms', 'Lista użytkowników'), 'url' => $this->createUrl('/user/index'), 'visible' => Yii::app()->user->checkAccess('User:Index')),
	array('label' => Yii::t('cms', 'Dodaj użytkownika'), 'url' => array('/user/create'), 'visible' => Yii::app()->user->checkAccess('User:Create')),
	array('label'=>Yii::t('cms', 'Edycja'), 'url'=>$this->createUrl('/user/update', array('id'=>$model->id)), 'visible' => Yii::app()->user->checkAccess('User:Update')),
	array('label'=>Yii::t('cms', 'Usuń użytkownika'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>Yii::t('cms','Czy jesteś pewien tej operacji?')), 'visible'=>Yii::app()->user->checkAccess('User:Delete')),
	array('label'=>Yii::t('cms', 'Zarządzaj użytkownikami'), 'url'=>array('admin'), 'visible'=>Yii::app()->user->checkAccess('User:Admin')),
);
?>

<h1><?= Yii::t('cms','Karta użytkownika')?> #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
    'surname',
		'email',
		'status',
	),
)); ?>
