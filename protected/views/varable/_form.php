<?=$form->errorSummary($model)?>
<div class="row">
  <?=$form->labelEx($model,'user_id'); ?>
  <?= $form->dropDownList($model,'user_id',array(''=>'')+CHtml::listData(User::model()->findAll(),'id','email'))?>
  <?=$form->error($model,'user_id'); ?>
</div>
<?if($model->getIsNewRecord()):?>
<div class="row">
    <?=$form->labelEx($model,'name'); ?>
    <?=$form->textField($model, 'name', array('size'=>60)) ?>
    <?=$form->error($model,'name'); ?>
  </div>
<?endif?>
<div class="row">
  <?=$form->labelEx($model,'description'); ?>
  <?=$form->textArea($model, 'description', array('rows'=>4, 'cols'=>50, 'style'=>'width:99%')) ?>
  <?=$form->error($model,'description'); ?>
</div>
<div class="row">
  <?=$form->labelEx($model,'value'); ?>
  <?=$form->textArea($model, 'value', array('rows'=>3, 'cols'=>50, 'style'=>'width:99%')) ?>
  <?=$form->error($model,'value'); ?>
</div>