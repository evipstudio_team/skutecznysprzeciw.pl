<?
$this->menu = array(
    array(
        'label' => Yii::t('cms', 'Lista zmiennych'),
        'url' => $this->createUrl('varable/index')
    ),
    array(
        'label' => Yii::t('cms', 'Dodaj zmienną'),
        'url' => $this->createUrl('varable/create'),
        'linkOptions'=>array('onclick'=>"$('#CreateVarableDialog').dialog('open'); return false;")
    ),
);
?>

<?$this->beginWidget('zii.widgets.jui.CJuiDialog', array('id'=>'CreateVarableDialog','options'=>array(
    'autoOpen'=>false,
    'title'=>  Yii::t('cms', 'Formularz zmiennej aplikacji'),
    'modal'=>true,
    'width'=>'700px',
    'close'=>'js:function(){$.fn.yiiGridView.update(\'varables-grid\');}',
    'open'=>'js:function(){$.ajax({\'url\':\''.$this->createUrl('varable/create').'\',\'success\':function(data){$(\'#CreateVarableDialog\').html(data)}});return false;}',
    'buttons'=>array(
        Yii::t('cms', 'Zamknij')=>'js:function(){$(this).dialog("close")}',
        Yii::t('cms', 'Zapisz')=>'js:function(){
          $.ajax({
            \'url\': $(\'#CreateVarableForm\').attr(\'action\'),
            \'type\':\'post\',
            \'data\':$(\'#CreateVarableForm\').serializeArray(),
            success: function(data) {
              $(\'#CreateVarableDialog\').html(data);
            }
          })
          }',
        )
    ))
);
$this->endWidget('zii.widgets.jui.CJuiDialog');?>