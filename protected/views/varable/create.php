<?= $this->renderPartial('//shared/_flash')?>
<div class="form">
  <? $form = $this->beginWidget('CActiveForm',array('id'=>'CreateVarableForm','action'=>$this->createUrl('varable/create'))); ?>
  <?= $this->renderPartial('_form',array('form'=>$form,'model'=>$model))?>
  <div class="button_bar">
  <div class="button_add">
		<?php echo CHtml::submitButton(Yii::t('cms', 'Zapisz')); ?>
	</div>
	</div>
  <? $this->endWidget(); ?>
</div>