<?= $this->renderPartial('//shared/_flash')?>
<div class="form">
  <? $form = $this->beginWidget('CActiveForm',array('id'=>'EditVarableForm','action'=>$this->createUrl('varable/edit',array('name'=>$varable->name,'user_id'=>$varable->user_id)))); ?>
  <?= $this->renderPartial('_form',array('form'=>$form,'model'=>$varable))?>
  <? $this->endWidget(); ?>
</div>