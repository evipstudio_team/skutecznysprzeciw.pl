<?$this->renderPartial('_leftMenu');?>
<div class="form">
  <p class="warning">Uwaga, poniższe zmienne dotyczą działania newralgicznych części aplikacji, prosimy o zachowanie ostrożności w ich edycji.</p>
</div>
<?
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'varables-grid',
    'dataProvider' => $model->search(),
    'cssFile' => Yii::app()->theme->baseUrl.'/css/gridview/gridview.css',
	 //'PagerCssClass' => Yii::app()->theme->baseUrl.'/css/gridview/gridview.css',
    'filter' => $model,
    'summaryText'=>summaryTextLayout('varables-grid'),
    'columns' => array(
        'description',
        'value',
        array(
            'name'=>'name',
            'htmlOptions'=>array('style'=>'padding-left: 0px !important; text-align: right;padding-right: 10px'),
        ),
        array(
            'name'=>'User',
            'value'=>'$data->UserIdentity',
            'filter'=>CHtml::activeDropDownList($model, 'user_id', array(''=>'')+CHtml::listData(User::model()->findAll(), 'id', 'email') ),
            'htmlOptions'=>array('style'=>'padding-left: 0px !important; text-align: right;padding-right: 10px'),
        ),
        array(
            'class' => 'SButtonColumn',
            'template' => '{update}',
            'header'=>  Yii::t('cms', 'Edytuj'),
            'htmlOptions'=>array('class'=>'icons'),
            'buttons' => array(
                'update' => array(
                    'label' => Yii::t('cms', 'Edytuj'),
                    'url' => 'Yii::app()->createUrl("varable/edit", array("name"=>$data->name,"user_id"=>$data->user_id))',
                    'click' => "function() {
                      $.ajax({
                        url: $(this).attr('href'),
                        success: function(data) {
                          $('#EditVarableDialog').html(data);
                          $('#EditVarableDialog').dialog('open');
                        }
                      });
                      return false;}
                      ",
                ),
            )
        ),
    ),
));
?>
<?$this->beginWidget('zii.widgets.jui.CJuiDialog', array('id'=>'EditVarableDialog','options'=>array(
    'autoOpen'=>false,
    'title'=>  Yii::t('cms', 'Edycja zmiennej aplikacji'),
    'modal'=>true,
    'width'=>'700px',
    'close'=>'js:function(){$.fn.yiiGridView.update(\'varables-grid\');}',
    'buttons'=>array(
        Yii::t('cms', 'Zamknij')=>'js:function(){$(this).dialog("close")}',
        Yii::t('cms', 'Zapisz zmiany')=>'js:function(){
          $.ajax({
            \'url\': $(\'#EditVarableForm\').attr(\'action\'),
            \'type\':\'post\',
            \'data\':$(\'#EditVarableForm\').serializeArray(),
            success: function(data) {
              $(\'#EditVarableDialog\').html(data);
            }
          })
          }',
        )
    ))
);
$this->endWidget('zii.widgets.jui.CJuiDialog');?>