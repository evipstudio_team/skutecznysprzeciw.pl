$(document).ready(function() {

    "use strict";

    /* FUNCTIONS 
    =================================================================*/

    /* Plugin
    --------------------------------------------*/
    var plugin = {
        toggleVal: function(target) {
            if ($(target).length){ 
                $(target).toggleVal();
            }
        },
        iLightbox: function(target) {
             if ($(target).length) {
                $(target).iLightBox({
                    skin: 'metro-black',
                    overlay: { opacity: 0.5 },
                    controls: {
                        arrows: true,
                        slideshow: true
                    },
                    thumbnails: {
                        normalOpacity: 1,
                        activeOpacity: 1
                    },
                    path: 'horizontal',
                    text: {
                        close: 'Zamknij',
                        next: 'Następne',
                        previous: 'Poprzednie',
                        enterFullscreen: 'Przejdź do widoku pełnoekranowego',
                        exitFullscreen: 'Zamknij widok pełnoekranowy',
                        slideShow: 'Pokaz slajdów'
                    }
                });
            }           
        }   
    };
    plugin.toggleVal('input[type="text"], input[type="email"], input[type="tel"], textarea');  
    plugin.iLightbox('.ilightbox');

	
});

