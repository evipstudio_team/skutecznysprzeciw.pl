function setRealImgWidth(id) {
  $("<img/>")
  .attr("src", $(id).attr("src"))
  .load(function() {
    if(this.width>500) $(id).css('width',500);
  });
}

this.imagePreview = function(){
  xOffset = 10;
  yOffset = 30;
  $("a.preview").hover(function(e){
    var actualWidth = $(window).width();
    this.t = this.title;
    this.title = "";
    var c = (this.t != "") ? "<br />" + this.t : "";
    $("body").append("<p id='preview'><img id='previewImg' src='"+ this.href +"' alt='Image preview' />"+ c +"</p>");
    setRealImgWidth('#previewImg');
    if((actualWidth/2)>e.pageX) {
      $("#preview")
      .css("top",(e.pageY-($('#preview').height()/2) - xOffset) + "px")
      .css("left",(e.pageX + yOffset) + "px")
      .fadeIn("fast");
    }
    else {
      $("#preview")
      .css("top",(e.pageY-($('#preview').height()/2) - xOffset) + "px")
      .css("right",(actualWidth-e.pageX + yOffset) + "px")
      .fadeIn("fast");
    }

  },
  function(){
    this.title = this.t;
    $("#preview").remove();
  });
  $("a.preview").mousemove(function(e){
    var actualWidth = $(window).width();
    if((actualWidth/2)>e.pageX) {
      $("#preview")
      .css("top",(e.pageY-($('#preview').height()/2) - xOffset) + "px")
      .css("left",(e.pageX + yOffset) + "px");
    }
    else {
      $("#preview")
      .css("top",(e.pageY-($('#preview').height()/2) - xOffset) + "px")
      .css("right",(actualWidth-e.pageX + yOffset) + "px");
    }

  });
};

$(function(){
  imagePreview();
});