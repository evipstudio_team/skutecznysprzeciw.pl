/* JQUERY *
=================================================================*/
$(document).ready(function(){

    /* APP
    ----------------------------------------------*/
    var APP = {};

    /* trigger
    ----------------------------------------------*/
    APP.trigger = {
    	dom: function() {
    		if ($('.center td img').length) {
    			$('.center td img').parent().addClass('image');
    		}
    	},    	
    	slideMenu: function(target) {
    		$(window).on('resize', function() {
				if (window.innerWidth > 1100) {
                    if ($(target).length) {
                        $(target).mouseenter(function() {
                            $(this).stop().animate({
                                left: 0
                            }, 500);
                        }).mouseleave(function() {
                            $(this).stop().animate({
                                left: -80
                            }, 500);
                        });
                    }
				}
			}).trigger('resize');
    	},
    	slideFAQ: function() {
    		if ($('.faqLink').length) {
			    $('.faqLink').click(function() {
				    if ($(this).hasClass('active')) {
				        $(this).removeClass('active');
				       	var divInstance = $(this).parent().parent();
				        $(divInstance).find('div.title a').removeClass('active');
				        $(divInstance).find('div.content').slideUp(800);
				    } else {
				        var divInstance = $(this).parent().parent();
				        $(divInstance).addClass('active');
				        $(this).addClass('active');
				        var divContent = $(divInstance).find('div.content');
				        $(divContent).slideDown(800);
				    }
			    });
			}
		    if (window.location.hash) {
		    	if ($('#faq div.question div.title a').length) {
			    	$('#faq div.question div.title a').each(function() {
			        if (window.location.hash == $(this).attr('href')) {
			          	$('html, body').animate({
			            	scrollTop: $(this).offset().top
			          	}, 3000);
			          	$(this).click();
			          	return false;
			        }
			    });		    		
			  }
		    }
    	},
    	mobileMenu: function(target) {
    		if ($(target).length) {
    			$(target).before('<div class="nav_trigger"><span class="line"></span><span class="line"></span><span class="line"></span></div>');
    			$('.nav_trigger').on('click', function() {
    				$(this).next().slideToggle();
    			});
    		}
    	},
    	hideKeyboard: function(target) {
    		if ($(target).length) {
    			$(target).attr('readonly', 'readonly').on('focus', function() {
    				var that = $(this);
    				setTimeout(function() {
    					that.trigger('blur');
    				}, 100);
				});
    		}
    	},
    	blur: function(target) {
    		if ($(target).length) {
    			$(target).each(function() {
    				$(this).blur();
    			});
    		}
    	},
    	checkbox: function(target, checkbox) {
    		if ($(target).length && $(checkbox).length) {
    			$(checkbox).on('click', function() {
	  				if ($(target).val()) {
	  					$(target).val(''); 
	  					$(this).css('background-position','0px 0px');
	  				} else {
	  					$(target).val('1'); 
	  					$(this).css('background-position','0px -50px');
	  				}  	
    			});		
    		}
    	},
    	submit: function(target) {
    		if ($(target).length) {
			    $(window).bind('beforeunload', function(){
			      return 'Proces rejestracji sprawy nie został jeszcze zakończony. Czy na pewno chcesz go przerwać?';
			    });
			    $(target).click(function() {
			      $(window).unbind('beforeunload');
			    });    			
    		}
    	},
    	send: function(target) {
    		if ($(target).length) {
    			$(target).on('click', function() {
		            $('#LoadingDialogBox').dialog('open');
		            $.ajax({
		                'url': $('#formularz').attr('action'),
		                'type':'post',
		                'data': $('#formularz').serializeArray(),
		                'success': function(data) {
		                	$('#LoadingDialogBox').dialog('close');
		                  	$('#form').html(data);
		               	}
		            }); 
    			});
    		}
    	},
    	facebook: function() {
    		$('body').prepend('<div id="fb-root"></div>');
    		(function(d, s, id) { 
				var js, 
					fjs = d.getElementsByTagName(s)[0]; 
		      	if (d.getElementById(id)) {
		      		return;
		      	} 
		      	js = d.createElement(s); 
		      	js.id = id; 
		      	js.src = "//connect.facebook.net/pl_PL/all.js#xfbml=1";
		      	fjs.parentNode.insertBefore(js, fjs); 
    		})(document, 'script', 'facebook-jssdk');
    	},
		truncate: function(target) {
            if ($(target).length) {
                $(target).not('.full').each(function() {
                	if (!/[a-zA-z]+/.test($(this).text())) {
                        $(this).hide();
					} else {
                        var content = '<div class="truncate">' + $(this).text() + '</div>';
                        $(this).empty();
                        $(this).html(content);
                        var truncate = $(this).find('.truncate');
                        var contentHeight = truncate.parent().height();
                        while (truncate.outerHeight(true) > contentHeight) {
                            truncate.text(function(index, text) {
                                return text.replace(/\W*\s(\S)*$/, '...');
                            });
                        }
					}
                });
            }
		},
		photoswipe: function(target) {
            var initPhotoSwipeFromDOM = function(gallerySelector) {

                // parse slide data (url, title, size ...) from DOM elements
                // (children of gallerySelector)
                var parseThumbnailElements = function(el) {
                    var thumbElements = el.childNodes,
                        numNodes = thumbElements.length,
                        items = [],
                        figureEl,
                        linkEl,
                        size,
                        item;

                    for(var i = 0; i < numNodes; i++) {

                        figureEl = thumbElements[i]; // <figure> element

                        // include only element nodes
                        if(figureEl.nodeType !== 1) {
                            continue;
                        }

                        linkEl = figureEl.children[0]; // <a> element

                        size = linkEl.getAttribute('data-size').split('x');

                        // create slide object
                        item = {
                            src: linkEl.getAttribute('href'),
                            w: parseInt(size[0], 10),
                            h: parseInt(size[1], 10)
                        };



                        if(figureEl.children.length > 1) {
                            // <figcaption> content
                            item.title = figureEl.children[1].innerHTML;
                        }

                        if(linkEl.children.length > 0) {
                            // <img> thumbnail element, retrieving thumbnail url
                            item.msrc = linkEl.children[0].getAttribute('src');
                        }

                        item.el = figureEl; // save link to element for getThumbBoundsFn
                        items.push(item);
                    }

                    return items;
                };

                // find nearest parent element
                var closest = function closest(el, fn) {
                    return el && ( fn(el) ? el : closest(el.parentNode, fn) );
                };

                // triggers when user clicks on thumbnail
                var onThumbnailsClick = function(e) {
                    e = e || window.event;
                    e.preventDefault ? e.preventDefault() : e.returnValue = false;

                    var eTarget = e.target || e.srcElement;

                    // find root element of slide
                    var clickedListItem = closest(eTarget, function(el) {
                        return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
                    });

                    if(!clickedListItem) {
                        return;
                    }

                    // find index of clicked item by looping through all child nodes
                    // alternatively, you may define index via data- attribute
                    var clickedGallery = clickedListItem.parentNode,
                        childNodes = clickedListItem.parentNode.childNodes,
                        numChildNodes = childNodes.length,
                        nodeIndex = 0,
                        index;

                    for (var i = 0; i < numChildNodes; i++) {
                        if(childNodes[i].nodeType !== 1) {
                            continue;
                        }

                        if(childNodes[i] === clickedListItem) {
                            index = nodeIndex;
                            break;
                        }
                        nodeIndex++;
                    }



                    if(index >= 0) {
                        // open PhotoSwipe if valid index found
                        openPhotoSwipe( index, clickedGallery );
                    }
                    return false;
                };

                // parse picture index and gallery index from URL (#&pid=1&gid=2)
                var photoswipeParseHash = function() {
                    var hash = window.location.hash.substring(1),
                        params = {};

                    if(hash.length < 5) {
                        return params;
                    }

                    var vars = hash.split('&');
                    for (var i = 0; i < vars.length; i++) {
                        if(!vars[i]) {
                            continue;
                        }
                        var pair = vars[i].split('=');
                        if(pair.length < 2) {
                            continue;
                        }
                        params[pair[0]] = pair[1];
                    }

                    if(params.gid) {
                        params.gid = parseInt(params.gid, 10);
                    }

                    return params;
                };

                var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
                    var pswpElement = document.querySelectorAll('.pswp')[0],
                        gallery,
                        options,
                        items;

                    items = parseThumbnailElements(galleryElement);

                    // define options (if needed)
                    options = {

                        fullscreenEl: false,
                        shareEl: false,

                        // define gallery index (for URL)
                        galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                        getThumbBoundsFn: function(index) {
                            // See Options -> getThumbBoundsFn section of documentation for more info
                            var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                                pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                                rect = thumbnail.getBoundingClientRect();

                            return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
                        }

                    };

                    // PhotoSwipe opened from URL
                    if(fromURL) {
                        if(options.galleryPIDs) {
                            // parse real index when custom PIDs are used
                            // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                            for(var j = 0; j < items.length; j++) {
                                if(items[j].pid == index) {
                                    options.index = j;
                                    break;
                                }
                            }
                        } else {
                            // in URL indexes start from 1
                            options.index = parseInt(index, 10) - 1;
                        }
                    } else {
                        options.index = parseInt(index, 10);
                    }

                    // exit if index not found
                    if( isNaN(options.index) ) {
                        return;
                    }

                    if(disableAnimation) {
                        options.showAnimationDuration = 0;
                    }

                    // Pass data to PhotoSwipe and initialize it
                    gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
                    gallery.init();
                    var zoomed = false;
                    var zoomIn = function() {
                        gallery.zoomTo(2, {
                            x: gallery.viewportSize.x/2,
                            y: gallery.viewportSize.y/2
                        }, 1000, false, function(now) {
                            zoomed = true;
                        });
                    };
                    var zoomOut = function() {
                        gallery.zoomTo(gallery.currItem.fitRatio, {
                            x: gallery.viewportSize.x,
                            y: gallery.viewportSize.y
                        }, 1000, false, function(now) {
                            zoomed = false;
                        });
                    };
                    zoomIn();
                    $('body').on('click', '.pswp__button--zoom', function(e) {
                        e.stopPropagation();
                        if (zoomed) {
                            zoomOut();
                        } else {
                            zoomIn();
                        }
                    });
                };

                // loop through all gallery elements and bind events
                var galleryElements = document.querySelectorAll( gallerySelector );

                for(var i = 0, l = galleryElements.length; i < l; i++) {
                    galleryElements[i].setAttribute('data-pswp-uid', i+1);
                    galleryElements[i].onclick = onThumbnailsClick;
                }

                // Parse URL and open gallery if it contains #&pid=3&gid=1
                var hashData = photoswipeParseHash();
                if(hashData.pid && hashData.gid) {
                    openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
                }
            };


    	    $(window).on('load', function() {

                $(target).find('.figure').each(function() {
                    var img = $(this).find('img').get(0),
                        imgWidth = parseInt(img.naturalWidth).toString(),
                        imgHeight = parseInt(img.naturalHeight).toString(),
                        size = imgWidth + 'x' + imgHeight;
                    $(this).find('a').attr('data-size', size);
                });
                initPhotoSwipeFromDOM(target);

            });

		},
        iLightbox: function(target) {
            if ($(target).length) {
                $(target).each(function() {
					$(this).find('.ilightbox').iLightBox({
						skin: 'metro-black',
						overlay: { opacity: 0.5 },
						controls: {
							arrows: true,
							slideshow: true,
							fullscreen: true,
							thumbnail: false
						},
						thumbnails: {
							normalOpacity: 1,
							activeOpacity: 1
						},
						path: 'horizontal',
						text: {
							close: 'Zamknij',
							next: 'Następne',
							previous: 'Poprzednie',
							enterFullscreen: 'Przejdź do widoku pełnoekranowego',
							exitFullscreen: 'Zamknij widok pełnoekranowy',
							slideShow: 'Pokaz slajdów'
						}
					});
				});
            }
        }
    };

    /* plugin
    ----------------------------------------------*/
    APP.plugin = {
    	customFileInput: function(target) {
    		if ($(target).length) {
    			$(target).customFileInput();
    		}
    	},
    	dialog: function(target, modal) {
    		if ($(target).length) {
			    $(target).on('click', function() {
			    	if ($(modal).length) {
			    		$(modal).dialog('open');
			    	}
			      	if ($(".ui-dialog-titlebar").length) {
			      		$(".ui-dialog-titlebar").hide();
			      	}
			    });    			
    		}
    	},
    	datepicker: function() {
    		$(window).on('load', function() {
    			//$.datepicker.setDefaults($.datepicker.regional['pl']);
	    		if ($('.hasDatepicker').length && $('.error').length) {
	    			$('#ui-datepicker-div').find('.ui-datepicker-title').contents().filter(function() {
				    	return this.nodeType !== 1;
				    }).remove(); 	
	    		}
    		});
    	}
    };

    /* setup
    ----------------------------------------------*/
    APP.setup = function() {
    	this.trigger.dom();
        this.trigger.truncate('.blog_entry .article_content');
        // this.trigger.iLightbox('.blog_entry');
        // this.trigger.chocolat('.chocolat');
        this.trigger.photoswipe('.lightbox');
    	this.trigger.slideMenu('.box_menu');
    	this.trigger.slideFAQ();
    	this.trigger.hideKeyboard('#ContactForm_date');
    	this.trigger.blur('#LoginForm_username, #LoginForm_password');
    	//this.trigger.checkbox('#ContactForm_terms', '#rules');
    	//this.trigger.checkbox('#ContactForm_policy', '#policy');
    	this.trigger.submit('input[type=submit]');
    	//this.trigger.send('.send_button');
    	this.trigger.facebook();
    	this.trigger.mobileMenu('#menu .nav_ul');
    	//this.plugin.customFileInput('.file');
    	//this.plugin.datepicker();
    	this.plugin.dialog('.whereIsTheCode', '#whereIsTheCode');
    	//this.plugin.dialog('.rules a[href="#Regulamin"]', '#Terms');
    	//this.plugin.dialog('.rules a[href="#Polityka prywatności"]', '#Policy-rules');
    };
    APP.setup();

});