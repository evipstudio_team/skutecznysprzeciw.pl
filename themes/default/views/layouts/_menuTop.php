<?php if($this->beginCache('menuBlock_'.$this->page_id, array('dependency'=>array(
        'class'=>'system.caching.dependencies.CDbCacheDependency',
        'sql'=>'SELECT MAX(last_update) FROM `blocks`')))) { ?>
      <?= $this->renderPartial('/shared/_menu',array('block'=>Block::model()->cache(3600,new CDbCacheDependency('SELECT MAX(last_update) FROM `blocks`'))->find(array('condition'=>'`t`.`id`=23','with'=>array('activePages'=>array('select'=>'id,parent_id'),'activePages.url','activePages.parent'=>array('select'=>'id,parent_id'))))))?>
<?php $this->endCache(); } ?>