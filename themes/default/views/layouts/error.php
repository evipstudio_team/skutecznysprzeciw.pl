<!DOCTYPE html>
<html lang="pl">
<head>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title><?= CHtml::encode($this->pageTitle); ?></title>
  	<meta name="description" content="<?= $this->getPageState('description')?>">
  	<link rel="shortcut icon" href="<?= Yii::app()->theme->baseUrl?>/img/favicon.png">
  	<link rel="stylesheet" href="<?= Yii::app()->theme->baseUrl?>/css/style.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Francois+One&amp;subset=latin,latin-ext">
  	<script src="<?= Yii::app()->theme->baseUrl?>/js/helpers.js"></script>
  	<script src="<?= Yii::app()->theme->baseUrl?>/js/script.js"></script>
  	<!--[if lt IE 9]>
    	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->  
</head>

<body>

<div class="wrapper">

<header id="top">
	<div class="kontener">
    <a class="logo_top" href="/"><img src="<?= Yii::app()->theme->baseUrl?>/img/logo_top.png" alt="Wróć do strony głównej skutecznysprzeciw.pl" title="Wróć do strony głównej skutecznysprzeciw.pl"/></a>
    <img class="napis_top" src="<?= Yii::app()->theme->baseUrl ?>/img/napis_top.png" alt=""/>
<!--     <if(Yii::app()->user->isGuest):?>
      <div id="loginForm">
        <$formModel = new LoginForm;?>
        < $this->renderPartial('//show/login',array('model'=>$formModel))?>
      </div>
    <else:?>
      <div id="userInformation">
          <p style="color: white">Zalogowany jako <strong>< Yii::app()->user->name?></strong> </p>
      </div>
      <div class="userAcc">
          <a href="< $this->createUrl('show/causes')?>">moje zgłoszenia</a> 
          <a href="< $this->createUrl('show/clientDataForm')?>">moje dane</a>	
          <a href="< $this->createUrl('show/logout')?>">wyloguj się</a>
      </div>
    <endif?> -->
	</div>
</header>
<!-- top -->

<nav id="menu">
	<div class="kontener">
    <?= $this->renderPartial('//layouts/_menuTop')?>
	</div>
</nav>
<!-- menu -->

<section class="left_slide_menu">
  <a href="https://www.facebook.com/skutecznysprzeciw/" target="_blank" class="box_menu">
    <img title="Znajdź nas na facebook’u" alt="" src="<?= Yii::app()->theme->baseUrl ?>/img/facebook.png">
    <span>Znajdź nas <br>na facebook’u</span>
  </a>
  <div class="fb-like" data-href="https://www.facebook.com/skutecznysprzeciw/" data-colorscheme="light" data-layout="button_count" data-action="like" data-show-faces="false" data-send="false"></div>
</section>

<section class="main">
	<div class="kontener">
  		<div class="center" style="border: none !important;"><!-- CENTER -->
			<?= $content?>
  		</div>
	</div>
</section>

<footer id="footer">
	<div class="footer_inside">
  	<div class="kontener">
      <a class="logo_footer" href="/"><img src="<?= Yii::app()->theme->baseUrl ?>/img/logo_footer.png" alt="skutecznysprzeciw.pl" title="skutecznysprzeciw.pl"/></a>
      <span><?= Yii::app()->params['varables']['FooterAddress']?></span>
  		<div class="menu_footer">
        <?= $this->renderPartial('//layouts/_menuBottom')?>
  		</div>
      <div class="copyright">Copyright Skutecznysprzeciw.pl <?= date('Y')?>. Powered by <a rel="nofollow" href="http://www.evipstudio.pl">Evipstudio.pl</a></div>
  	</div>
	</div>
</footer>
<!-- footer -->

</div>
<!-- wrapper -->

<?= $this->renderPartial('//shared/_loading')?>
<?= $this->renderPartial('//shared/_terms')?>
</body>
</html>