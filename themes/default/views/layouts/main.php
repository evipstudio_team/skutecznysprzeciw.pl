<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <meta name="description" content="<?= $this->getPageState('description')?>">
    <meta name="google-site-verification" content="kbM3i1Jr3hVROF_p2jLVEe8DsVVJy3bYQZcllz-TcSU" />
    <link rel="shortcut icon" href="<?= Yii::app()->theme->baseUrl?>/img/favicon.png">
    <link rel="stylesheet" href="<?= Yii::app()->theme->baseUrl?>/js/photoswipe/photoswipe.css">
    <link rel="stylesheet" href="<?= Yii::app()->theme->baseUrl?>/js/photoswipe/default-skin/default-skin.css">
    <link rel="stylesheet" href="<?= Yii::app()->theme->baseUrl?>/css/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Francois+One&amp;subset=latin,latin-ext">
    <script src="<?= Yii::app()->theme->baseUrl?>/js/jquery-3.3.1.min.js"></script>
    <script src="<?= Yii::app()->theme->baseUrl?>/js/jquery-migrate-1.4.1.min.js"></script>
    <script src="<?= Yii::app()->theme->baseUrl?>/js/helpers.js"></script>
    <script src="<?= Yii::app()->theme->baseUrl?>/js/photoswipe/photoswipe.min.js"></script>
    <script src="<?= Yii::app()->theme->baseUrl?>/js/photoswipe/photoswipe-ui-default.min.js"></script>
    <script src="<?= Yii::app()->theme->baseUrl?>/js/script.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=pl"></script>
</head>

<body>

<div class="wrapper">

<header id="top">
	<div class="kontener">
    <a class="logo_top" href="/"><img src="<?= Yii::app()->theme->baseUrl?>/img/logo_top.png" alt="Wróć do strony głównej skutecznysprzeciw.pl" title="Wróć do strony głównej skutecznysprzeciw.pl"/></a>
    <img class="napis_top" src="<?= Yii::app()->theme->baseUrl ?>/img/napis_top.png" alt=""/>
<!--     <if(Yii::app()->user->isGuest):?>
      <div id="loginForm">
        <$formModel = new LoginForm;?>
        < $this->renderPartial('//show/login',array('model'=>$formModel))?>
      </div>
    <else:?>
      <div id="userInformation">
          <p style="color: white">Zalogowany jako <strong>< Yii::app()->user->name?></strong> </p>
      </div>
      <div class="userAcc">
          <a href="< $this->createUrl('show/causes')?>">moje zgłoszenia</a> 
          <a href="< $this->createUrl('show/clientDataForm')?>">moje dane</a>	
          <a href="< $this->createUrl('show/logout')?>">wyloguj się</a>
      </div>
    <endif?> -->
	</div>
</header>
<!-- top -->

<nav id="menu">
	<div class="kontener">
    <?= $this->renderPartial('//layouts/_menuTop')?>
	</div>
</nav>
<!-- menu -->

<section class="left_slide_menu">
  <a href="https://www.facebook.com/skutecznysprzeciw/" target="_blank" class="box_menu">
    <img title="Znajdź nas na facebook’u" alt="" src="<?= Yii::app()->theme->baseUrl ?>/img/facebook.png">
    <span>Znajdź nas <br>na facebook’u</span>
  </a>
  <div class="fb-like" data-href="https://www.facebook.com/skutecznysprzeciw/" data-colorscheme="light" data-layout="button_count" data-action="like" data-show-faces="false" data-send="false"></div>
</section>

<section class="main">
  <div class="kontener">

  	<div class="left">
      <div id="form">
        <?$formModel = new ContactForm;?>
        <?= $this->renderPartial('//show/ContactForm',array('model'=>$formModel))?>
      </div>
  	</div>
    <!-- left -->

    <?if(Yii::app()->controller->action->id=='causes'):?>
      <div class="center_long">
    <?else:?>
      <div class="center">
    <?endif?>
  	<?= $content?>
  	</div>
    <!-- center -->

    <?if(Yii::app()->controller->action->id!='causes'):?>
    	<div class="right">
        <?$causeStep = getNextStep()?>
        <?$cause = Cause::model()->findByPk(Yii::app()->user->getState('unloggedCauseId'))?>
        <?$causeContactForm = isset($this->causeContactForm)? $this->causeContactForm:new CauseContactForm()?>
        <?if(!Yii::app()->user->isGuest && $causeStep>2):?>
          <div class="contact">
            <h3>Formularz kontaktowy</h3>
            <div class="separator"></div>
            <p>W każdej chwili mogą się Państwo z&nbsp;Nami skontaktować wypełniając formularz znajdujacy się poniżej.</p>
            <div class="cause_form">
              <div class="inner">
                <? $form = $this->beginWidget('CActiveForm', array('htmlOptions' => array('enctype' => 'multipart/form-data'))) ?>
                <div class="row">
                  <?php echo $form->labelEx($causeContactForm, 'file'); ?><br />
                  <?= $form->fileField($causeContactForm, 'file', array('class' => "file", 'size' => 10)) ?>
                  <?= $form->error($causeContactForm, 'file'); ?>
                </div>
                <div class="row">
                  <?php echo $form->labelEx($causeContactForm, 'comment'); ?><br />
                  <?= $form->textArea($causeContactForm, 'comment', array('cols' => '28', 'rows' => '5')) ?>
                  <?= $form->error($causeContactForm, 'comment'); ?>
                </div>
                <input type="submit" value="Wyślij" class="submit_step2"/>
                <?php $this->endWidget(); ?>
              </div>
            </div>
            <?if(Yii::app()->controller->action->id!='causes'):?>
              <?$causeStep = getNextStep()?>
              <?$cause = Cause::model()->findByPk(Yii::app()->user->getState('unloggedCauseId'))?>
              <?$causeContactForm = isset($this->causeContactForm)? $this->causeContactForm:new CauseContactForm()?>
  		        <?if(!Yii::app()->user->isGuest && $causeStep>2):?>
                <?if($cause->filesCount):?>
                  <div class="cause_files">
                    <h3>Pliki dołączone do zgłoszenia</h3>
                    <div class="link">
                    <?foreach($cause->files(array('order'=>'created_at')) as $file):?>
                      <a href="<?= $this->createUrl('show/getFile',array('hash1'=>$file->createLinkHash1(),'hash2'=>$file->createLinkHash2()))?>"><?= $file->filename?></a>
  			            <?endforeach?>
                    </div>
                  </div>
                <?endif?>
  		        <?endif?>
            <?endif?>
            <div class="clear"></div>
          </div>
        <?else:?>
          <?= $this->renderPartial('//shared/_step_block',array('title'=>'Krok 1','content'=>'Wypełnij formularz kontaktowy z lewej strony ekranu.','causeStep'=>$causeStep,'step'=>1, 'url'=>'#Krok-1', 'onclick'=>'blinkStep1Form()'))?>
          <?= $this->renderPartial('//shared/_step_block',array('title'=>'Krok 2','content'=>'Prześlij do Nas<br />kod dostępu do akt lub skany dokumentów.','causeStep'=>$causeStep,'step'=>2,'url'=>$this->createUrl('show/step2',array('causeId'=>Yii::app()->user->getState('unloggedCauseId')))))?>
          <?= $this->renderPartial('//shared/_step_block',array('title'=>'Krok 3','content'=>'W ciągu 24 godzin dokonamy bezpłatnej analizy Twojej sprawy.','causeStep'=>$causeStep,'step'=>3))?>
          <?= $this->renderPartial('//shared/_step_block',array('title'=>'Krok 4','content'=>'Po dokonaniu analizy skontaktujemy się z Tobą.','causeStep'=>$causeStep,'step'=>4))?>
          <?= $this->renderPartial('//shared/_popular')?>
        <?endif?>
    	</div>
      <!-- right -->
    <?endif?>
  </div>
</section>
<!-- main -->

<footer id="footer">
	<div class="footer_inside">
  	<div class="kontener">
      <a class="logo_footer" href="/"><img src="<?= Yii::app()->theme->baseUrl ?>/img/logo_footer.png" alt="skutecznysprzeciw.pl" title="skutecznysprzeciw.pl"/></a>
      <span><?= Yii::app()->params['varables']['FooterAddress']?></span>
  		<div class="menu_footer">
        <?= $this->renderPartial('//layouts/_menuBottom')?>
  		</div>
      <div class="copyright">Copyright Skutecznysprzeciw.pl <?= date('Y')?>. Powered by <a rel="nofollow" href="http://www.evipstudio.pl">Evipstudio.pl</a></div>
  	</div>
	</div>
</footer>
<!-- footer -->

</div>
<!-- wrapper -->

<div class="shares">
    <?= $this->renderPartial('//shared/_loading')?>
    <?= $this->renderPartial('//shared/_terms')?>
    <?= $this->renderPartial('//shared/_policy')?>
    <?= $this->renderPartial('//shared/_flash')?>
</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36854165-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>