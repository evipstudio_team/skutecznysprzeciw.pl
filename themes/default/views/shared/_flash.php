<div style="display: none">
<? $flashMessages = Yii::app()->user->getFlashes(); ?>
<? if ($flashMessages): ?>
  <?
  $this->beginWidget('zii.widgets.jui.CJuiDialog',
          array('id'=>'FlashDialog','options' => array(
          'modal' => true,
          'autoOpen'=>false,
          'width'=>'600',
          'closeOnEscape'=> false,
              'close'=>'js: function() {$(\'#FlashDialog\').remove()}'
          //'open'=>'function() {var buttons = (\'#FlashDialog\').dialog(\'option\',\'buttons\'); console.debug(buttons)}',
//          'buttons' => array('OK' => 'js:function(){$(this).dialog("close")}')
          ))
  );
  ?>
  <ul class="flashes" style="list-style: none; margin-top: 20px">
    <? foreach ($flashMessages as $key => $message): ?>
      <li><div class="flash-<?= $key ?>"><?= $message ?></div></li>
  <? endforeach ?>
  </ul>
  <? $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>
<?endif?>
</div>