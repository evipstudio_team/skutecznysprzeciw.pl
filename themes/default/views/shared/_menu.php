<ul class="nav_ul">
  <?foreach($block->activePages as $page):?>
      <?
        $calculatedAddress = strtolower($page->activeUrl);
      ?>
      <?if((isset($this->page_id) && ($page->id==$this->page_id || $page->parent_id==$this->page_id))):?>
        <li class="nav_li"><a href="<?= $calculatedAddress?>" class="active"<?if($page->url->blank):?> target="_blank"<?endif?>><?= $page->url->anchor?></a></li>
      <?else:?>
        <li class="nav_li"><a href="<?= $calculatedAddress?>"<?if($page->url->blank):?> target="_blank"<?endif?>><?= $page->url->anchor?></a></li>
      <?endif?>
  <?endforeach?>
</ul>