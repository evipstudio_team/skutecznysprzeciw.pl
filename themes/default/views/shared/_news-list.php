<div class="content_box">
    <div class="left"><!-- LEFT -->
    </div><!-- LEFT end -->

    <div class="blog_content">
        <h1>Aktualności</h1>
        <div class="content_line"></div>
        <div id="article-list" class="list-view">
            <div class="summary">1-4 z 6</div>
            <div class="items">
                <div class="blog_entry">
                    <div class="article_title">
                        <h2><a href="/aktualnosci/03-07-2018-r-best-nakaz-zaplaty-z-sadu-rejonowego-w-elblagu-dlaczego-akurat-stamtad" rel="#03-07-2018-r-BEST-nakaz-zaplaty-z-Sadu-Rejonowego-w-Elblagu-Dlaczego-akurat-stamtad">03.07.2018 r. - BEST nakaz zapłaty z Sądu Rejonowego w Elblągu? Dlaczego akurat stamtąd?</a></h2>
                    </div>
                    <div class="article_content">
                        Sąd Rejonowy w Elblągu wydał w 2017 roku ponad 100 tys. nakazów zapłaty. Znaczna ich część została wystawiona na wniosek firmy windykacyjnej BEST, która masowo kieruje sprawy do tego właśnie sądu... Praktycznie każdy z nich warto zaskarżyć.
                    </div>
                    <div class="show_more">
                        <a href="/aktualnosci/03-07-2018-r-best-nakaz-zaplaty-z-sadu-rejonowego-w-elblagu-dlaczego-akurat-stamtad">czytaj więcej</a>
                    </div>
                </div><div class="blog_entry">
                    <div class="article_title">
                        <h2><a href="/aktualnosci/27-05-2018-r-profi-credit-polska-s-a-jak-sie-bronic-w-postepowaniu-sadowym" rel="#27-05-2018-r-Profi-Credit-Polska-S-A-jak-sie-bronic-w-postepowaniu-sadowym">27.05.2018 r. - Profi Credit Polska S.A. – jak się bronić w postępowaniu sądowym?</a></h2>
                    </div>
                    <div class="article_content">
                        Profi Credit Polska S.A. to kolejny podmiot, który kieruje pozwy przeciwko naszym klientom. Warto pochylić się nad jego strategią działania, żeby uświadomić sobie, czy warto się bronić przed ich żądaniami, a jeżeli tak, to dlaczego oraz jakie kroki podjąć.
                    </div>
                    <div class="show_more">
                        <a href="/aktualnosci/27-05-2018-r-profi-credit-polska-s-a-jak-sie-bronic-w-postepowaniu-sadowym">czytaj więcej</a>
                    </div>
                </div><div class="blog_entry">
                    <div class="article_title">
                        <h2><a href="/aktualnosci/23-04-2018-r-kruk-nakaz-zaplaty-czy-warto-sie-bronic" rel="#23-04-2018-r-Kruk-nakaz-zaplaty-czy-warto-sie-bronic">23.04.2018 r. - Kruk nakaz zapłaty – czy warto się bronić?</a></h2>
                    </div>
                    <div class="article_content">
                        Firma windykacyjna Kruk prowadzi wiele akcji marketingowych
                        zachęcających dłużników do podpisywania ugód, które zgodnie z zapewnieniami windykatora mają polepszyć ich sytuację i umożliwić wyjście z problemu zadłużenia. Jednak w rzeczywistości podpisanie ugody... polepsza jedynie sytuację Kruka w...
                    </div>
                    <div class="show_more">
                        <a href="/aktualnosci/23-04-2018-r-kruk-nakaz-zaplaty-czy-warto-sie-bronic">czytaj więcej</a>
                    </div>
                </div><div class="blog_entry">
                    <div class="article_title">
                        <h2><a href="/aktualnosci/27-03-2018-r-hoist-komornik-i-co-teraz" rel="#27-03-2018-r-Hoist-komornik-i-co-teraz">27.03.2018 r. - Hoist komornik – i co teraz?</a></h2>
                    </div>
                    <div class="article_content">
                        Warto zapoznać się z treścią niniejszego wpisu, jeżeli dowiedzieli się Państwo o całej sprawie dopiero poprzez zajęcie egzekucyjne dokonane na wniosek firmy Hoist.
                    </div>
                    <div class="show_more">
                        <a href="/aktualnosci/27-03-2018-r-hoist-komornik-i-co-teraz">czytaj więcej</a>
                    </div>
                </div></div>
            <div class="pager"><ul id="yw0" class="yiiPager"><li class="first hidden"><a href="/show/homeByUrl?address=aktualnosci">&lt;&lt; Pierwsza</a></li>
                    <li class="previous hidden"><a href="/show/homeByUrl?address=aktualnosci">Poprzednia</a></li>
                    <li class="page selected"><a href="/show/homeByUrl?address=aktualnosci">1</a></li>
                    <li class="page"><a href="/show/homeByUrl?address=aktualnosci&amp;Page_page=2">2</a></li>
                    <li class="next"><a href="/show/homeByUrl?address=aktualnosci&amp;Page_page=2">Następna</a></li>
                    <li class="last"><a href="/show/homeByUrl?address=aktualnosci&amp;Page_page=2">Ostatnia &gt;&gt;</a></li></ul></div><div class="keys" style="display:none" title="/aktualnosci"><span>135</span><span>134</span><span>133</span><span>132</span></div>
        </div>    <script type="text/javascript">
            $(function() {
                if(window.location.hash) {
                    $('.article_title a').each(function() {
                        if(window.location.hash == $(this).attr('rel')) {
                            $('html, body').animate({
                                scrollTop: $(this).offset().top
                            }, 1000);
                            return false;
                        }
                    });
                }
            });

        </script>
    </div>
</div>