<div class="steps">
  <?if(isset($causeStep) && $causeStep && isset($step) && $causeStep==$step):?>
    <?$class = 'actual';?>
  <?elseif(isset($causeStep) && $causeStep && isset($step) && $causeStep>$step):?>
    <?$class = 'done';?>
  <?endif?>
  <div class="step_nr<?=((isset($class)? ' '.$class:''))?>">
    <?if(isset($class) && $class=='done'):?>
      <img src="<?= Yii::app()->theme->baseUrl?>/img/checkbox_checked.png" alt="" title="" style="width: 14px;" />
    <?else:?>
    <img src="<?= Yii::app()->theme->baseUrl?>/img/checkbox_unchecked.png" alt="" title="" style="width: 14px" />
    <?endif?>
    <?if(isset($url) && isset($class) && $class=='actual'):?>
      <a href="<?= $url?>"<?if(isset($onclick)):?> onclick="<?=$onclick?>"<?endif?>><?= $title?></a>
    <?else:?>
      <?= $title?>
    <?endif?>
  </div>
  <div class="step_desc">
    <?= $content?>
  </div>
</div>

