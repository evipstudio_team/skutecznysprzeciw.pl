<?if(Yii::app()->user->isGuest || (!Yii::app()->user->isGuest &&  !in_array(Yii::app()->controller->action->id,array('causes','cause')))):?>
  <?$step = getNextStep()?>
  <?if($step==2):?>
      <div id="clearCacheDiv">
      	<div class="info-1">
	        <h2><strong>Jesteś w trakcie realizacji kroku 2</strong></h2>
	        <p class="separator">Dopiero po jego zakończeniu będziesz mógł zgłosić kolejną sprawę.</p>
	     </div>
        <div class="info-2">&nbsp;</div>
      </div>
  <?elseif($step>2):?>
      <div id="clearCacheDiv">
      	<div class="info-1">
	        <h2><strong>Twoja sprawa jest właśnie analizowana.</strong></h2>
	        <p class="separator">Skontaktujemy się z Tobą w ciągu 24 h.<br /><br />Jeżeli chcesz zgłosić kolejną sprawę kliknij <a href="<?= $this->createUrl('show/clearCache')?>">TUTAJ</a>.</p>
       </div>
        <div class="info-2">&nbsp;</div>
      </div>
  <?endif?>
  <?php $form=$this->beginWidget('CActiveForm',array('id'=>'formularz', 'action'=>$this->createUrl('show/contactForm'))); ?>
    <div class="form_header">Krok 1</div>
    <div class="deco_formularz">
      <div class="formularz">
          <div class="form-box-1">
              <?= $form->textField($model,'name', array('onfocus'=>  defaultOnfocus($model->defaultValue('name')), 'onblur'=>  defaultOnBlur($model->defaultValue('name')))); ?>
              <div class="errorMessage"></div>
              <?=$form->error($model,'name'); ?>
              <?= $form->textField($model,'email', array('onfocus'=>  defaultOnfocus($model->defaultValue('email')), 'onblur'=>  defaultOnBlur($model->defaultValue('email')))); ?>
              <div class="errorMessage"></div>
              <?=$form->error($model,'email'); ?>
              <?= $form->textField($model,'phone', array('onfocus'=>  defaultOnfocus($model->defaultValue('phone')), 'onblur'=>  defaultOnBlur($model->defaultValue('phone')))); ?>
              <div class="errorMessage"></div>
              <?=$form->error($model,'phone'); ?>
              <?Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');?>
              <?$this->widget('CJuiDateTimePicker', array(
                  'attribute'=>'date',
                  'mode'=>'date',
                  'language'=>Yii::app()->language,
                  'model'=>$model,
                  'options'=>array(
                    'dateFormat'=>'yy-mm-dd',
                    'maxDate'=>'new Date(year,month,day)',
                    'prevText'=> 'Wstecz',
                    'nextText'=> 'Dalej',
                    'currentText'=> 'Dzisiaj',
                    'monthNames'=>["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
                    'monthNamesShort'=>["Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "Paź", "Lis", "Gru"],
                    'dayNames'=> ['Poniedziałek','Wtorek','Środa','Czwartek','Piątek','Sobota','Niedziela'],
                    'dayNamesShort'=> ['Pon','Wto','Śro','Czw','Pią','Sob','Nie'],
                    'dayNamesMin'=> ['Pn','Wt','Śr','Cz','Pt','So','N'],
                    'yearSuffix'=> ''
                  ),
                  'htmlOptions'=>array('style'=>'cursor: pointer', 'class'=>'datePickIcon')
                  ));?>
              <div class="errorMessage"></div>
              <?=$form->error($model,'date'); ?>
              <?= $form->textArea($model,'body',array('rows'=>5, 'cols'=>20,'onfocus'=>  defaultOnfocus($model->defaultValue('body')), 'onblur'=>  defaultOnBlur($model->defaultValue('body')))); ?>
              <div class="errorMessage"></div>
              <?=$form->error($model,'body'); ?>
          </div>
          <div class="form-box-2">
              <div id="recaptcha">

              </div>
              <script type="text/javascript">
                  var recaptcha = {
                      id: '#recaptcha',
                      sitekey: '6LedXF0UAAAAAGAjufR00ygWgYbC3zIt8MujOtka',
                      redesign: function() {
                          this.context = $(this.id);
                          this.iframe = this.context.find('iframe');
                          this.iframe.css({
                              '-webkit-transform': 'scale(1.15)',
                              '-moz-transform': 'scale(1.15)',
                              '-ms-transform': 'scale(1.15)',
                              '-o-transform': 'scale(1.15)',
                              'transform': 'scale(1.15)',
                              '-webkit-transform-origin': '0 0',
                              '-moz-transform-origin': '0 0',
                              '-ms-transform-origin': '0 0',
                              '-o-transform-origin': '0 0',
                              'transform-origin': '0 0'
                          });
                          this.context.children().height(this.iframe.height() + 30);
                      },
                      create: function() {
                          var context = this;
                          grecaptcha.render('recaptcha', {
                              'sitekey': context.sitekey,
                              'size': 'compact',
                              'expired-callback': context.redesign,
                              'error-callback': context.redesign
                          });
                      },
                      setup: function() {
                          var context = this;
                          $(window).on('load', function() {
                              context.create();
                              context.redesign();
                          });
                      }
                  };
                  recaptcha.setup();
              </script>
              <!--          --><?//= $this->renderPartial('//shared/_recaptcha')?>
          </div>

          <div class="form-box-3">
              <?= $form->hiddenField($model,'terms')?>
              <span class="checkbox" id="rules" style="background-position: 0px <?if($model->terms):?>-5<?endif?>0px;" onclick="if($('#ContactForm_terms').val()){$('#ContactForm_terms').val(''); $(this).css('background-position','0px 0px')}else{$('#ContactForm_terms').val('1'); $(this).css('background-position','0px -50px')}"></span>
              <span class="rules">Akceptuję <a href="#Regulamin" onclick="$(this).off().closest('body').find('#Terms').dialog('open')">Regulamin</a> strony.</span>
              <div class="errorMessage"></div>
              <?=$form->error($model,'terms'); ?>
              <?= $form->hiddenField($model,'policy')?>
              <span class="checkbox" id="policy" style="background-position: 0px <?if($model->policy):?>-5<?endif?>0px;" onclick="if($('#ContactForm_policy').val()){$('#ContactForm_policy').val(''); $(this).css('background-position','0px 0px')}else{$('#ContactForm_policy').val('1'); $(this).css('background-position','0px -50px')}"></span>
              <span class="rules">Akceptuję <a href="#Polityka prywatności" onclick="$(this).off().closest('body').find('#Policy-rules').dialog('open')">Politykę prywatności</a>.</span>
              <div class="errorMessage"></div>
              <?=$form->error($model,'policy'); ?>
          </div>
      </div>
    </div>
    <input type="button" name="send" value="<?= Yii::t('cms', 'Wyślij') ?>" class="send_button" />
    <script>
        var validator = {
            form: {
                content: '#content',
                placeholder: '.form-placeholder',
                id: '#formularz',
                box: '#form',
                box1: '.form-box-1',
                box2: '.form-box-2',
                box3: '.form-box-3',
                messages: '.errorMessage',
                cache: '#clearCacheDiv'
            },
            modal: {
                loading: '#LoadingDialogBox',
                flash: '#FlashDialog'
            },
            name: {
                pattern: /^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]{1}[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ\s]*$/,
                input: 'ContactForm_name',
                placeholder: 'Imię i nazwisko',
                message: 'Pole Nazwa nie może być puste.',
                message2: 'Pole Nazwa musi składać się z minimum pierwszej 1 litery.'
            },
            email: {
                pattern: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
                input: 'ContactForm_email',
                placeholder: 'Adres e-mail',
                message: 'Pole Adres e-mail nie może być puste.',
                message2: 'Zawartość pola Adres e-mail nie jest poprawnym adresem e-mail.'
            },
            phone: {
                pattern: /^\d{9,}$/,
                input: 'ContactForm_phone',
                placeholder: 'Numer telefonu',
                message: 'Pole Numer telefonu nie może być puste.',
                message2: 'Pole Numer telefonu musi składać się z minimum 9 cyfr.'
            },
            date: {
                pattern: false,
                input: 'ContactForm_date',
                placeholder: 'Data otrzymania nakazu',
                message: 'Pole Data otrzymania nakazu nie może być puste.'
            },
            body: {
                input: 'ContactForm_body',
                placeholder: 'Dodatkowa wiadomość'
            },
            terms: {
                pattern: false,
                input: 'ContactForm_terms',
                message: 'Musisz zaakceptować regulamin.'
            },
            policy: {
                pattern: false,
                input: 'ContactForm_policy',
                message: 'Musisz zaakceptować politykę prywatności.'
            },
            hideMessages: function() {
                $(this.form.messages).hide();
            },
            showModal: function() {
                $(this.modal.loading).dialog('open');
            },
            hideModal: function() {
                $(this.modal.loading).dialog('close');
            },
            saveAction: function() {
                this.form.action = $(this.form.id).attr('action');
            },
            saveData: function() {
                this.form.data = $(this.form.id).serializeArray();
            },
            reloadDatepicker: function() {
                $('#' + this.date.input).datepicker('destroy').datepicker($.extend({
                    showMonthAfterYear: false
                }, jQuery.datepicker.regional['pl'], {
                    'dateFormat': 'yy-mm-dd',
                    'maxDate':'new Date(year,month,day)'
                }));
            },
            reload: function() {
                this.hideMessages();
                this.createPlaceholders();
                this.reloadDatepicker();
                this.focus();
                this.keypress();
                this.click();
            },
            redesign: function(data) {
                this.form.html = $(data).not(this.form.id);
                $(this.form.id).siblings().remove();
                $(this.form.box).find(this.form.box1).replaceWith($(data).find(this.form.box1));
                $(this.form.box).find(this.form.box3).replaceWith($(data).find(this.form.box3));
                $(this.form.box).append(this.form.html);
            },
            submit: function() {
                var context = this;
                this.saveAction();
                this.saveData();
                this.showModal();
                $.ajax({
                    'url': context.form.action,
                    'type': 'post',
                    'data': context.form.data,
                    'success': function(data) {
                        context.hideModal();
                        context.redesign(data);
                        context.reload();
                    }
                });
            },
            saveRecaptchaResponse: function() {
                this.recaptcha = {};
                this.recaptcha.response = grecaptcha.getResponse();
            },
            validate: function() {
                var context = this;
                this.status = false;
                $(context.form.id + ' input').each(function() {
                    context.id = $(this).attr('id');
                    context.value = $(this).val();
                    if (context.id === context.name.input) {
                        if (context.value === '' || context.value === context.name.placeholder) {
                            context.status = true;
                            $(this).next().empty().show().text(context.name.message);
                        } else {
                            if (!context.name.pattern.test(context.value)) {
                                context.status = true;
                                $(this).next().empty().show().text(context.name.message2);
                            } else {
                                $(this).next().empty().hide();
                            }
                        }
                    }
                    if (context.id === context.email.input) {
                        if (context.value === '' || context.value === context.email.placeholder) {
                            context.status = true;
                            $(this).next().empty().show().text(context.email.message);
                        } else {
                            if (!context.email.pattern.test(context.value)) {
                                context.status = true;
                                $(this).next().empty().show().text(context.email.message2);
                            } else {
                                $(this).next().empty().hide();
                            }
                        }
                    }
                    if (context.id === context.phone.input) {
                        if (context.value === '' || context.value === context.phone.placeholder) {
                            context.status = true;
                            $(this).next().empty().show().text(context.phone.message);
                        } else {
                            if (!context.phone.pattern.test(context.value)) {
                                context.status = true;
                                $(this).next().empty().show().text(context.phone.message2);
                            } else {
                                $(this).next().empty().hide();
                            }
                        }
                    }
                    if (context.id === context.date.input) {
                        if (context.value === '' || context.value === context.date.placeholder) {
                            context.status = true;
                            $(this).next().empty().show().text(context.date.message);
                        } else {
                            $(this).next().empty().hide();
                        }
                    }
                    if (context.id === context.terms.input) {
                        if (context.value === '') {
                            context.status = true;
                            $(this).siblings(context.form.messages).first().empty().show().text(context.terms.message);
                        } else {
                            $(this).siblings(context.form.messages).first().empty().hide();
                        }
                    }
                    if (context.id === context.policy.input) {
                        if (context.value === '') {
                            context.status = true;
                            $(this).siblings(context.form.messages).last().empty().show().text(context.policy.message);
                        } else {
                            $(this).siblings(context.form.messages).last().empty().hide();
                        }
                    }
                });
                if (!this.status) {
                    if (this.recaptcha.response.length === 0) {
                        alert('Uzupełnij captche');
                    } else {
                        this.submit();
                    }
                }
            },
            createPlaceholders: function() {
                $(this.form.id).find(this.form.box1).children().not(this.form.messages).each(function() {
                    var placeholder = $(this).val();
                    $(this).removeAttr('value onfocus onblur').attr('placeholder', placeholder);
                });
            },
            focus: function() {
                $(this.form.id).find(this.form.box1).children().not(this.form.messages).on('focus', function(e) {
                    var input = $(this);
                    if (input.hasClass('hasDatepicker')) {
                        document.activeElement.blur();
                        input.blur();
                    }
                });
            },
            keypress: function() {
                $(this.form.id).find(this.form.box1).children().not(this.form.messages).on('keypress', function(e) {
                    var input = $(this);
                    var keycode = (e.keyCode ? e.keyCode : e.which);
                    if (parseInt(keycode) === 13) {
                        document.activeElement.blur();
                        input.blur();
                    }
                });
            },
            click: function() {
                var context = this;
                $(this.form.id + ' input[name="send"]').on('click', function() {
                    context.saveRecaptchaResponse();
                    context.validate();
                });
            },
            replace: function() {
                var context = this;
                this.form.widget = $(this.form.content + ' .form_box');
                this.form.clone = this.form.widget.clone(true, true);
                if (window.innerWidth < 768) {
                    this.form.widget.remove();
                    $(this.form.placeholder).append(context.form.clone);
                }
//                    else {
//                        $(this.form.content).append(context.form.clone);
//                    }
            },
            setup: function() {
                var context = this;
                $(document).on('ready', function() {
                    context.replace();
                });
                $(window).on('load', function() {
                    context.reload();
                });
            }
        };
        validator.setup();
    </script>
    </script>
  <?php $this->endWidget(); ?>
  <?if(!isset($withoutFlash)):?>
    <?= $this->renderPartial('//shared/_flash')?>
  <?endif?>

  <?if($step>1):?>
    <?Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl.'/js/grayscale.js');?>
    <script type="text/javascript">
          $(function() {
            $("#formularz :input").attr("disabled", true);
            $("#formularz :input").css("cursor", 'default');
            $('#rules').attr('onclick','');
            grayscale( $('#form') );
            grayscale.reset($('#clearCacheDiv'));
            $('#ContactForm_date').removeClass('datePickIcon');
            $('#ContactForm_date').addClass('datePickIcon2');
            $('#form').height($('#form').height());
            var htmlContent = $('#formularz').html();
            $('#form').hover(
              function() {
                if($('#formularz').html()!=$('#clearCacheDiv').html()) {
                  $('#formularz').animate({opacity: '0'},{queue:false,duration:'fast',complete: function() {
                      $('#formularz').html($('#clearCacheDiv').html());
                      $('#formularz').css('opacity','1');
                      //$('#formularz').animate({opacity: "1"},{queue:false,duration:'fast'});
                    }
                  });
                }
              },
              function() {
                if($('#formularz').html()!=htmlContent) {
                  $('#formularz').animate({opacity: "0"},{queue:false,duration:'fast',complete:function() {
                      $('#formularz').html(htmlContent);
                      $('#formularz').css('opacity','1');
                      //$('#formularz').animate({opacity: "1"},{queue:false,duration:'fast'});

                  }});
                }
              }
            );
          });
        </script>
  <? endif; ?>
  <?if(!Yii::app()->user->isGuest):?>
        <?$user = User::model()->findByPk(Yii::app()->user->id)?>
    <script type="text/javascript">
      $(function() {
        $('#ContactForm_name').val('<?=$user->name?>');
        $('#ContactForm_email').val('<?=$user->email?>');
        $('#ContactForm_phone').val('<?=$user->phone?>');
      });
      </script>
  <? endif; ?>
<?else:?>
  <div class="steps">
    <div class="step_nr">Dodaj nową sprawę</div>
    <div class="step_desc">Aby dodać nową sprawę <a href="<?= $this->createUrl('show/clearCache')?>">kliknij tutaj</a>.</div>
  </div>
<? endif; ?>
