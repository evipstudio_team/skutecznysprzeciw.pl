
<h1><?= $page->article->title?></h1>

<?if($page->article->short_content):?><h2><?= $page->article->short_content?></h2><?endif?>

<div style="height:20px"></div>
<?= $page->article->content?>
<?if($page->parent_id==103):?>
<div class="goBack">
  <?$urls = $this->getPageState('urlHistory')?>
  <?$url = isset($urls[1])? $urls[1]:false?>
  <?if($url):?>
    <a href="<?= $url?>#<?=$page->article->url->address?>">Wróć</a>
  <?endif?>
</div>
<?endif?>

<?= $this->renderPartial('multimedia',array('files'=>$page->files,'insideDiv'=>'insideDiv','imageWidth'=>200))?>