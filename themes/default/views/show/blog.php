<div class="content_box">
    <div class="left"><!-- LEFT -->
      <?= $mainPage->article->content ?>
    </div><!-- LEFT end -->

    <div class="blog_content">
      <?if($page->id==103):?>
        <h1><?= $page->article->title?></h1>
        <?
          $this->widget('zii.widgets.CListView', array(
              'id'=>'article-list',
              'dataProvider'=>$searchPage->searchArticles(1,5, 'id DESC'),
              'itemView'=>'//shared/_blogArticle',
          ));
        ?>
        <script type="text/javascript">
          $(function() {
            if(window.location.hash) {
              $('.article_title a').each(function() {
                if(window.location.hash == $(this).attr('rel')) {
                  $('html, body').animate({
                    scrollTop: $(this).offset().top
                  }, 1000);
                  return false;
                }
              });
            }
          });

        </script>
      <?else:?>
        <?= $this->renderPartial('//show/article',array('page'=>$page))?>
      <?endif?>
    </div>
</div>