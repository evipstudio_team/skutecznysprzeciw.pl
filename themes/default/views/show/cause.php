<h3>Karta sprawy z dnia <?= date('d/m/Y',strtotime($cause->created_at))?></h3>
<div class="separator"></div>
<p>Poniżej przedstawione zostały wszystkie operacje związane z Państwa zgłoszeniem.<br /></p>
<ol id="timeline">
  <li style="border: 0; text-align: center;">
    <div class="time" style="border: 0; padding-top: 2px;">Akcje wykonane po stronie klienta</div>
  </li>
  <li style="border: 0; text-align: center">
    <div class="time" style="border: 0; padding-top: 2px;">Akcje wykonane po stronie zespołu SkutecznySprzeciw.pl</div>
  </li>
  <?foreach($timeLine as $date=>$byDate):?>
    <?foreach($byDate as $user=>$byUser):?>
      <?foreach($byUser as $entry):?>
        <?if($entry['user']->role != 'Client'):?>
          <li style="border: 0">
            <div class="time" style="border: 0; padding-top: 2px;">&nbsp;</div>
            <p>&nbsp;</p>
          </li>
        <?endif?>
        <li>
          <div class="time"><?= date('d/m/Y H:i',strtotime($entry['created_at']))?> - <i><?= $entry['user']->fullName?></i></div>
          <span class="corner"></span>
          <p><?= $entry['comment']?></p>

        </li>
        <?if($entry['user']->role == 'Client'):?>
          <li style="border: 0">
            <div class="time" style="border: 0; padding-top: 2px;">&nbsp;</div>
            <p>&nbsp;</p>
          </li>
        <?endif?>
      <?endforeach?>
    <?endforeach?>
  <?endforeach?>

</ol>
