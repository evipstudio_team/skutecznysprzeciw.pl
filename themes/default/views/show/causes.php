<h3>Moje zgłoszenia</h3>
<p>Poniżej widnieje lista Twoich zgłoszeń, kliknij "pokaż szczegóły" aby zobaczyć pełną historię wybranego elementu.</p>
<?foreach($causes as $cause):?>
<div class="causes">
  <div class="add_date">
    <span>Data zgłoszenia: <?= date('d/m/Y',strtotime($cause->created_at))?></span>
  </div>
  <div class="last_update">
    <span class="data-title"><strong>Ostatnia aktualizacja:</strong></span><br/>
    <span class="data-content">
    	<?= date('d/m/Y H:i',strtotime($cause->lastComment->created_at))?> - <?= $cause->lastComment->author->fullName?> - <?= $cause->lastComment->comment?>
    </span>
  </div>
  <div class="show_details">
    <?if($cause->actual_step==2):?>
      <a href="<?= $this->createUrl('show/step2',array('causeId'=>$cause->id))?>">Przejdź do kroku 2</a>
    <?else:?>
      <a href="<?= $this->createUrl('show/cause',array('causeId'=>$cause->id))?>">Pokaż szczegóły</a>
    <?endif?>
  </div>
</div>
<? endforeach; ?>
