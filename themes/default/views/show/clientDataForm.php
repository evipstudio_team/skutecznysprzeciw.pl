<h3>Twoje dane</h3>
<p>
  Poniżej wypisane zostały Twoje dane. W każdej chwili możesz dokonać zmian danych wypełniając poniższy formularz.
</p>
<div class="userForm">
  <?php $form=$this->beginWidget('CActiveForm', array('id'=>'user-form')); ?>
    <div class="row">
      <?php echo $form->labelEx($clientDataForm,'name'); ?>
      <?php echo $form->textField($clientDataForm,'name',array('size'=>60,'maxlength'=>128)); ?>
      <?php echo $form->error($clientDataForm,'name'); ?>
    </div>

    <div class="row">
      <?php echo $form->labelEx($clientDataForm,'surname'); ?>
      <?php echo $form->textField($clientDataForm,'surname',array('size'=>60,'maxlength'=>128)); ?>
      <?php echo $form->error($clientDataForm,'surname'); ?>
    </div>

    <div class="row">
      <?php echo $form->labelEx($clientDataForm,'email'); ?>
      <?php echo $form->textField($clientDataForm,'email',array('size'=>60,'maxlength'=>128)); ?>
      <?php echo $form->error($clientDataForm,'email'); ?>
    </div>
    <div class="row">
      <?php echo $form->labelEx($clientDataForm,'phone'); ?>
      <?php echo $form->textField($clientDataForm,'phone',array('size'=>60,'maxlength'=>128)); ?>
      <?php echo $form->error($clientDataForm,'phone'); ?>
    </div>
    <div class="row">
      <?php echo $form->labelEx($clientDataForm,'password'); ?>
      <?php echo $form->passwordField($clientDataForm,'password',array('size'=>60,'maxlength'=>40, 'value'=>'')); ?>
      <?php echo $form->error($clientDataForm,'password'); ?>
    </div>
    <div class="row">
      <?php echo $form->labelEx($clientDataForm,'password_repeat'); ?>
      <?php echo $form->passwordField($clientDataForm,'password_repeat',array('size'=>60,'maxlength'=>40, 'value'=>'')); ?>
      <?php echo $form->error($clientDataForm,'password'); ?>
    </div>
    <div class="row buttons">
      <?php echo CHtml::submitButton(Yii::t('cms','Zapisz zmiany')); ?>
    </div>
  <?php $this->endWidget(); ?>
</div><!-- form -->