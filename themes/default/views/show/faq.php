<?
$activeChilds = $page->activeChilds(array('with' => array('article', 'files')));
?>
<?= $this->renderPartial('article', array('page' => $page)) ?>

<div id="faq">
  <? foreach ($activeChilds as $childPage): ?>
    <div class="question">
      <div class="title"><a href="#<?= $childPage->url->address ?>" class="faqLink"><?= $childPage->article->title ?></a></div>
      <div class="short_content"><?= $childPage->article->short_content ?></div>
      <div class="content"><?= $childPage->article->content ?></div>
    </div>
  <? endforeach; ?>
</div>