<?=$this->renderPartial('//shared/_sliderTop')?>

<div id="content">
  <h1><?= $page->activeAnchor?></h1>
  <?foreach($page->activeBanners as $banner):?>
  <div>
    <?if($banner->files && $banner->files[0]->type=='Image'):?>
      <?$file = $banner->files[0]?>
      <div>
        <?if($banner->url):?>
          <a href="<?= Url::getActiveUrl($banner)?>"><img src="<?= $file->link(200)?>" alt="" title=""/></a>
        <?else:?>
          <img src="<?= $file->link(200)?>" alt="" title=""/>
        <?endif?>
      </div>
      <div><?= $file->ActiveTranslationTitle?></div>
      <div><?= $file->ActiveTranslationDescription?></div>
    <?endif?>

  </div>
  <?endforeach?>
</div>

<?= $this->renderPartial('//shared/_listingPages',array('page'=>$page))?>