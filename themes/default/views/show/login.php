<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'logowanie',
    'action'=>$this->createAbsoluteUrl('show/login',array(),'https')
    ));
?>
<?= $form->textField($model, 'username', array('onfocus' => defaultOnFocus('Login'), 'onblur' => defaultOnBlur('Login'))); ?>
<?= $form->passwordField($model, 'password', array('onfocus' => defaultOnFocus('Hasło'), 'onblur' => defaultOnBlur('Hasło'))); ?>
<input name="" id="log" type="submit" value="Zaloguj się" class="login_button" />
<?if($model->getErrors('password')):?>
  <p class="info"><?= implode('<br />',$model->getErrors('password'))?></p>
<?endif?>
<?php $this->endWidget(); ?>