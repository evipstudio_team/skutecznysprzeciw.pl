<div class="content_box">
    <div class="blog_content">
      <?if($page->id==145):?>
        <h1><?= $page->article->title?></h1>
          <div class="left articles-description"><!-- LEFT -->
              <?= $mainPage->article->content ?>
          </div><!-- LEFT end -->
        <?
          $this->widget('zii.widgets.CListView', array(
              'id'=>'article-list',
              'dataProvider'=>$searchPage->searchArticles(1,5),
              'itemView'=>'//shared/_blogArticle',
          ));
        ?>
        <script type="text/javascript">
          $(function() {
            if(window.location.hash) {
              $('.article_title a').each(function() {
                if(window.location.hash == $(this).attr('rel')) {
                  $('html, body').animate({
                    scrollTop: $(this).offset().top
                  }, 1000);
                  return false;
                }
              });
            }
          });

        </script>
      <?else:?>
        <?= $this->renderPartial('//show/article',array('page'=>$page))?>
      <?endif?>
    </div>
</div>