<?=$this->renderPartial('article',array('page'=>$page))?>
<div class="step2">
  <div class="mod mod-document">
  	<div class="lub">lub</div>
    <div class="form_header_step2">Dokument</div>
    <?$form=$this->beginWidget('CActiveForm', array('htmlOptions'=>array('enctype'=>'multipart/form-data')))?>
		<div class="formularz_step2">
      <span class="input_label">Załącz skan lub plik pdf dokumentów:</span>
      <label class="cabinet">
        <?= $form->fileField($step2Form,'file',array('class'=>"file"))?>
        <?= $form->fileField($step2Form,'file2',array('class'=>"file"))?>
        <?= $form->fileField($step2Form,'file3',array('class'=>"file"))?>
      </label>
      <?=$form->error($step2Form,'file'); ?>
      <input type="submit" name="Step2Form[saveFiles]" value="Wyślij" class="submit_step2"/>
    </div>
    <?php $this->endWidget(); ?>
  </div>
  <div class="mod mod-accesscode">
    <div class="form_header_step2">Kod dostępu</div>
    <?$form=$this->beginWidget('CActiveForm', array('htmlOptions'=>array('enctype'=>'multipart/form-data')))?>
    <div class="formularz_step2">
      <span class="input_label">Elektroniczny kod dostępu do akt:</span>
      <?= $form->textField($step2Form,'passCode')?>
      <?=$form->error($step2Form,'passCode'); ?>
      <span><a href="#Gdzie-znajdę-kod-dostępu" class="whereIsTheCode">Gdzie go znajdę?</a></span>
      <input type="submit" name="Step2Form[savePassCode]" value="Wyślij" class="submit_step2"/>
    </div>
    <?php $this->endWidget(); ?>
  </div>
  <div class="clear"></div>
</div>
<?
  $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
      'id'=>'whereIsTheCode',
      'options' => array(
          'title' => 'Komunikat',
          'modal' => true,
          'autoOpen'=>false,
          'width'=>'900',
          'buttons' => array('OK' => 'js:function(){$(this).dialog("close")}')
          )));
?>
<?= $this->renderPartial('//show/article',array('page'=>Page::model()->findByPk(102)))?> 
<? $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>