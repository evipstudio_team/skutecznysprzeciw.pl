Witamy,

Twoje konto w serwisie www.skutecznysprzeciw.pl zostało aktywowane. Od tej chwili możesz 24h / 7 dni w tygodniu sprawdzać status swojej sprawy.
Poniżej podane zostały Twoje dane logowania, w tym hasło (dla bezpieczeństwa niezwłocznie po zalogowaniu zmień je w zakładce moje dane):

login: <?= $login?>

hasło: <?= $password?>

--
Ten e-mail został wygenerowany automatycznie.

Jeżeli masz jakiekolwiek pytanie proszę odpowiedź na ten e-mail lub zadzwoń +48 730 83 83 03.

Pozdrawiamy
Zespół SkutecznySprzeciw.pl