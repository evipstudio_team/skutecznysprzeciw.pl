<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="pl" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/form.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
  </head>
  <body>
    <div id="top">
      <div class="top_left"><?= CHtml::encode(Yii::app()->name) ?></div>
      <div class="top_right">
        <?php
        $this->widget('zii.widgets.CMenu', array(
            'items' => array_merge(array(
                array('label' => Yii::t('cms', 'Struktura aplikacji'), 'url' => array('/page/index'), 'visible' => Yii::app()->user->checkAccess('Root')),
                array('label' => Yii::t('cms', 'Użytkownicy'), 'url' => array('/user/index'), 'visible' => Yii::app()->user->checkAccess('Administrator')),
                array('label' => Yii::t('cms', 'Role'), 'url' => array('/rbam'), 'visible' => Yii::app()->user->checkAccess('RBAC Manager')),
                array('label' => Yii::t('cms', 'Bloki'), 'url' => array('/block/index'), 'visible' => Yii::app()->user->checkAccess('Root'), 'active' => (Yii::app()->getController()->id == 'block') ? true : false),
                array('label' => Yii::t('cms', 'Preferencje'), 'url' => array('/varable/index'), 'visible' => Yii::app()->user->checkAccess('Administrator'), 'active' => (Yii::app()->getController()->id == 'varable') ? true : false),
            )),
        ));
        ?>
      </div>
    </div>
    <div id="content">
      <?= $this->renderPartial('//shared/_flash') ?>
      <?= $this->renderPartial('//shared/_loading') ?>
      <table class="wrap_table"><!--	tabela-kontener	-->
        <tbody>
          <tr>
            <td class="left"><!--	lewa strona contentu	-->
              <div id="sidebar">
                <?php
                $this->beginWidget('zii.widgets.CPortlet', array('title' => '<h5>' . Yii::t('cms', 'Menu') . '</h5>', 'titleCssClass' => 'menu bar'));
                $this->widget('zii.widgets.CMenu', array(
                    'htmlOptions' => array('class' => 'nav_ul_left margin'),
                    'itemCssClass' => 'nav_li_left',
                    'activeCssClass' => 'active',
                    'items' => array_merge(
                        array(
                          array('label' => Yii::t('cms', 'Strona główna'),'url' => array('/site/index')),
                          array('label' => Yii::t('cms', 'Zgłoszenia'), 'url' => array('/cause/index')),
                        ),
                        Page::managedMenu(isset($this->page_id) ? $this->page_id : null),
                        Block::managedMenu()),
                ));
                $this->endWidget();
                ?>
              </div><!-- sidebar -->
            </td> <!--	end	-->

            <td class="right"><!--	prawa strona contentu	-->
              <div id="navi">
                <? if (isset($this->breadcrumbs)): ?>
                  <?
                  $this->widget('zii.widgets.CBreadcrumbs', array(
                      'links' => $this->breadcrumbs,
                      'homeLink' => '<a href="' . $this->createUrl('site/index') . '">Start</a>',
                      'htmlOptions' => array('class' => 'navi_content'),
                      'inactiveLinkTemplate' => '<strong>{label}</strong>'
                  ));
                  ?>

                <? endif ?>
              </div>
              <div style="clear: both"></div>
              <?php echo $content; ?>
            </td>  <!--	end	-->
          </tr>
        </tbody>
      </table>
    </div>
    <div style="height: 100px; width: 100%; float: left"></div>
    <div id="footer">
      <div class="footer_left">Wszelkie prawa zastrzeżone. <br />Powered by <a href="http://www.evipstudio.pl" target="_blank">evipstudio.pl</a></div>
      <div class="footer_right">
        <div style="float:left; margin-top: 6px;">
          <?php $this->widget('LoginInformationWidget') ?>
        </div>
        <div style="float:left">
          <a href="javascript:scroll(0,0);"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/up.png" alt="" /></a>
        </div>
      </div>
    </div>
  </body>
</html>
