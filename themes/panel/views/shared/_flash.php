<? $flashMessages = Yii::app()->user->getFlashes(); ?>
<? if ($flashMessages): ?>
  <?if(!isset($dialogBox) || $dialogBox == true):?>
    <?
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
            array('id'=>'FlashDialog','options' => array(
            'title' => 'Komunikat'
            , 'modal' => true,
            'width'=>'auto'
            , 'buttons' => array('OK' => 'js:function(){$(this).dialog("close")}')
            ))
    );
    ?>
  <?endif?>
  <ul class="flashes" style="list-style: none; margin-top: 20px">
    <? foreach ($flashMessages as $key => $message): ?>
      <li><div class="flash-<?= $key ?>"><?= $message ?></div></li>
  <? endforeach ?>
  </ul>

  <?if(!isset($dialogBox) || $dialogBox == true):?>
    <? $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>
  <?endif?>
<?endif?>