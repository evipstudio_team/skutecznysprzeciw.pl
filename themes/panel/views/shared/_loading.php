<?$this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'LoadingDialogBox',
            'options'=>array(
              'modal'=>true,
              'autoOpen'=>false,
              'open'=>'js:function(){$(".ui-dialog-titlebar").hide();}'
            )));?>

<div style="text-align: center; margin-top: 20px">
  <img src="<?=Yii::app()->theme->baseUrl?>/images/loading.gif" alt="" title="" /><br />
  trwa ładowanie treści...
</div>
<?$this->endWidget('zii.widgets.jui.CJuiDialog');?>